import 'dart:convert';
import 'dart:math';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/models/all_api.dart';
import 'package:http/http.dart';
import 'package:travelx_v1/goodley/models/banking.dart';
import 'dart:io';
import 'dart:async';

import 'package:travelx_v1/goodley/models/counties.dart';
import 'package:travelx_v1/goodley/models/fosa.dart';
import 'package:travelx_v1/goodley/models/loan.dart';
import 'package:travelx_v1/goodley/models/loanapp.dart';


Future<List<Counties>> getCounty(btype) async {
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/getcounty'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: {
        'btype': btype,
      });

  print(response.body.toString());
  if (response.statusCode == 200) {
    List data  = jsonDecode(response.body);
    return data.map((f) => Counties.fromJson(f)).toList();
  } else {
    return [];
  }
}
Future<List<Counties>> getSalesAgent(btype) async {
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/saleagent'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: {
        'btype': btype,
      });

  print('SALESSSS');
  print(response.body.toString());
  if (response.statusCode == 200) {
    List data  = jsonDecode(response.body);
    return data.map((f) => Counties.fromJson(f)).toList();
  } else {
    return [];
  }
}
Future<List<Counties>> getMarital(btype) async {
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/getmarital'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: {
        'btype': btype,
      });

  print(response.body.toString());
  if (response.statusCode == 200) {
    List data  = jsonDecode(response.body);
    return data.map((f) => Counties.fromJson(f)).toList();
  } else {
    return [];
  }
}

Future<List<Counties>> getMemberType(btype) async {
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/membertype'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: {
        'btype': btype,
      });

  print(response.body.toString());
  if (response.statusCode == 200) {
    List data  = jsonDecode(response.body);
    return data.map((f) => Counties.fromJson(f)).toList();
  } else {
    return [];
  }
}

Future<List<Counties>> getSecurityQ(phone,btype) async {
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/getsecurityquestion'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: {
        'username': phone,
        'btype': btype,
      });

  print(response.body.toString());
  if (response.statusCode == 200) {
    List data  = jsonDecode(response.body);
    return data.map((f) => Counties.fromJson(f)).toList();
  } else {
    return [];
  }
}
Future<List<Counties>> getDocType(btype) async {
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/getdocumenttype'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: {
        'btype': btype,
      });

  print(response.body.toString());
  if (response.statusCode == 200) {
    List data  = jsonDecode(response.body);
    return data.map((f) => Counties.fromJson(f)).toList();
  } else {
    return [];
  }
}

Future<AllApi> loginUser(phoneNo, pass, btype) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'pass': pass,
    'btype': btype
  };
  print('body '+body.toString());
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/login'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  print('aft '+phoneNo);

  print('aft'+response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {

    Map data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('phone', phoneNo);
    prefs.setBool('kodi', true);

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: response.body);
  }
}
Future<AllApi> resetPass(phoneNo,btype,answer,agent) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'btype': btype,
    'question': agent,
    'answer': answer,
  };
  print('body '+body.toString());
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/login'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  print('aft '+phoneNo);

  print('aft'+response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('phone', phoneNo);

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: response.body);
  }
}

Future<AllApi> addSecurityQ(phoneNo,btype,answer,agent,pass) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'pass': pass,
    'question': agent,
    'answer': answer,
    'btype': btype,
  };
  print('body '+body.toString());
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/login'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  print('aft '+phoneNo);

  print('aft'+response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('phone', phoneNo);

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: response.body);
  }
}
Future<AllApi> verifyOTPURL(phoneNumber, otpCode, btype, ) async {

  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);
  Map<String, dynamic> body = {
    'username': phoneNumber,
    'otp': otpCode,
    'btype': btype
  };
  print(body);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/confirmlogin'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: json.encode(body));

  print(response.body.toString());
  if (response.statusCode == 200 || response.statusCode == 201) {
    Map data = jsonDecode(response.body);
    print(data);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('logged_in', true);
    prefs.setString('accessToken', data['apikey']);
    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

Future<AllApi> registerUser(phone, fullName, residence, email, dob, gender, id_number) async {

  final response = await post(
      Uri.parse('https://finovate.co.ke/sacco/v1/auth/register'),
      body: {
        'phone': phone,
        'fullName': fullName,
        'residence':residence,
        'email':email,
        'dob':dob,
        'gender':gender,
        'id_number':id_number
      });

  print(response.body.toString());
  if (response.statusCode == 200) {
    Map data = jsonDecode(response.body);
    print(data);
    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}


Future<AllApi> RegUser(String name ,address ,contact ,Btype,email,dob,dtype,
    dno,county,ppin, agent, typeMember, imname, imphone) async {
  var counties = int.parse(county);
  assert(counties is int);
  // var maritals = int.parse(marital);
  // assert(maritals is int);
  var dtypes = int.parse(dtype);
  assert(dtypes is int);
  print(counties);
  // print(maritals);
  print(dtypes);
  Map<String, dynamic> body = {
    'name': name,
    'address': address,
    'contact': contact,
    'btype': Btype,
    'email': email,
    'dob': dob,
    'dtype':dtypes,
    'dno':dno,
    'county':counties,
    'ppin':ppin,
    'agent':agent,
    'mtype': typeMember,
    'imname': imname,
    'imphone': imphone
  };
  print(body.toString());

  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/createcustomer'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: json.encode(body));

  print(body.toString());
  print(response.body.toString());
  Map data = jsonDecode(response.body);
  if (response.statusCode == 200) {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('registered', true);
    prefs.setBool('paid', false);
    prefs.setBool('security', false);
    prefs.setString('IDNO', dno);
    prefs.setString('contact', contact);
    prefs.setString('accountNo', dno);
    print(data);
    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: data);
  }
}

Future<AllApi> subscribe(btype) async {

  Map<String, dynamic> body = {
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/confirmregistrationpayment'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: body);

  print(response.body.toString());
  Map data = jsonDecode(response.body);
  if (response.statusCode == 200) {

    print(data);
    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data:data);
  }
}
Future<AllApi> pay(phone,account) async {

  Map<String, dynamic> body = {
    'phoneNo': phone,
    'account': account
  };

  final response = await post(
      Uri.parse('https://finovate.co.ke/sacco/v1/subscription'),
      body: body);

  print(response.body.toString());
  if (response.statusCode == 200) {
    Map data = jsonDecode(response.body);
    print(data);
    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}
Future<AllApi> clubInvest(name,email,phone,id,cycle,plan) async {

  Map<String, dynamic> body = {
    'name': name,
    'email': email,
    'phone': phone,
    'id': id,
    'cycle': cycle,
    'plan': plan
  };

  final response = await post(
      Uri.parse('https://finovate.co.ke/sacco/v1/sacco/club'),
      body: body);

  print(response.body.toString());
  if (response.statusCode == 200) {
    Map data = jsonDecode(response.body);
    print(data);
    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

Future<AllApi> chamaData(groupName,location,regNumber,chairName,chairId,chairPin,
    chairPhone,treasurerName,treasurerId,treasurerPin,treasurerPhone,
    secretaryName,secretaryId,secretaryPin,secretaryPhone,
    contribution,agent) async {

  Map<String, dynamic> body = {
    'groupName': groupName,
    'location': location,
    'regNumber': regNumber,
    'chairName': chairName,
    'chairId': chairId,
    'chairPin': chairPin,
    'chairPhone': chairPhone,
    'treasurerName': treasurerName,
    'treasurerId': treasurerId,
    'treasurerPin': treasurerPin,
    'treasurerPhone': treasurerPhone,
    'secretaryName': secretaryName,
    'secretaryId': secretaryId,
    'secretaryPin': secretaryPin,
    'secretaryPhone': secretaryPhone,
    'contribution': contribution,
    'agent': agent,
  };

  final response = await post(
      Uri.parse('https://finovate.co.ke/sacco/v1/sacco/chama'),
      body: body);

  print(response.body.toString());
  if (response.statusCode == 200) {
    Map data = jsonDecode(response.body);
    print(data);
    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

Future<AllApi> checkPay(checkout) async {

  Map<String, dynamic> body = {
    'CheckoutRequestID': checkout
  };

  final response = await post(
      Uri.parse('https://finovate.co.ke/sacco/v1/sacco/check'),
      body: body);

  print(response.body.toString());
  if (response.statusCode == 200) {
    Map data = jsonDecode(response.body);
    print(data);
    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}
Future<AllApi> depositFosa(phone,account,amount) async {

  Map<String, dynamic> body = {
    'phoneNo': phone,
    'account': account,
    'amount': amount
  };

  final response = await post(
      Uri.parse('https://finovate.co.ke/sacco/v1/subscription'),
      body: body);

  print(response.body.toString());
  if (response.statusCode == 200) {
    Map data = jsonDecode(response.body);
    print(data);
    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

Future<AllApi> confirmPay(phone,code,amount,btype) async {

  Map<String, dynamic> body = {
    'username': phone,
    'code': code,
    'amount': amount,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);
  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/confirmregistrationpayment'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  print('body '+body.toString());
  print(response.body.toString());
  if (response.statusCode == 200) {
    Map data = jsonDecode(response.body);
    print(data);
    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}
Future<AllApi> sendOTP(phoneNo) async {
  Map<String, dynamic> body = {
    'phoneNumber': phoneNo,
  };
  print('oja'+phoneNo);
  final response = await post(Uri.parse('https://goodleycompany.com/api/v1/otp/code'), body: body);
  print('aft'+phoneNo);
print('sadad'+response.body.toString());
  if (response.statusCode == 200) {

    Map data = jsonDecode(response.body);
    print(data);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('otpToken', data['otpToken']);
    prefs.setString('phoneNumber', phoneNo);

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}



//Reset Pin
Future<AllApi> resetPin(phoneNo, pass, btype) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'pass': pass,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/login'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: body);

  print('aft '+phoneNo);

  print('aft'+response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('phone', phoneNo);

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

//Profile
Future<AllApi> profile(phoneNo, apikey, btype) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/profile'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('phone', phoneNo);

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

//NextOfKin
Future<AllApi> addNextOfKin(name ,kinphone ,relation, percent, usernames, apikey, Btype) async {
  Map<String, dynamic> body = {
    'name': name,
    'kinphone': kinphone,
    'relation': relation,
    'percent': percent,
    'username': usernames,
    'apikey': apikey,
    'btype': Btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/addnxtkin'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

//Update Profile
Future<AllApi> updateProfile(kra,dob,pass,phoneNo, apikey, btype) async {
  Map<String, dynamic> body = {
    'kra': kra,
    'dob': dob,
    'pass': pass,
    'username': phoneNo,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);
  print(body);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/uprofile'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('phone', phoneNo);

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

//View Fosa Accounts
Future<List<Fosa>> fosaAccount(phoneNo, apikey, btype) async {
  print('apikey '+apikey);
  print('phone '+phoneNo);

  Map<String, dynamic> body = {
    'username': phoneNo,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/baccounts'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));
  print('body '+body.toString());
  print('res '+response.body.toString());

  print(response.statusCode);
  if (response.statusCode == 200  || response.statusCode == 201) {


    List data  = jsonDecode(response.body);
    return data.map((f) => Fosa.fromJson(f)).toList();
  } else {
    return [];
  }
}
Future<AllApi> fosaAll(phoneNo, apikey, btype) async {
  print('apikey '+apikey);
  print('phone '+phoneNo);

  Map<String, dynamic> body = {
    'username': phoneNo,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/baccounts'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));
  print('body '+body.toString());
  print('res '+response.body.toString());

  print(response.statusCode);

  var data = jsonDecode(response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {


    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}
//View Facility Accounts
Future<AllApi> accountUser(phoneNo, apikey, btype) async {
  print('apikey '+apikey);
  print('phone '+phoneNo);

  Map<String, dynamic> body = {
    'username': phoneNo,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/laccounts'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));
  print('body '+body.toString());

  // print(response.statusCode);
  // print('dataOther'+response.body);
  var data = jsonDecode(response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {


    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

//Get Loan Facility types
Future<List<LoanApp>> loanFacility(phoneNo, apikey, btype) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print('FETCHING LOANS 2');
  print(basicAuth);
  print('https://afi.tridexgroup.com/run/ltype');
  print(body);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/ltype'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  print('aft '+phoneNo);

  print('Response: '+response.body);
  if (response.statusCode == 200) {
    List data  = jsonDecode(response.body);
    return data.map((f) => LoanApp.fromJson(f)).toList();
  } else {
    return [];
  }
}
Future<AllApi> depositMpesa(phoneNo, amount,pmethod,transd,source,refaccountno, btype,apikey, pass) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'amount': amount,
    'pmethod': pmethod,
    'transd': transd,
    'source': source,
    'refaccountno': refaccountno,
    'apikey': apikey,
    'btype': btype,
    'pass':pass
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);
  print('body '+body.toString());
  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/addbdepo'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));



  print('aft'+response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

//Deposit Money into banking /Loan Account
Future<AllApi> deposit(phoneNo, amount,pmethod,transd,source,refaccountno, btype,apikey) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'amount': amount,
    'pmethod': pmethod,
    'transd': transd,
    'source': source,
    'refaccountno': refaccountno,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);
  print('body '+body.toString());
  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/addbdepo'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));



  print('aft'+response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}
// Transfer Cash
Future<AllApi> confirmPass(phoneNo, pass,btype,apikey) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'pass': pass,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/confirmpass'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  print('aft '+body.toString());

  print('aft'+response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());

    return AllApi(data:  response.body);
  } else {
    return AllApi(data: response.body);
  }
}
//Deposit mpesa

Future<AllApi> depoFin(phone,account, amount) async {

  Map<String, dynamic> body = {
    'phoneNo': phone,
    'account': account,
    'amount': amount
  };

  final response = await post(
      Uri.parse('https://finovate.co.ke/sacco/v1/deposit'),
      body: body);

  print(response.body.toString());
  if (response.statusCode == 200) {
    Map data = jsonDecode(response.body);
    print(data);
    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}
//Transfer
Future<AllApi> transfer(phoneNo, amount,account,raccount,code,apikey,btype) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'amount': amount,
    'account': account,
    'raccount': raccount,
    'code': code,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);
  print(body);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/tapply'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  print('aft '+body.toString());

  print('aft'+response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());

    return AllApi(data: data,code: response.statusCode);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

//Transfer FOSA to FOSA
Future<AllApi> transferFosaToFosa(phoneNo, amount,account,raccount,code,apikey,btype) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'amount': amount,
    'account': raccount,
    'raccount': account,
    'code': code,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);
  print(body);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/tapply'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  print('aft '+body.toString());

  print('aft'+response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());

    return AllApi(data: data,code: response.statusCode);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}
//Withdraw Money from Account
Future<AllApi> withdraw(phoneNo, amount,refaccountno, btype,apikey,code) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'amount': amount,
    'pmethod': '2',
    'transd': 'MPESA CODE',
    'refaccountno':refaccountno,
    'apikey': apikey,
    'btype': btype,
    'code': code
  };
  print('_______BODY PRINT');

  print(body);
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/addbwith'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body:  jsonEncode(body));


  print('aft1 '+response.body);
  print('aft2 '+response.body.toString());
  print('aft3 '+response.statusCode.toString());
  Map data;
  if (response.statusCode == 200  || response.statusCode == 201) {


    data = jsonDecode(response.body);

    return AllApi(data: data);
  } else {
    data = jsonDecode(response.body);
    return AllApi(code: response.statusCode, data: data);
  }
}

//Withdraw Money from Account
Future<AllApi> transferFosa(phoneNo, amount,refaccountno, btype,apikey,code) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'amount': amount,
    'pmethod': '2',
    'transd': 'MPESA CODE',
    'refaccountno':refaccountno,
    'apikey': apikey,
    'btype': btype,
    'code': code
  };
  print('_______BODY PRINT');

  print(body);
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/addbwith'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body:  jsonEncode(body));


  print('aft1'+response.body);
  print('aft2'+response.body.toString());
  print('aft3'+response.statusCode.toString());
  Map data;
  if (response.statusCode == 200  || response.statusCode == 201) {


    data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());

    return AllApi(data: data);
  } else {
    data = jsonDecode(response.body);
    print('DATAAAAAA2>'+ data.toString());
    return AllApi(code: response.statusCode, data: data);
  }
}
//Set Target
Future<AllApi> setTarget(phoneNo, amount,refaccountno, btype,apikey) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'amount': amount,
    'refaccountno':refaccountno,
    'apikey': apikey,
    'btype': btype
  };
  print('_______BODY PRINT');

  print(body);
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/addtarget'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body:  jsonEncode(body));


  print('aft1'+response.body);
  print('aft2'+response.body.toString());
  print('aft3'+response.statusCode.toString());
  Map data;
  if (response.statusCode == 200  || response.statusCode == 201) {


    data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());

    return AllApi(data: data);
  } else {
    data = jsonDecode(response.body);
    print('DATAAAAAA2>'+ data.toString());
    return AllApi(code: response.statusCode, data: data);
  }
}
//Set Targets
Future<AllApi> setTargets(phoneNo, amount,targetperiod,refaccountno, btype,apikey) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'amount': amount,
    'targetperiod': targetperiod,
    'refaccountno':refaccountno,
    'apikey': apikey,
    'btype': btype
  };
  print('_______BODY PRINT');

  print(body);
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/addtarget'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body:  jsonEncode(body));


  print('aft1'+response.body);
  print('aft2'+response.body.toString());
  print('aft3'+response.statusCode.toString());
  Map data;
  if (response.statusCode == 200  || response.statusCode == 201) {


    data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());

    return AllApi(data: data);
  } else {
    data = jsonDecode(response.body);
    print('DATAAAAAA2>'+ data.toString());
    return AllApi(code: response.statusCode, data: data);
  }
}
//Set Target Kadogo
Future<AllApi> setTargetKadogo(phoneNo, amount,targetperiod,refaccountno, btype,apikey) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'amount': amount,
    'targetperiod': int.parse(targetperiod),
    'refaccountno':refaccountno,
    'apikey': apikey,
    'btype': btype
  };
  print('_______BODY PRINT');

  print(body);
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/addtarget'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body:  jsonEncode(body));


  print('aft1'+response.body);
  print('aft2'+response.body.toString());
  print('aft3'+response.statusCode.toString());
  Map data;
  if (response.statusCode == 200  || response.statusCode == 201) {


    data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());

    return AllApi(data: data);
  } else {
    data = jsonDecode(response.body);
    print('DATAAAAAA2>'+ data.toString());
    return AllApi(code: response.statusCode, data: data);
  }
}
//Withdraw Money from Account
Future<AllApi> transactionCharges(amount, btype,apikey) async {
  Map<String, dynamic> body = {
    'amount': amount,
    'apikey': apikey,
    'btype': btype
  };
  print('_______BODY PRINT');

  print(body);
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/transactioncharge'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body:  jsonEncode(body));


  print('aft'+response.body);
  print('aft'+response.toString());
  print('aft'+response.statusCode.toString());
  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

//Transfer Money from Account
Future<AllApi> transferCharges(amount, btype,apikey) async {
  Map<String, dynamic> body = {
    'amount': amount,
    'apikey': apikey,
    'btype': btype
  };
  print('_______BODY PRINT');

  print(body);
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/transfercharge'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body:  jsonEncode(body));


  print('aft'+response.body);
  print('aft'+response.toString());
  print('aft'+response.statusCode.toString());
  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

//Banking Account statemnet
Future<List<Bank>> bankAccount(phoneNo, refaccountno, btype,apikey) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'refaccountno': refaccountno,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/accountstatement'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  print('aft '+phoneNo);

  print('aft'+response.body);
  if (response.statusCode == 200) {
    List data  = jsonDecode(response.body);
    return data.map((f) => Bank.fromJson(f)).toList();
  } else {
    return [];
  }
}

//Loan Account statemnet
Future<List<Loan>> loanAccount(phoneNo, refaccountno, btype,apikey) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'refaccountno': refaccountno,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/loanaccountstatement'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: body);

  print('aft '+phoneNo);

  print('aft'+response.body);
  if (response.statusCode == 200) {
    List data  = jsonDecode(response.body);
    return data.map((f) => Loan.fromJson(f)).toList();
  } else {
    return [];
  }
}

///Apply Loan
Future<AllApi> applyLoan(phoneNo, amount,account,raccount,reason,code,apikey,btype) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'amount': amount,
    'type': account,
    'rperiod': raccount,
    'reason': reason,
    'code': code,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);
  print(body);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/lapply'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  print('aft '+body.toString());

  print('aft'+response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());

    return AllApi(data: data);
  } else {
    Map data = jsonDecode(response.body);
    return AllApi(code: response.statusCode, data: data);
  }
}

///New Apply Jijenge Loan
Future<AllApi> applyJijengeLoanNew(phoneNo,pass, amount,account,raccount,apikey,btype,nameG1,
    phoneG1,idno1,kra1,member_no1,nameG2,phoneG2,idno2,kra2,member_no2) async {

  var body = {
    'username': phoneNo,
    'pass': pass,
    'btype': btype,
    'type': account.toString(),
    'amount': amount.toString(),
    'rperiod': raccount.toString(),
    'apikey': apikey,
    'name1': nameG1,
    'phone1': phoneG1,
    'idno1': idno1,
    'kra1': kra1,
    'member_no1': member_no1,
    'name2': nameG2,
    'phone2': phoneG2,
    'idno2': idno2,
    'kra2': kra2,
    'member_no2': member_no2,
  };
  final map = body;
  map.removeWhere((key, value) => key == null || value == null || value == "" );
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/lapplyj'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(map));

  print('aft '+map.toString());

  print('aft'+response.body);
  print('Status Code: '+response.statusCode.toString());
  Map data = jsonDecode(response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {



    print('DATAAAAAA>'+ data.toString());

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: data);
  }
}
//Apply Jijenge Loan
Future<AllApi> applyJijengeLoan(phoneNo, amount,account,raccount,code,apikey,btype) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'amount': amount,
    'type': account,
    'rperiod': raccount,
    'code': code,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/lapplyj'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  print('aft '+body.toString());

  print('aft'+response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

//Apply Jijenge Loan
Future<AllApi> addGuarantor(phoneNo, name,phone,idno,member_no,kra,ref,apikey,btype) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'name': name,
    'phone': phone,
    'idno': idno,
    'member_no': member_no,
    'kra': kra,
    'ref': ref,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/addgrnt'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  print('aft '+body.toString());

  print('aft'+response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());

    return AllApi(data: data);
  } else {
    Map data = jsonDecode(response.body);
    return AllApi(code: response.statusCode, data: data);
  }
}

//Apply Jijenge Loan
Future<AllApi> addBusiness(phoneNo, name,baddress,bloc,nature,monthly,apikey,btype) async {
  Map<String, dynamic> body = {
    'username': phoneNo,
    'name': name,
    'baddress': baddress,
    'bloc': bloc,
    'nature': nature,
    'monthly': monthly,
    'apikey': apikey,
    'btype': btype
  };
  String username = 'AfriNofiMCERewqe';
  String password = 'GuterDcsewr401f713d842e7f2067318732ad92eb1e';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await post(
      Uri.parse('https://afi.tridexgroup.com/run/addmemberbus'),
      headers: {HttpHeaders.authorizationHeader: "$basicAuth"},
      body: jsonEncode(body));

  print('aft '+body.toString());

  print('aft'+response.body);
  if (response.statusCode == 200  || response.statusCode == 201) {


    Map data = jsonDecode(response.body);
    print('DATAAAAAA>'+ data.toString());

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}


//Version

Future<AllApi> versionApp() async {

  final response = await get(
      Uri.parse('https://finovate.co.ke/sacco/v1/sacco/version'));

  print(response.body.toString());
  if (response.statusCode == 200) {
    Map data = jsonDecode(response.body);
    print(data);
    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}