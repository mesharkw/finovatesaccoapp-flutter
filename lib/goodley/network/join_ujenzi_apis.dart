import 'dart:convert';

import 'package:travelx_v1/goodley/utils/constants.dart';
import 'package:travelx_v1/goodley/utils/preferences.dart';




Future<bool> addPersonalInfo(body) async {
  final res = await post('$baseUrl/auth/register', body: body);
  if (res.statusCode == 200) {
    Map js = jsonDecode(res.body);
    Preferences().setMemberId(js['memberid']);
    return true;
  }
  return false;
}

post(String s, {body}) {
}

Future<bool> addKinInfo(body) async {
  final res = await post('$baseUrl/auth/add_kin', body: body);
  print(res.body);
  if (res.statusCode == 200) {
    return true;
  }
  return false;
}

Future<bool> addJobInfo(body) async {
  final res = await post('$baseUrl/auth/add_job_info', body: body);
  if (res.statusCode == 200) {
    return true;
  }
  return false;
}

Future<bool> addSocialInfo(body) async {
  final res = await post('$baseUrl/auth/add_social_info', body: jsonEncode(body));
  if (res.statusCode == 200) {
    return true;
  }
  return false;
}

Future<bool> payJoin(body) async {
  final res = await post('$baseUrl/fundi/payment', body: jsonEncode(body));
  if (res.statusCode == 200) {
    return true;
  }
  return false;
}
