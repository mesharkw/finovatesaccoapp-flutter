import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/models/all_api.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'dart:io';
import 'dart:async';

import 'package:travelx_v1/goodley/models/funds.dart';



Future<AllApi> makePayment(phoneNo, amount, type, user,token) async {
  Map<String, dynamic> body = {
    'phoneNo': phoneNo,
  };
  print(amount);
  final response = await post(Uri.parse('https://finovate.co.ke/sacco/v1/sacco/send_money'),
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
      body: {
    'phoneNumber': phoneNo,
    'amount': amount,
    'type': type,
    'user_id': user,
  });
String authToken;
print(response.body.toString());
  if (response.statusCode == 200) {

    Map data = jsonDecode(response.body);

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}


Future<AllApi> getTargets(user,token) async {
  final response = await post(Uri.parse('https://goodleycompany.com/api/v1/goodley/get_target'),
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
      body: {
        'user_id': user,
      });

  if (response.statusCode == 200) {

    Map data = jsonDecode(response.body);
    print(data['target']);
    return AllApi(data: data['target']);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

Future<AllApi> addtarget(target, user_id,token) async {
  final response = await post(Uri.parse('https://goodleycompany.com/api/v1/goodley/target'),
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
      body: {
        'target': target,
        'user_id': user_id,
      });
  print(response.body.toString());
  if (response.statusCode == 200) {

    Map data = jsonDecode(response.body);

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}
Future<AllApi> addFeedback(name,email,phoneNumber,message, user_id,token) async {
  final response = await post(Uri.parse('https://goodleycompany.com/api/v1/goodley/feedback'),
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
      body: {
        'name': name,
        'email': email,
        'phoneNumber': phoneNumber,
        'message': message,
        'user_id': user_id,
      });
  print(response.body.toString());
  if (response.statusCode == 200) {

    Map data = jsonDecode(response.body);

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}
Future<AllApi> addQuestion(name,email,phoneNumber,message, user_id,token) async {
  final response = await post(Uri.parse('https://goodleycompany.com/api/v1/goodley/questions'),
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
      body: {
        'name': name,
        'email': email,
        'phoneNumber': phoneNumber,
        'message': message,
        'user_id': user_id,
      });
  print(response.body.toString());
  if (response.statusCode == 200) {

    Map data = jsonDecode(response.body);

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

Future<AllApi> updateUser(name, email, career, phoneNumber,nextofkinNames,nextofkinPhone, user_id, token) async {
  final response = await post(Uri.parse('https://finovate.co.ke/sacco/v1/sacco/update_user'),
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
      body: {
        'name': name,
        'career': career,
        'email': email,
        'phoneNumber': phoneNumber,
        'nextOfKinName': nextofkinNames,
        'nextOfKinPhone': nextofkinPhone,
        'user_id': user_id,
      });
  print(response.body.toString());
  if (response.statusCode == 200) {

    Map data = jsonDecode(response.body);

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

Future<AllApi> getTotalWallets(user_id,token) async {
  final response = await post(Uri.parse('https://finovate.co.ke/sacco/v1/sacco/total_funds_totals'),
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
      body: {
        'user_id': user_id,
      });
  if (response.statusCode == 200) {
    Map data = jsonDecode(response.body);
    print(data);

    return AllApi(data: data['data']);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}


Future<AllApi> getReport(startDate,endDate, user_id,token) async {
  final response = await post(Uri.parse('https://finovate.co.ke/sacco/v1/sacco/reports'),
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
      body: {
        'startDate': startDate,
        'endDate': endDate,
        'user_id': user_id,
      });
  if (response.statusCode == 200) {
    Map data = jsonDecode(response.body);
    return AllApi(data: data['file']);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}



Future<AllApi> transferFunds(amount, type, user,token) async {

  print(amount);
  final response = await post(Uri.parse('https://finovate.co.ke/sacco/v1/sacco/transfer_funds'),
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
      body: {
        'amount': amount,
        'type': type,
        'user_id': user,
      });
  String authToken;
  print(response.body.toString());
  if (response.statusCode == 200) {

    Map data = jsonDecode(response.body);

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}

Future<AllApi> checkPayment(phoneNo) async {

  final response = await post(
      Uri.parse('https://finovate.co.ke/sacco/v1/sacco/mpesa_trans'), body: {'phoneNumber': phoneNo});

  print(response.body.toString());
  if (response.statusCode == 200) {
    Map data = jsonDecode(response.body);

    return AllApi(data: data);
  } else {
    return AllApi(code: response.statusCode, data: []);
  }
}


Future<List<FundsModel>> checkFunds(user,token) async {
  final response = await post(Uri.parse('https://finovate.co.ke/sacco/v1/sacco/total_funds_user'),
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
      body: {
        'user_id': user,
      });


  print('*****************');
  // print(response.body.toString());
  if (response.statusCode == 200) {

    Map data  = jsonDecode(response.body);

    print('data' +  data.toString());
    var list = data['data'] as List;
    // List<FundsModel> imagesList = list.map((i) => FundsModel.fromJson(i)).toList();
    return list.map((f) => FundsModel.fromJson(f)).toList();
  } else {
    return [];
  }
}


