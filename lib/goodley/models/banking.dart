class Bank {
  String date_posted;
  String description;
  String money_out;
  String money_in;
  Bank(
      { this.date_posted,
        this.description,
        this.money_out,
        this.money_in,
      });

  Bank.fromJson(Map<String, dynamic> json) {
    date_posted = json['date_posted'];
    description = json['description'];
    money_out = json['money_out'];
    money_in = json['money_in'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date_posted'] = this.date_posted;
    data['description'] = this.description;
    data['money_out'] = this.money_out;
    data['money_in'] = this.money_in;

    return data;
  }
}
