class LAccounts {
  String name;
  String no;
  String amount;
  String repay_period;
  String type;
  String status;
  String balance;
  LAccounts(
      { this.name,
        this.no,
        this.amount,
        this.repay_period,
        this.type,
        this.status,
        this.balance,
      });

  LAccounts.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    no = json['no'];
    amount = json['amount'];
    repay_period = json['repay_period'];
    type = json['type'];
    status = json['status'];
    balance = json['balance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['no'] = this.no;
    data['amount'] = this.amount;
    data['repay_period'] = this.repay_period;
    data['type'] = this.type;
    data['status'] = this.status;
    data['balance'] = this.balance;

    return data;
  }
}
