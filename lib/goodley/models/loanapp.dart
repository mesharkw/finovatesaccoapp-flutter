class LoanApp {
  String id;
  String name;
  String description;
  String requirement;
  String max_repay_period;
  String deposit;
  String set_loan_amount;
  String repaid_in;
  LoanApp(
      { this.id,
        this.name,
        this.description,
        this.requirement,
        this.max_repay_period,
        this.deposit,
        this.set_loan_amount,
        this.repaid_in,
      });

  LoanApp.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    requirement = json['requirement'];
    max_repay_period = json['max_repay_period'];
    deposit = json['deposit'];
    set_loan_amount = json['set_loan_amount'];
    repaid_in = json['repaid_in'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['requirement'] = this.requirement;
    data['max_repay_period'] = this.max_repay_period;
    data['deposit'] = this.deposit;
    data['set_loan_amount'] = this.set_loan_amount;
    data['repaid_in'] = this.repaid_in;

    return data;
  }
}
