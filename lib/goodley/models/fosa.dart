class Fosa {
  String name;
  String no;
  String type;
  String target;
  String targetperiod;
  var bal;
  Fosa(
      { this.name,
        this.no,
        this.type,
        this.bal,
        this.target,
        this.targetperiod,
      });

  Fosa.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    no = json['no'];
    type = json['type'];
    bal = json['bal'];
    target = json['target'];
    targetperiod = json['targetperiod'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['no'] = this.no;
    data['type'] = this.type;
    data['bal'] = this.bal;
    data['target'] = this.target;
    data['targetperiod'] = this.targetperiod;

    return data;
  }
}
