class FundsModel {
  String user_id;
  String type;
  String created_at;
  String amount;
  String completed;
  String phoneNumber;
  String transaction_id;
  String mpesaReceiptNumber;
  FundsModel(
      {this.user_id,
        this.type,
        this.created_at,
        this.amount,
        this.completed,
        this.phoneNumber,
        this.transaction_id,
        this.mpesaReceiptNumber
      });

  FundsModel.fromJson(Map<String, dynamic> json) {
    user_id = json['user_id'];
    type = json['type'];
    created_at = json['created_at'];
    amount = json['amount'];
    completed = json['completed'];
    phoneNumber = json['phoneNumber'];
    transaction_id = json['transaction_id'];
    mpesaReceiptNumber = json['mpesaReceiptNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.user_id;
    data['type'] = this.type;
    data['created_at'] = this.created_at;
    data['amount'] = this.amount;
    data['completed'] = this.completed;
    data['phoneNumber'] = this.phoneNumber;
    data['transaction_id'] = this.transaction_id;
    data['mpesaReceiptNumber'] = this.mpesaReceiptNumber;

    return data;
  }
}
