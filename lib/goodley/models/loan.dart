class Loan {
  String date_posted;
  String description;
  String pay_out;
  String pay_in;
  Loan(
      { this.date_posted,
        this.description,
        this.pay_out,
        this.pay_in,
      });

  Loan.fromJson(Map<String, dynamic> json) {
    date_posted = json['date_posted'];
    description = json['description'];
    pay_out = json['pay_out'];
    pay_in = json['pay_in'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date_posted'] = this.date_posted;
    data['description'] = this.description;
    data['pay_out'] = this.pay_out;
    data['pay_in'] = this.pay_in;

    return data;
  }
}
