import 'package:flutter/material.dart';

fieldFocusChange(
    BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
  currentFocus.unfocus();
  FocusScope.of(context).requestFocus(nextFocus);
}

String validatorPassword(String value) {
  if (value.isEmpty || value.length < 5) {
    return 'Add atleast 4 characters';
  }
  return null;
}
String validatorOtp(String value) {
  if (value.isEmpty || value.length < 6) {
    return 'Add atleast 4 characters';
  }
  return null;
}


String validatorEmpty(String value) {
  if (value.isEmpty) {
    return 'Required field';
  }
  return null;
}

String validatePhone(String value) {
  if (value.length < 9 || value.length >15) {
    return 'Invalid phone number';
  }
  return null;
}

String validateEmail(String value) {
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(pattern);
  if (!regex.hasMatch(value))
    return 'Enter a valid Email';
  else
    return null;
}
