import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

Widget spinKit (){
  return
  SpinKitThreeBounce(
    color: Color(0xFF922b20),
    size: 30.0,
  );

}

Widget spinKitBounce (){
  return
    SpinKitWanderingCubes(
      color:Color(0xFF922b20),
      size: 30.0,
    );

}

