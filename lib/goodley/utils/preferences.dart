

import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  setCurrentStep(int step) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('step', step);
  }

  getCurrentStep() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt('step');
  }

  setMemberId(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('memberID', id);
  }
  getMemberId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberID');
  }

  //not needed
  clearStep() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('step');
  }
}
