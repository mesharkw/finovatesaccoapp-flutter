import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/screens/login.dart';
import 'package:travelx_v1/screens/onboarding.dart';

import '../../screens/forgot_password.dart';
import '../../size_config.dart';
import '../models/all_api.dart';
import '../models/counties.dart';
import '../network/otp_api.dart';
import '../start.dart';
import '../utils/common_utils.dart';
import '../utils/spinner.dart';
import '../widgets/toast.dart';


class UpdateProfile extends StatefulWidget {
  @override
  _UpdateProfileState createState() => _UpdateProfileState();
}

class _UpdateProfileState extends State<UpdateProfile> {
  bool loading = false;
  final _formKey_signup = GlobalKey<FormState>();
  final f = new DateFormat('yyyy-MM-dd');

  final kra = new TextEditingController();
  final name = new TextEditingController();
  final kinphone = new TextEditingController();
  final relation = new TextEditingController();
  final percent = new TextEditingController();
  final pass = new TextEditingController();

  String business;
  String  employed;
  String  dtype;

  int _groupValue = -1;
  int typeMember;
  String gender;
  String countyID;
  String countyName;
  String _chosenValue;
  List docType = [];
  bool _show = true;
  bool _accept = true;
  List<Counties> maritals;
  List<Counties> mType;
  List<Counties> counties;
  List<Counties> sAagents;
  String Btype = '1a862df26f6943997cef90233877a4fe';
  String _mySelection;
  String _mySelectionM;
  String agent;
  String accessToken ="";
  String target ="";
  String residence="";
  String phoneNumber="";
  String user_id ="";
  String token ="";
  String nextOfKinName ="";
  String nextOfKinPhone ="";
  String member_no ="";
  String savings ="";
  String dob ="";
  String id_no="";

  Future<void> sendReg(String kraNumber ,names ,kinphone ,relation, percent,Btype,username,apikey, pass) async {
    AllApi response = await updateProfile(kraNumber,dob,pass, username, apikey, Btype);
    print(response.code);
    print(response.data);

    if (response != null && response.code != 400 && response.code != 401) {
      AllApi response = await addNextOfKin(
          names,
          kinphone,
          relation,
          percent,
          username,
          apikey,
          Btype);
      print(response.code);

      if (response != null && response.code != 400) {
//        await instance.fetchCitizenPost(id, token);
        setState(() {
          loading = false;
        });
        Navigator.pop(context);
      } else {
        setState(() {
          loading = false;
        });
      }
    } else {
      setState(() {
        loading = false;
      });
    }
  }

  /// Valid phone number
  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }



  @override
  void initState()  {
    // TODO: implement initState
    super.initState();
    getSharedPref();
  }
  void getSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    setState(() {

      phoneNumber = prefs.getString('phone');
      accessToken = prefs.getString('accessToken');

    });

    getProfile(phoneNumber,accessToken,Btype);
  }
  Future<void> getProfile(phoneNumbers,accessToken,btype) async {
    if (btype != null) {
      var response = await profile(phoneNumbers,accessToken,btype);
      print('dataaaaaa'+ response.toString());
      if (response != null || response.code !=400 ) {

        setState(() {
          dob = response.data['dob'];
          // id_no = response.data['id_no'];
        });
        // this.counties = response;
        // setState(() {
        // });
      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Form(
            key: _formKey_signup,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //Logo
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,

                  children: [
                    SizedBox.fromSize(),
                    Padding(
                      padding: const EdgeInsets.only(top:50,right: 25.0),
                      child: GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ForgotPassword()));
                        },
                        child: Text(
                          'Update User Data',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 2.6 * SizeConfig.textMultiplier,
                              fontFamily: 'PoppinsRegular',
                              fontWeight: FontWeight.bold
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    SizedBox.fromSize(),
                  ],
                ),
                Column(

                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 30.0,
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    //KRA PIN
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 7.8* SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: kra,
                          keyboardType: TextInputType.text,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.5 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'KRA PIN',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.account_box_outlined),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 5.8 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: name,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.5 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Next Of Kin Name',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.person),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    //Area of Residence
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 5.8 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: kinphone,
                          validator: (value) => validatorEmpty(value),
                          keyboardType: TextInputType.phone,
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.5 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Next Of kin Phone Number',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.phone),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 5.8 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: relation,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.5 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Next Of Kin Relationship',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.person),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 5.8 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: percent,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.5 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Next Of Kin Percentage',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.percent),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 5.8 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: pass,
                          obscureText: true,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.5 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Verify Pin',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.password),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    //NextButton
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,4,20,4),
                      child: Container(
                          height: 6* SizeConfig.heightSizeMultiplier,
                          width: MediaQuery.of(context).size.width-10,
                          child: loading ? spinKit() : RaisedButton(
                            onPressed: () {

                              if (_formKey_signup.currentState.validate()) {
                                setState(() {
                                  loading = true;
                                });
                                sendReg(kra.text ,name.text ,kinphone.text ,relation.text, percent.text,Btype,phoneNumber,accessToken,pass.text);

                              }
                              print('Next');
                            },
                            child: Text(
                              'Update Information ',
                              style: TextStyle(fontSize: 2.2 * SizeConfig.textMultiplier, fontWeight: FontWeight.bold, color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                            color: Theme.of(context).primaryColor,
                          )),
                    ),
                    SizedBox(
                      height: 40.0,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

}
