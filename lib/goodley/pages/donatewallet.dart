import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/models/all_api.dart';
import 'package:travelx_v1/goodley/models/ewallet.dart';
import 'package:travelx_v1/goodley/network/payment.dart';
import 'package:travelx_v1/goodley/utils/common_utils.dart';
import 'package:travelx_v1/goodley/utils/new_radio.dart';
import 'package:travelx_v1/goodley/utils/spinner.dart';
import 'package:travelx_v1/goodley/widgets/toast.dart';
import 'package:url_launcher/url_launcher.dart';

class DonateWallet extends StatefulWidget {
  DonateWallet({Key key, this.title}) : super(key: key);

  final String title;



  @override
  _EWalletState createState() => _EWalletState();
}

class _EWalletState extends State<DonateWallet> {
  List lessons;
  void _settingModalBottomSheet(context, setState, amount, calling){

    bool _needCover = true;
    showModalBottomSheet(context: context, isScrollControlled: true, shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(
        top: Radius.circular(20),
      ),
    ),
      clipBehavior: Clip.antiAliasWithSaveLayer,

        builder: (BuildContext context) {
          return DraggableScrollableSheet(
              initialChildSize: 0.75, //set this as you want
              maxChildSize: 0.75, //set this as you want
              minChildSize: 0.75, //set this as you want
              expand: true,
              builder: (context, scrollController) {
      return ModalPayment();
    }
          );
        }
    );
  }


  bool loading = true;
  @override
  void initState() {
    lessons = getLessons();



    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    ListTile makeListTile(Lesson lesson) => ListTile(
      contentPadding:
      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      leading: Container(
        padding: EdgeInsets.only(right: 12.0),
        decoration: new BoxDecoration(
            border: new Border(
                right: new BorderSide(width: 1.0, color: Colors.white24))),
        child: Icon(Icons.account_balance_wallet, color: Colors.white),
      ),
      title: Text(
        lesson.title,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

      subtitle: Row(
        children: <Widget>[
          Expanded(
              flex: 1,
              child: Container(
                // tag: 'hero',
                child: LinearProgressIndicator(
                    backgroundColor: Color.fromRGBO(209, 224, 224, 0.2),
                    value: lesson.indicatorValue,
                    valueColor: AlwaysStoppedAnimation(Colors.green)),
              )),
          Expanded(
            flex: 4,
            child: Padding(
                padding: EdgeInsets.only(left: 10.0),
                child: Text("Ksh. "+lesson.price.toString(),
                    style: TextStyle(color: Colors.white))),
          )
        ],
      ),
      trailing:
      Text(lesson.level,
          style: TextStyle(color: Colors.white)),
      onTap: () {
        // Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //         builder: (context) => DetailPage(lesson: lesson)));
      },
    );

    Card makeCard(Lesson lesson) => Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
        child: makeListTile(lesson),
      ),
    );

    final makeBody = Container(
      // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
      child: loading ?  Center(child: Lottie.asset('assets/nodata.json',height: 200)):ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: lessons.length,
        itemBuilder: (BuildContext context, int index) {
          return makeCard(lessons[index]);
        },
      ),
    );

    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      title:  Text(widget.title),
      actions: <Widget>[
      ],
    );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: topAppBar,
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 10,),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Card(
                  color: Color(0xffe0e0e0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 5,
                  child: Column(
                    children: [

                      SizedBox(
                        height: 10.0,
                      ),
                      InkWell(
                        child: Image(
                          image: AssetImage('assets/mngaroo-logo.jpg'),
                          fit: BoxFit.cover,
                        ),
                          onTap: () => launch('http://www.mngaromtaani.org/')
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: InkWell(
                          child: Text(
                            'http://www.mngaromtaani.org/',
                            style: TextStyle(
                              fontSize: 15,
                              fontFamily: 'PoppinsMedium',
                              color: Color(0xff2a2ad9),
                            ),
                          ),
                          onTap: () => launch('http://www.mngaromtaani.org/')
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          'Mpesa Pay bill -891300',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Colors.green,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          'Account-Mngaro',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),

                      // RaisedButton(
                      //   shape: new RoundedRectangleBorder(
                      //     borderRadius: new BorderRadius.circular(10.0),
                      //
                      //   ),
                      //   color: Theme.of(context).primaryColor,
                      //   onPressed: () {
                      //     showModalBottomSheet(context: context, builder: (BuildContext bc){
                      //       Map fundiData = ModalRoute.of(context).settings.arguments;
                      //       return ModalPayment();
                      //     }
                      //     );
                      //   },
                      //   child: Text(
                      //     'Donate',
                      //     style: TextStyle(
                      //       color: Colors.white,
                      //       fontSize: 14,
                      //       fontFamily: 'PoppinsRegular',
                      //     ),
                      //   ),
                      // ),

                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 10,),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Card(
                  color: Color(0xffe0e0e0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 5,
                  child: Column(
                    children: [

                      SizedBox(
                        height: 10.0,
                      ),
                      InkWell(
                          child: Image(
                            height: 250,
                            image: AssetImage('assets/sprouting.jpeg' ),
                            fit: BoxFit.cover,
                          ),
                          onTap: () => launch('https://www.facebook.com/profile.php?id=100004742147860')
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: InkWell(
                            child: Text(
                              'SPROUTING AGAIN CHILDREN HOME',
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xff2a2ad9),
                              ),
                            ),
                            onTap: () => launch('https://www.facebook.com/profile.php?id=100004742147860')
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          'BANK- NATIONAL BANK KITENGELA BRANCH',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          'ACCOUNT NAME - SPROUTING AGAIN C.B.O',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          'ACCOUNT No. - 01256129015600',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Colors.green,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          'PAYBILL No. - 4077039',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Colors.green,
                          ),
                        ),
                      ),
                      // RaisedButton(
                      //   shape: new RoundedRectangleBorder(
                      //     borderRadius: new BorderRadius.circular(10.0),
                      //
                      //   ),
                      //   color: Theme.of(context).primaryColor,
                      //   onPressed: () {
                      //     showModalBottomSheet(context: context, builder: (BuildContext bc){
                      //       Map fundiData = ModalRoute.of(context).settings.arguments;
                      //       return ModalPayment();
                      //     }
                      //     );
                      //   },
                      //   child: Text(
                      //     'Donate',
                      //     style: TextStyle(
                      //       color: Colors.white,
                      //       fontSize: 14,
                      //       fontFamily: 'PoppinsRegular',
                      //     ),
                      //   ),
                      // ),

                    ],
                  ),
                ),
              ),
            ),


          ],
        ),
      ),
      // bottomNavigationBar: makeBottom,
    );
  }
}

List getLessons() {
  return [
    Lesson(
        title: "Donated",
        level: "12/12/2020",
        indicatorValue: 1.0,
        price: 20,
        content:
        "Start by taking a couple of minutes to read the info in this section. Launch your app and click on the Settings menu.  While on the settings page, click the Save button.  You should see a circular progress indicator display in the middle of the page and the user interface elements cannot be clicked due to the modal barrier that is constructed."),
    Lesson(
        title: "Donated",
        level: "11/12/2020",
        indicatorValue: 1.0,
        price: 50,
        content:
        "Start by taking a couple of minutes to read the info in this section. Launch your app and click on the Settings menu.  While on the settings page, click the Save button.  You should see a circular progress indicator display in the middle of the page and the user interface elements cannot be clicked due to the modal barrier that is constructed."),
    Lesson(
        title: "Donated",
        level: "09/12/2020",
        indicatorValue: 1.0,
        price: 30,
        content:
        "Start by taking a couple of minutes to read the info in this section. Launch your app and click on the Settings menu.  While on the settings page, click the Save button.  You should see a circular progress indicator display in the middle of the page and the user interface elements cannot be clicked due to the modal barrier that is constructed."),

    Lesson(
        title: "Donated",
        level: "01/12/2020",
        indicatorValue: 1.0,
        price: 50,
        content:
        "Start by taking a couple of minutes to read the info in this section. Launch your app and click on the Settings menu.  While on the settings page, click the Save button.  You should see a circular progress indicator display in the middle of the page and the user interface elements cannot be clicked due to the modal barrier that is constructed.  ")
  ];
}

class Payment {
  final String amount;
  final String ujenzi;
  final String phone;
  final bool calling;

  Payment(this.amount, this.ujenzi,this.phone,this.calling);
}

class ModalPayment extends StatefulWidget {



  @override
  _ModalPaymentState createState() => _ModalPaymentState();
}

class _ModalPaymentState extends State<ModalPayment> {

  bool loading = false;
  bool show = false;
  final _formKey_payment = GlobalKey<FormState>();
  final amount = new TextEditingController();
  final phone = new TextEditingController();
  int _needCover = 0;
  int vlaue = 0;
  int _wallet = 0;
  bool visible = false;
  bool visible_af = false;
  bool visible_kh  = false;
  bool visible_afc = false;
  bool visible_gb = false;
  bool visible_afriCF= false;
  bool visible_ccf = false;
  bool visible_amhf = false;
  bool visible_wallet = false;
  bool visible_krt = false;
  bool changeScreen = false;

  bool showing =true;
  @override
  Widget build(BuildContext context) {


    return Container(
      height: 900,
      color: Colors.white,
      child:
        Container(
        decoration: new BoxDecoration(
            color: Colors.white,
            borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(20.0),
                topRight: const Radius.circular(20.0))),
        child:
        show? ListView(
          children: <Widget>[
            SizedBox(height: 30,),
            Padding(
              padding: const EdgeInsets.only(left: 30),
              child: Text('Check Payment',
                style: TextStyle(fontWeight: FontWeight.w500),),
            ),
            SizedBox(
              height: 20,
            ),
            Divider(),

            Container(

              margin: EdgeInsets.only(left: 30, right: 30,top: 10),
              child:  Padding(
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Center(
                  child: loading ? spinKit() :
                  SizedBox(
                    width: 300.0,
                    height: 40.0,
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Colors.blue[700],
                      child: Text("Proceed "),
                      onPressed: () async {
                        String phoneNumber = '';
                        setState(() {
                          loading = true;
                        });

                        if(visible ==false){
                          SharedPreferences prefs = await SharedPreferences.getInstance();
                          //Return String
                          String phm = prefs.getString('phoneNo');
                          phoneNumber = phm;
                        } else{
                          phoneNumber = phone.text;
                        }
                        checkPay(phoneNumber);

                      },
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0),

                      ),
                    ),
                  ),

                ),
              ),
              decoration: BoxDecoration(
                borderRadius:
                BorderRadius.all(Radius.circular(10.0)),
              ),
            ),

          ],
        ):
        ListView(
          children: <Widget>[
            SizedBox(height: 30,),
            Padding(
              padding: const EdgeInsets.only(left: 30),
              child: Text('Donation Mode',
                style: TextStyle(fontWeight: FontWeight.w500),),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  LabeledRadio(
                    label: 'e-Wallet',
                    padding:
                    const EdgeInsets.symmetric(horizontal: 5.0),
                    value: 0,
                    groupValue: _wallet,
                    onChanged: (int val) {
                      setState(() {
                        _wallet = val;
                        visible_wallet = false;
                      });
                    },
                  ),
                  LabeledRadio(
                    label: 'M-Pesa ',
                    padding:
                    const EdgeInsets.symmetric(horizontal: 5.0),
                    value: 1,
                    groupValue: _wallet,
                    onChanged: (int val) {
                      setState(() {
                        _wallet = val;
                        visible_wallet = true;
                      });
                    },
                  ),
                ],
              ),
            ),

            SizedBox(height: 30,),
            !visible_wallet? SizedBox():Padding(
              padding: const EdgeInsets.only(left: 30),
              child: Text('STK Push Payment Option',
                style: TextStyle(fontWeight: FontWeight.w500),),
            ),
            SizedBox(
              height: 20,
            ),
            !visible_wallet? SizedBox():Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  LabeledRadio(
                    label: 'My Number',
                    padding:
                    const EdgeInsets.symmetric(horizontal: 5.0),
                    value: 0,
                    groupValue: _needCover,
                    onChanged: (int val) {
                      setState(() {
                        _needCover = val;
                        visible = false;
                      });
                    },
                  ),
                  LabeledRadio(
                    label: 'Other ',
                    padding:
                    const EdgeInsets.symmetric(horizontal: 5.0),
                    value: 1,
                    groupValue: _needCover,
                    onChanged: (int val) {
                      setState(() {
                        _needCover = val;
                        visible = true;
                      });
                    },
                  ),
                ],
              ),
            ),
            Divider(),
            SizedBox(height: 20,),
            Padding(
              padding: const EdgeInsets.only(left: 30,right: 50),
              child: TextFormField(
                controller: amount,
                keyboardType: TextInputType.number,
                validator: (value) => validatePhone(value),
                decoration: InputDecoration(
                  labelText: 'Amount',
                  hintText: 'eg. 100',
                  filled: true,
                  fillColor: Colors.white,
                  labelStyle:
                  TextStyle(color: Colors.blue, fontSize: 18.0),
                  focusedBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.blue),
                  ),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.blue),
                      borderRadius: BorderRadius.circular(10)),),
              ),
            ),

            SizedBox(height: 20,),
            !visible? SizedBox():Padding(
              padding: const EdgeInsets.only(left: 30,right: 50),
              child: TextFormField(
                controller: phone,
                keyboardType: TextInputType.number,
                validator: (value) => validatePhone(value),
                decoration: InputDecoration(
                  labelText: 'Phone Number',
                  hintText: 'eg. 2547********',
                  filled: true,
                  fillColor: Colors.white,
                  labelStyle:
                  TextStyle(color: Colors.blue, fontSize: 18.0),
                  focusedBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.blue),
                  ),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.blue),
                      borderRadius: BorderRadius.circular(10)),),
              ),
            ),
            Container(

              margin: EdgeInsets.only(left: 30, right: 30,top: 10),
              child:  Padding(
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Center(
                  child: loading ? spinKit() :
                  SizedBox(
                    width: 300.0,
                    height: 40.0,
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Theme.of(context).primaryColor,
                      child: Text("Donate "),
                      onPressed: () async {

                        String phoneNumber = '';
                        setState(() {
                          loading = true;
                        });

                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        String user_id = prefs.getString('user');
                        String token = prefs.getString('accessToken');
                        if(visible ==false){
                          //Return String
                          String phm = prefs.getString('phone');
                          phoneNumber = phm;
                        } else{
                          phoneNumber = phone.text;
                        }

                        pay(phoneNumber,amount.text, user_id, token);

                      },
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0),

                      ),
                    ),
                  ),

                ),
              ),
              decoration: BoxDecoration(
                borderRadius:
                BorderRadius.all(Radius.circular(10.0)),
              ),
            ),

           SizedBox(),
          ],
        ),
      ),
    );
  }
  /// Valid phone number
  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }
  void checkPay(phone) async {

    AllApi resp = await checkPayment(formatPhoneNumber(phone));
    print(resp.code);

    if (resp != null && resp.code != 400 ) {
//        await instance.fetchCitizenPost(id, token);

      print(resp.data['status'].toString());
      if(resp.data['status'] == true) {

        // if(widget.payment.calling == true){
        //   setState(() {
        //     loading = false;
        //     Navigator.pop(context);
        //   });
        //
        //   // _service.call(widget.payment.phone);
        // }
        // else{
        //
        //   AllApi res = await checkBlacklist(widget.payment.ujenzi);
        //   print(res.code);
        //
        //   if (res != null && res.code != 400 ) {
        //     setState(() {
        //       loading = false;
        //       show = false;
        //       Navigator.of(context).pop();
        //     });
        //     showDialog<void>(
        //       context: context,
        //       builder: (BuildContext context) {
        //         return AlertDialog(
        //           title: Text('Fundi'),
        //           content:  Text('${res.data['message']}'),
        //           actions: <Widget>[
        //             FlatButton(
        //               child: Text('Ok'),
        //               onPressed: () {
        //                 Navigator.of(context).pop();
        //               },import 'package:flutter/material.dart';
        //             ),
        //           ],
        //         );
        //       },
        //     );
        //   }
        //
        // }
      } else {
        showToast('Payment Not Received');
        setState(() {

          loading = false;
        });
      }


    }
    else {
      setState(() {

        loading = false;
      });
      showToast('Unable to to get response');
    }

  }

  void pay(String phone, String amount, String user, String token) async {
    if (phone != null) {
      AllApi response = await makePayment(formatPhoneNumber(phone), amount, 'Donate', user, token);
      print(response.code);

      if (response != null && response.code != 400 ) {
//        await instance.fetchCitizenPost(id, token);
        setState(() {
          loading = false;
          show = true;
        });
      }
      else {
        setState(() {

          show = false;
          loading = false;
        });
        showToast('Unable to lock Mpesa STK Push, Use valid Number');
      }
    }
  }
}