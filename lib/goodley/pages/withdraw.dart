

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/models/all_api.dart';
import 'package:travelx_v1/goodley/network/otp_api.dart';
import 'package:travelx_v1/goodley/network/payment.dart';
import 'package:travelx_v1/goodley/start.dart';
import 'package:travelx_v1/goodley/utils/common_utils.dart';
import 'package:travelx_v1/goodley/utils/spinner.dart';
import 'package:travelx_v1/goodley/widgets/radio_button.dart';
import 'package:travelx_v1/goodley/widgets/toast.dart';
import 'package:travelx_v1/size_config.dart';

class WithDrawAccount extends StatefulWidget {
  @override
  _WithDrawState createState() => _WithDrawState();
}

class _WithDrawState extends State<WithDrawAccount> {


  bool loading = false;
  bool show = false;

  String user;
  String token;
  final amount = new TextEditingController();
  final formKey_withdraw= GlobalKey<FormState>();
  final reason = new TextEditingController();
  bool _needCover = true;
  bool visible = false;
  bool changeScreen = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'APPLY',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: false,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Form(
            key: formKey_withdraw,
            child: Column(

              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                SizedBox(
                  height: 25.0,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(20,4,50,4),
                  child: Text(
                    'Feature will only be activated after a year of active saving:',
                    style: TextStyle(
                      color: Theme.of(context).accentColor,
                      fontSize: 2.58 * SizeConfig.textMultiplier,
                      fontFamily: 'PoppinsRegular',
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),

               Container(
                 height: MediaQuery.of(context).size.height/2,
                 child:
                 ListView(
                   children: <Widget>[
                     SizedBox(height: 20,),
                     Padding(
                       padding: const EdgeInsets.only(left: 10),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: <Widget>[
                           LabeledRadio(
                             label: 'My Number',
                             padding:
                             const EdgeInsets.symmetric(horizontal: 5.0),
                             value: true,
                             groupValue: _needCover,
                             onChanged: (bool val) {
                               setState(() {
                                 _needCover = val;
                                 visible = false;
                               });
                             },
                           ),
                         ],
                       ),
                     ),
                     Divider(),
                     SizedBox(
                       height: 15.0,
                     ),
                     Padding(
                       padding: const EdgeInsets.only(left: 30,right: 50),
                       child: TextFormField(
                         controller: amount,
                         keyboardType: TextInputType.number,
                         validator: (value) => validatePhone(value),
                         decoration: InputDecoration(
                           labelText: 'Amount',
                           hintText: 'eg. 100',
                           filled: true,
                           fillColor: Colors.white,
                           labelStyle:
                           TextStyle(color: Theme.of(context).primaryColor, fontSize: 18.0),
                           focusedBorder: const OutlineInputBorder(
                             borderSide: const BorderSide(color: Colors.blue),
                           ),
                           border: OutlineInputBorder(
                               borderSide: BorderSide(color: Colors.blue),
                               borderRadius: BorderRadius.circular(10)),),
                       ),
                     ),
                     SizedBox(
                       height: 10,
                     ),
                     Padding(
                       padding: const EdgeInsets.only(left: 30,right: 50),
                       child: TextFormField(
                         maxLines: 8,
                         controller: reason,
                         keyboardType: TextInputType.text,
                         validator: (value) => validatorEmpty(value),
                         decoration: InputDecoration(
                           labelText: 'Reason for Withdrwal',
                           hintText: 'eg. .........',
                           filled: true,
                           fillColor: Colors.white,
                           labelStyle:
                           TextStyle(color: Theme.of(context).primaryColor, fontSize: 18.0),
                           focusedBorder: const OutlineInputBorder(
                             borderSide: const BorderSide(color: Colors.blue),
                           ),
                           border: OutlineInputBorder(
                               borderSide: BorderSide(color: Colors.blue),
                               borderRadius: BorderRadius.circular(10)),),
                       ),
                     ),
                     Container(

                       margin: EdgeInsets.only(left: 30, right: 30,top: 10),
                       child:  Padding(
                         padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                         child: Center(
                           child: loading ? spinKit() :
                           SizedBox(
                             width: 300.0,
                             height: 40.0,
                             child: RaisedButton(
                               textColor: Colors.white,
                               color: Theme.of(context).primaryColor,
                               child: Text("WITHDRAW FUNDS "),
                               onPressed: () async {

                                 // String phoneNumber = '';
                                 // setState(() {
                                 //   loading = true;
                                 // });
                                 // SharedPreferences prefs = await SharedPreferences.getInstance();
                                 // user = prefs.getString('user');
                                 // token = prefs.getString('accessToken');
                                 // if(visible ==false){
                                 //   //Return String
                                 //  String phm = prefs.getString('phone');
                                 //   phoneNumber = phm;
                                 // }
                                 //
                                 // pay(phoneNumber,amount.text,user, token);
                                 showToast('Feature Not Activated');
                               },
                               shape: new RoundedRectangleBorder(
                                 borderRadius: new BorderRadius.circular(10.0),

                               ),
                             ),
                           ),

                         ),
                       ),
                       decoration: BoxDecoration(
                         borderRadius:
                         BorderRadius.all(Radius.circular(10.0)),
                       ),
                     ),

                   ],
                 ),
               )
              ],
            ),
          )
        ],
      ),
    );
  }

  /// Valid phone number
  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }
  void checkPay(phone) async {

    AllApi resp = await checkPayment(formatPhoneNumber(phone));
    print(resp.code);

    if (resp != null && resp.code != 400 ) {
//        await instance.fetchCitizenPost(id, token);

      print(resp.data['status'].toString());
      // if(resp.data['status'] == true) {
      //
      //   if(widget.payment.calling == true){
      //     setState(() {
      //       loading = false;
      //       Navigator.pop(context);
      //     });
      //
      //   } else{
      //
      //     // AllApi res = await checkBlacklist(widget.payment.ujenzi);
      //     // print(res.code);
      //     //
      //     // if (res != null && res.code != 400 ) {
      //     //   setState(() {
      //     //     loading = false;
      //     //     show = false;
      //     //     Navigator.of(context).pop();
      //     //   });
      //     //   showDialog<void>(
      //     //     context: context,
      //     //     builder: (BuildContext context) {
      //     //       return AlertDialog(
      //     //         title: Text('Fundi'),
      //     //         content:  Text('${res.data['message']}'),
      //     //         actions: <Widget>[
      //     //           FlatButton(
      //     //             child: Text('Ok'),
      //     //             onPressed: () {
      //     //               Navigator.of(context).pop();
      //     //             },
      //     //           ),
      //     //         ],
      //     //       );
      //     //     },
      //     //   );
      //     // }
      //
      //   }
      // }
      // else {
      //   showToast('Payment Not Received');
      //   setState(() {
      //
      //     loading = false;
      //   });
      // }


    }
    else {
      setState(() {

        loading = false;
      });
      showToast('Unable to to get response');
    }

  }

//   void withDrawFunds(String phone, String amount, String user, String token) async {
//
//     print(amount);
//     AllApi response = await withdraw(formatPhoneNumber(phone), amount, 'WithDraw', user, token);
//     print(response.code);
//
//       if (response != null && response.code != 400 ) {
// //        await instance.fetchCitizenPost(id, token);
//         setState(() {
//           loading = false;
//           show = true;
//         });
//       }
//       else {
//         setState(() {
//
//           show = false;
//           loading = false;
//         });
//         showToast('Unable to lock Mpesa STK Push, Use valid Number');
//       }
//     }
  }

