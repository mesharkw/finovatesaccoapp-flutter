import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/models/all_api.dart';
import 'package:travelx_v1/goodley/models/banking.dart';
import 'package:travelx_v1/goodley/models/funds.dart';
import 'package:travelx_v1/goodley/network/otp_api.dart';
import 'package:travelx_v1/goodley/network/payment.dart';
import 'package:travelx_v1/goodley/utils/common_utils.dart';
import 'package:travelx_v1/goodley/utils/new_radio.dart';
import 'package:travelx_v1/goodley/utils/spinner.dart';
import 'package:travelx_v1/goodley/widgets/toast.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

import '../../size_config.dart';
import '../start.dart';


//slider Items
class Item1 extends StatelessWidget {
  const Item1({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [
              0.3,
              1
            ],
            colors: [
              Color(0xffffffff),
              Color(0xffffffff),
            ]),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
            Text('Mai Mahiu ',
              style: TextStyle(
                  fontSize: 23.5,
                  color: Colors.black),
          ),
          Text('50 X 100 Plots ',
            style: TextStyle(
                fontSize: 22.0,
                color: Colors.grey[700]),
          ),
          Text('350K ',
            style: TextStyle(
                fontSize: 22.5,
                color: Colors.black),
          ),
          Text('Payment Plan 36 Months ',
            style: TextStyle(
                fontSize: 21.5,
                color: Colors.grey[700]),
          ),
        ],
      ),
    );
  }
}
class Item6 extends StatelessWidget {
  const Item6({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.3, 1],
            colors: [Color(0xffffffff),Color(0xffffffff),]),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Kithimani ',
            style: TextStyle(
                fontSize: 23.5,
                color: Colors.black),
          ),
          Text('50 X 100 Plots ',
            style: TextStyle(
                fontSize: 22.0,
                color: Colors.grey[700]),
          ),
          Text('350K ',
            style: TextStyle(
                fontSize: 22.5,
                color: Colors.black),
          ),
          Text('Payment Plan 36 Months ',
            style: TextStyle(
                fontSize: 21.5,
                color: Colors.grey[700]),
          ),
        ],
      ),
    );
  }
}
class Item7 extends StatelessWidget {
  const Item7({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.3, 1],
            colors: [Color(0xffffffff),Color(0xffffffff),]),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Nanyuki ',
            style: TextStyle(
                fontSize: 23.5,
                color: Colors.black),
          ),
          Text('50 X 100 Plots ',
            style: TextStyle(
                fontSize: 22.0,
                color: Colors.grey[700]),
          ),
          Text('180K ',
            style: TextStyle(
                fontSize: 22.5,
                color: Colors.black),
          ),
          Text('Payment Plan 36 Months ',
            style: TextStyle(
                fontSize: 21.5,
                color: Colors.grey[700]),
          ),
        ],
      ),
    );
  }
}

class Item3 extends StatelessWidget {
  const Item3({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.3, 1],
            colors: [Color(0xffffffff), Color(0xffffffff)]),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('JujaFarm-Athi ',
            style: TextStyle(
                fontSize: 23.5,
                color: Colors.black),
          ),
          Text('40 X 90 Plots ',
            style: TextStyle(
                fontSize: 22.0,
                color: Colors.grey[700]),
          ),
          Text('450K ',
            style: TextStyle(
                fontSize: 22.5,
                color: Colors.black),
          ),
          Text('Payment Plan 36 Months ',
            style: TextStyle(
                fontSize: 21.5,
                color: Colors.grey[700]),
          ),
        ],
      ),
    );
  }
}

class ShareWallet extends StatefulWidget {
  ShareWallet({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ShareWalletState createState() => _ShareWalletState();
}

class _ShareWalletState extends State<ShareWallet> {
  List<FundsModel> lessons = [];
  String name = "";
  String user_id = "";
  String token = "";
  String email = "";
  String dob = "";
  String idNumber = "";
  String career = "";
  String phoneNumber = "";
  double total = 0.0;
  bool loading = true;
  bool loaded = true;
  bool kodi = false;
  DateTime date;
  List loanType = [];

  List otherAcc = [];
  bool status = false;
  String Btype = "";
  DateFormat dateFormat = DateFormat("yyyy-MM-dd");

  List<Bank> statemet;
  String savings ="";
  String savingsNo ="";
  List fosaAcc = [];
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  Future<Null> refreshList() async {
    refreshKey.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 3));
    lessons.clear();
    total = 0.00;

    fetchWallet(phoneNumber, token, Btype);
    // fetchOtherWallet(phoneNumber, token, Btype);
    setState(() {});

    return null;
  }

  void getSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      Btype = '1a862df26f6943997cef90233877a4fe';
      phoneNumber = prefs.getString('phone');
      user_id = prefs.getString('user');
      token = prefs.getString('accessToken');
      kodi = prefs.getBool('kodi');
    });
    print('kodi' + kodi.toString());

    fetchWallet(phoneNumber, token, Btype);
    // fetchOtherWallet(phoneNumber, token, Btype);
  }

  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }
  fetchWallet(phone, token, btype) async {
    print(token);
    print('******************Data');

    var response = await fosaAccount(formatPhoneNumber(phone), token, btype);
    if( response != null) {
      this.fosaAcc = response;
      for (var i = 0; i < this.fosaAcc.length; i++) {
        if(this.fosaAcc[i].type == "Share Capital"){
          setState(() {

            this.savings = this.fosaAcc[i].bal.toString();
            total =  total + double.parse(fosaAcc[i].bal.toString());
            this.savingsNo = this.fosaAcc[i].no;

            print('balance'+this.fosaAcc[i].bal.toString());
            print(this.fosaAcc[i].no);
            this.getStatements(formatPhoneNumber(phone), this.fosaAcc[i].no, btype,token);

          });
        }

      }
      print('******************8');

    }
  }

  Future<void> getStatements(phoneNo, refaccountno, btype,apikey) async {
    if (btype != null) {
      var response = await bankAccount(phoneNo, refaccountno, btype,apikey);

      if (response != null || response.isNotEmpty ) {
        this.statemet = response;
        for (var i = 0; i < this.statemet.length; i++) {
          date =  DateTime.parse(response[i].date_posted);
          FundsModel fundsModel =  new FundsModel(
            type: response[i].description,
            amount: response[i].money_in =='0.00'? response[i].money_out: response[i].money_in,
            created_at: response[i].date_posted,
            phoneNumber: response[i].money_out,
          );
          lessons.add(fundsModel);

        }
        setState(() {
          loaded = true;
          loading = false;

        });
      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }
  fetchOtherWallet(phone, token, btype) async {
    print(token);
    var response = await accountUser(formatPhoneNumber(phone), token, btype);
    if( response != null) {
      this.otherAcc = response.data;
      print('******************8');
      print(this.otherAcc);

      var sum = 0.0;
      for (var i = 0; i < this.otherAcc.length; i++) {
        print('Account'+this.otherAcc[i]['type']);
        if(this.otherAcc[i]['type'].contains("Share")){
          if(this.otherAcc[i]['status'].contains("Pending")){
            setState(() {
              // this.savings = this.fosaAcc[i].bal.toString();
              total = 0.0;
              loaded = true;

            });
          } else {
            sum += (-double.parse(this.otherAcc[i]['bal'].toString()));
            print('Sum' + sum.toString());
            setState(() {
              // this.savings = this.fosaAcc[i].bal.toString();
              total = sum;
              loaded = true;
              this.getStatements(
                  formatPhoneNumber(phone), this.otherAcc[i]['no'], btype,
                  token);
            });
          }
        }

        // print(this.otherAcc[i].bal);
      }
      print('******************8');
      print(this.otherAcc);

    }
  }

  @override
  void initState() {
    super.initState();
    getSharedPref();
  }

  @override
  Widget build(BuildContext context) {
    ListTile makeListTile(FundsModel lesson) => ListTile(
          contentPadding:
              EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          leading: Container(
            padding: EdgeInsets.only(right: 12.0),
            decoration: new BoxDecoration(
                border: new Border(
                    right: new BorderSide(width: 1.0, color: Colors.white24))),
            child: Icon(Icons.account_balance_wallet, color: Colors.white),
          ),
          title: Text(
            lesson.transaction_id,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
          // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

          subtitle: Row(
            children: <Widget>[
              Expanded(
                flex: 4,
                child: Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text("Ksh. " + lesson.amount,
                        style: TextStyle(color: Colors.white))),
              )
            ],
          ),
          trailing: Text(lesson.mpesaReceiptNumber,
              style: TextStyle(color: Colors.white)),
          onTap: () {
            // Navigator.push(
            //     context,
            //     MaterialPageRoute(
            //         builder: (context) => DetailPage(lesson: lesson)));
          },
        );

    Card makeCard(FundsModel lesson) => Card(
          elevation: 8.0,
          margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
          child: Container(
            decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
            child: makeListTile(lesson),
          ),
        );

    final makeBody = Container(
      height: MediaQuery.of(context).size.height / 1.64,
      decoration: BoxDecoration(
        color: Color(0xFF002642),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(8.0, 20, 8, 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  'Share Capital Wallet History',
                  style: TextStyle(
                    color: Theme.of(context).accentColor,
                    fontSize: 2.28 * SizeConfig.textMultiplier,
                    fontFamily: 'PoppinsRegular',
                  ),
                ),
                // Icon(Icons.arrow_forward_rounded, size: 30, color: Colors.white,),
              ],
            ),
          ),
          Expanded(
            child: loading
                ? Lottie.asset(
                    'assets/nodata.json',
                  )
                : ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: lessons.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width/4 -10,
                                  child: Text(lessons[index].created_at,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.w400)),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Text( lessons[index].type ,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w600)),


                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: Column(
                                children: [
                                  Text(lessons[index].amount+ ' Kes',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15.0,
                                          fontWeight: FontWeight.w600)),
                                  Text(lessons[index].type ,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.w400))
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Divider(
                          color: Colors.white
                      )
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );

    final topAppBar = AppBar(
      iconTheme: IconThemeData(
        color: Colors.black, //change your color here
      ),
      backgroundColor: Colors.white,
      elevation: 0,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
              width: 30.0,
              height: 30.0,
              decoration: new BoxDecoration(
                  image: new DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage('assets/fsac.jpeg'),
              ))),
        ],
      ),
      centerTitle: false,
    );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: topAppBar,
      body: loaded
          ? RefreshIndicator(
              key: refreshKey,
              onRefresh: refreshList,
              color: Theme.of(context).primaryColor,
              child: SingleChildScrollView(
                child: Container(
                  child: Column(
                    children: [
                      Text(
                        "KES ${total.toString()}",
                        style: TextStyle(
                          color: Color(0xFF002642),
                          fontWeight: FontWeight.bold,
                          fontSize: 4.48 * SizeConfig.textMultiplier,
                          fontFamily: 'PoppinsRegular',
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          RaisedButton(
                            onPressed: () {
                              bool status = false;
                              showModalBottomSheet(
                                  context: context,
                                  builder: (BuildContext bc) {
                                    Map fundiData = ModalRoute.of(context)
                                        .settings
                                        .arguments;
                                    return DepositPayment(
                                        payment: Payment(phoneNumber));
                                  });
                            },
                            child: RichText(
                              text: TextSpan(
                                children: [
                                  WidgetSpan(
                                    child: Icon(
                                      Icons.wrap_text,
                                      size: 14,
                                      color: Colors.white,
                                    ),
                                  ),
                                  TextSpan(
                                    text: " Deposit",
                                  ),
                                ],
                              ),
                            ),
                            color: Theme.of(context).primaryColor,
                          ),
                          SizedBox(
                            width: 20.0,
                          ),
                          SizedBox(
                            height: 16,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      makeBody,
                      SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                ),
              ),
            )
          : Center(child: CircularProgressIndicator()),
      // bottomNavigationBar: makeBottom,
    );
  }
}

class Payment {
  final String phoneNumber;

  Payment(this.phoneNumber);
}

class ModalPayment extends StatefulWidget {
  // Declare a field that holds the Person data
  final Payment payment;

  // In the constructor, require a Person
  ModalPayment({Key key, @required this.payment}) : super(key: key);
  @override
  _ModalPaymentState createState() => _ModalPaymentState();
}

class _ModalPaymentState extends State<ModalPayment> {
  bool loading = false;
  bool show = false;
  final _formKey_payment = GlobalKey<FormState>();
  final phone = new TextEditingController();
  final amount = new TextEditingController();
  final code = new TextEditingController();
  final pass = new TextEditingController();
  int _needCover = 0;
  int _wallet = 0;
  bool visible = false;
  bool visible_wallet = false;

  String  project;
  String  projectLoan;
  bool changeScreen = false;
  List fosaAcc = [];
  String token = "";
  String phoneNumber = "";
  String Btype = '1a862df26f6943997cef90233877a4fe';

  String loanName = '';
  String desc = '';
  String deposit = '';
  String requirement = '';
  String maxperiod = '';
  String loanAmount = '';
  String loanId = '';
  String accessToken = '';
  bool otpPage = false;
  bool loader = false;

  List loanType = [];
  String _chosenValue;

  List cardList = [
    Item1(),
    Item3(),
    Item6(),
    Item7(),
  ];
  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }
  int _current = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getSharedPref();
  }

  fetchWallet(phone, token, btype) async {
    print(token);
    var response = await fosaAll(formatPhoneNumber(phone), token, btype);
    if( response != null) {
      print('******************8');
      print('****************FOSA'+ this.fosaAcc.toString());
      setState(() {
        this.fosaAcc = response.data;
      });

    }
  }

  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }


  void getSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    setState(() {});

    // token = prefs.getString('accessToken');
    token = '627bb5278ce6b800d59c4d8472637e37';
    phoneNumber = prefs.getString('phone');

    // fetchLoans(phoneNumber, token, Btype);
    fetchWallet(phoneNumber, token, Btype);

  }


  applyKodiLoan(loan, period, code) async {
    print(token);
    print(loan);
    print(period);
    var response = await applyLoan(formatPhoneNumber(phoneNumber), amount.text,
        loan, period,"Loan Application",  code, token, Btype);
    if (response.code == 200 || response.code == 201) {
      setState(() {
        loading = false;
        Navigator.pushReplacementNamed(context, '/');
      });
      showToast('Successfully awaiting Approval');
    } else {
      setState(() {
        loading = false;
      });
      showToast('Application not Updated');
    }
  }

  void confirmP(pass) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    accessToken = prefs.getString('accessToken');
    phoneNumber = prefs.getString('phone');

    AllApi resp = await confirmPass(
        formatPhoneNumber(phoneNumber), pass, Btype, accessToken);
    print(resp.code);

    if (resp != null || resp.code != 400 || resp.code != 401 ) {

      Map data = jsonDecode(resp.data);
      print(data['error']);
      print(data['message']);

      if (data['error'] != null && data['message'] == null) {
        setState(() {
          this.otpPage = false;

          loading = false;

          loader = false;
        });
        showToast('Failed Confirmation, Check Password');
      }
      else if (data['message'] != null && data['error'] == null) {
        setState(() {
          this.otpPage = true;
          loading = false;

          loader = false;
        });
      }
    }

  }

  void pays(String amount) async {
    if (amount != null || amount != 0) {
      setState(() {
        this.show = true;
        this.otpPage = false;
      });
    } else {
      this.show = false;
    }
  }
  //widget Loans
  Widget DropDown(List data, String text){
    if(data!=null)
    {
      return DropdownButton(
        items: data.map((item) {
          return new DropdownMenuItem(
            child: new Text(
              item['type'],
              style: TextStyle(fontSize: 15.0),
            ),
            value: item['no'],
          );
        }).toList(),
        hint: Text(
          text,
          style: TextStyle(
              color: Colors.black45,
              fontSize: 12.0
          ),),
        onChanged: (newVal) {
          setState(() {
            _chosenValue = newVal;
            print('chosen'+_chosenValue);
            // customerid = newVal;
            // print('customrid:' + customerid.toString());
          });
        },
        value: _chosenValue,
      );
    }
    else{
      return new Center(
        child: CircularProgressIndicator(),
      );
    }
  }
  @override
  Widget build(BuildContext context) {
    visible_wallet = true;
    return Container(
      child: show
          ? otpPage
              ? ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 30),
                      child: Text(
                        'Enter OTP Code',
                        style: TextStyle(fontWeight: FontWeight.w500),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Divider(),
                    Padding(
                      padding: const EdgeInsets.only(left: 30, right: 50),
                      child: TextFormField(
                        controller: code,
                        keyboardType: TextInputType.number,
                        validator: (value) => validatePhone(value),
                        decoration: InputDecoration(
                          labelText: 'OTP code',
                          hintText: '',
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle:
                              TextStyle(color: Colors.blue, fontSize: 18.0),
                          focusedBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.blue),
                          ),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue),
                              borderRadius: BorderRadius.circular(10)),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 30, right: 30, top: 10),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                        child: Center(
                          child: SizedBox(
                            width: 300.0,
                            height: 40.0,
                            child: RaisedButton(
                              textColor: Colors.white,
                              color: Theme.of(context).primaryColor,
                              child: Text("Confirm "),
                              onPressed: () async {
                                String phoneNumber = '';

                                print(loanId);
                                print(maxperiod);
                                applyKodiLoan(loanId, maxperiod, code.text);
                              },
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                              ),
                            ),
                          ),
                        ),
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                    ),
                  ],
                )
              : ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 30),
                      child: Text(
                        'Confirm Transaction',
                        style: TextStyle(fontWeight: FontWeight.w500),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Divider(),
                    Padding(
                      padding: const EdgeInsets.only(left: 30, right: 50),
                      child: TextFormField(
                        controller: pass,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        validator: (value) => validatePhone(value),
                        decoration: InputDecoration(
                          labelText: 'Pin/Password',
                          hintText: 'eg. ',
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle:
                              TextStyle(color: Colors.blue, fontSize: 18.0),
                          focusedBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.blue),
                          ),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue),
                              borderRadius: BorderRadius.circular(10)),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 30, right: 30, top: 10),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                        child: Center(
                          child: loader
                              ? spinKit()
                              : SizedBox(
                                  width: 300.0,
                                  height: 40.0,
                                  child: RaisedButton(
                                    textColor: Colors.white,
                                    color: Theme.of(context).primaryColor,
                                    child: Text("Confirm "),
                                    onPressed: () async {
                                      String phoneNumber = '';
                                      setState(() {
                                        loader = true;
                                      });
                                      confirmP(pass.text);
                                    },
                                    shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(10.0),
                                    ),
                                  ),
                                ),
                        ),
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                    ),
                  ],
                )
          : ListView(
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: Text(
                    'Share Capital Mode',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                      fontFamily: 'PoppinsMedium',
                    ),
                  ),
                ),
                SizedBox(
                  height: 1,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      visible_wallet
                          ? SizedBox()
                          : LabeledRadio(
                              label: 'Deposit',
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5.0),
                              value: 0,
                              groupValue: _wallet,
                              onChanged: (int val) {
                                setState(() {
                                  _wallet = val;
                                  visible_wallet = false;
                                });
                              },
                            ),
                    ],
                  ),
                ),
                SizedBox(height: 1,),
                visible_wallet
                    ? SizedBox():Padding(
                  padding: EdgeInsets.fromLTRB(20,1,20,10),
                  child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.black45,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(6))
                    ),
                    height: 7.8 * SizeConfig.heightSizeMultiplier,
                    width: MediaQuery.of(context).size.width- 10,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: DropdownButtonHideUnderline(
                        child: DropDown(this.fosaAcc, 'Select Account'),
                      ),
                    ),
                  ),
                ),

                visible_wallet
                    ? SizedBox()
                    : Padding(
                        padding: const EdgeInsets.only(left: 30),
                        child: Text(
                          'STK Push Payment Option',
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                      ),
                visible_wallet
                    ? SizedBox()
                    : SizedBox(
                        height: 10,
                      ),
                visible_wallet
                    ? SizedBox()
                    : Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            LabeledRadio(
                              label: 'My Number',
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5.0),
                              value: 0,
                              groupValue: _needCover,
                              onChanged: (int val) {
                                setState(() {
                                  _needCover = val;
                                  visible = false;
                                });
                              },
                            ),
                            LabeledRadio(
                              label: 'Other ',
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5.0),
                              value: 1,
                              groupValue: _needCover,
                              onChanged: (int val) {
                                setState(() {
                                  _needCover = val;
                                  visible = true;
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 30, right: 50),
                  child: TextFormField(
                    controller: amount,
                    keyboardType: TextInputType.number,
                    validator: (value) => validatePhone(value),
                    decoration: InputDecoration(
                      labelText: 'Amount',
                      hintText: 'eg. 100',
                      filled: true,
                      fillColor: Colors.white,
                      labelStyle: TextStyle(color: Colors.blue, fontSize: 18.0),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.blue),
                      ),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue),
                          borderRadius: BorderRadius.circular(10)),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                !visible
                    ? SizedBox()
                    : Padding(
                        padding: const EdgeInsets.only(left: 30, right: 50),
                        child: TextFormField(
                          controller: phone,
                          keyboardType: TextInputType.number,
                          validator: (value) => validatePhone(value),
                          decoration: InputDecoration(
                            labelText: 'Phone Number',
                            hintText: 'eg. 2547********',
                            filled: true,
                            fillColor: Colors.white,
                            labelStyle:
                                TextStyle(color: Colors.blue, fontSize: 18.0),
                            focusedBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.blue),
                            ),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue),
                                borderRadius: BorderRadius.circular(10)),
                          ),
                        ),
                      ),
                visible_wallet
                    ? SizedBox()
                    : Container(
                        margin: EdgeInsets.only(left: 30, right: 30, top: 10 , bottom: 20),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                          child: Center(
                            child: loading
                                ? spinKit()
                                : SizedBox(
                                    width: 300.0,
                                    height: 40.0,
                                    child: RaisedButton(
                                      textColor: Colors.white,
                                      color: Theme.of(context).primaryColor,
                                      child: Text("Deposit "),
                                      onPressed: () async {
                                        String phoneNumber = '';
                                        setState(() {
                                          loading = true;
                                        });
                                        SharedPreferences prefs =
                                            await SharedPreferences
                                                .getInstance();
                                        String user_id =
                                            prefs.getString('user');
                                        String token =
                                            prefs.getString('accessToken');
                                        if (visible == false) {
                                          //Return String
                                          String phm = prefs.getString('phone');
                                          phoneNumber = phm;
                                        } else {
                                          phoneNumber = phone.text;
                                        }

                                        pay(phoneNumber, amount.text, user_id,
                                            token);
                                      },
                                      shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(10.0),
                                      ),
                                    ),
                                  ),
                          ),
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        ),
                      ),
                SizedBox(
                  height: 20,
                ),
                !visible_wallet
                    ? SizedBox()
                    : Container(
                        margin: EdgeInsets.only(left: 30, right: 30, top: 10),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                          child: Center(
                            child: loading
                                ? spinKit()
                                : SizedBox(
                                    width: 300.0,
                                    height: 40.0,
                                    child: RaisedButton(
                                      textColor: Colors.white,
                                      color: Theme.of(context).primaryColor,
                                      child: Text("APPLY "),
                                      onPressed: () async {
                                        pays(amount.text);
                                      },
                                      shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(10.0),
                                      ),
                                    ),
                                  ),
                          ),
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        ),
                      ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
    );
  }

  /// Valid phone number
  void checkPay(phone) async {
    AllApi resp = await checkPayment(formatPhoneNumber(phone));
    print(resp.code);

    if (resp != null && resp.code != 400) {
//        await instance.fetchCitizenPost(id, token);

      print(resp.data['status'].toString());
      if (resp.data['status'] == true) {
        // if(widget.payment.calling == true){
        //   setState(() {
        //     loading = false;
        //     Navigator.pop(context);
        //   });
        //
        //   // _service.call(widget.payment.phone);
        // }
        // else{
        //
        //   AllApi res = await checkBlacklist(widget.payment.ujenzi);
        //   print(res.code);
        //
        //   if (res != null && res.code != 400 ) {
        //     setState(() {
        //       loading = false;
        //       show = false;
        //       Navigator.of(context).pop();
        //     });
        //     showDialog<void>(
        //       context: context,
        //       builder: (BuildContext context) {
        //         return AlertDialog(
        //           title: Text('Fundi'),
        //           content:  Text('${res.data['message']}'),
        //           actions: <Widget>[
        //             FlatButton(
        //               child: Text('Ok'),
        //               onPressed: () {
        //                 Navigator.of(context).pop();
        //               },
        //             ),
        //           ],
        //         );
        //       },
        //     );
        //   }
        //
        // }
      } else {
        showToast('Payment Not Received');
        setState(() {
          loading = false;
        });
      }
    } else {
      setState(() {
        loading = false;
      });
      showToast('Unable to to get response');
    }
  }

  void pay(String phone, String amount, String user, String token) async {
    print(amount);
    AllApi response = await makePayment(
        formatPhoneNumber(phone), amount, 'rent', user, token);
    print(response.code);

    if (response != null && response.code != 400) {
//        await instance.fetchCitizenPost(id, token);
      setState(() {
        loading = false;
        show = true;
      });

      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => MainStart()));
    } else {
      setState(() {
        show = false;
        loading = false;
      });
      showToast('Unable to lock Mpesa STK Push, Use valid Number');
    }
  }
}

//Deposit
class DepositPayment extends StatefulWidget {
  // Declare a field that holds the Person data
  final Payment payment;

  // In the constructor, require a Person
  DepositPayment({Key key, @required this.payment}) : super(key: key);
  @override
  _DepositPaymentState createState() => _DepositPaymentState();
}

class _DepositPaymentState extends State<DepositPayment> {
  bool loading = false;
  bool show = false;
  final _formKey_payment = GlobalKey<FormState>();
  final phone = new TextEditingController();
  final amount = new TextEditingController();
  final code = new TextEditingController();
  final pass = new TextEditingController();
  int _needCover = 0;
  int _wallet = 0;
  bool visible = false;
  bool visible_wallet = false;
  bool changeScreen = false;
  List fosaAcc = [];
  String token = "";
  String phoneNumber = "";
  String Btype = '1a862df26f6943997cef90233877a4fe';

  String loanName = '';
  String desc = '';
  String requirement = '';
  String maxperiod = '';
  String loanAmount = '';
  String phones = '';
  String loanId = '';
  String accessToken = '';
  bool otpPage = false;
  bool loader = false;
  bool loaders = false;

  String _chosenValue;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getSharedPref();
  }

  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }

  void getSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    setState(() {});

    // token = prefs.getString('accessToken');
    token = '627bb5278ce6b800d59c4d8472637e37';
    phoneNumber = prefs.getString('phone');

    fetchWallet(phoneNumber, token, Btype);
  }

  fetchWallet(phone, token, btype) async {
    print(token);
    var response = await fosaAll(formatPhoneNumber(phone), token, btype);
    if( response != null) {

      print('******************8');
      print('****************FOSA'+ response.data.toString());
      for (var i = 0; i < response.data.length; i++) {

        if(response.data[i]['type'].contains("Share")){
          setState(() {
            // this.fosaAcc.addAll(response.data[i]);
            this.fosaAcc.add(response.data[i]);
          });
        }
      }
    }
  }
  void checkPay(phone) async {

    AllApi resp = await checkPayment(formatPhoneNumber(phone));
    print(resp.code);

    if (resp != null && resp.code != 400 ) {
//        await instance.fetchCitizenPost(id, token);

      print(resp.data['status'].toString());
      if(resp.data['status'] == true) {

        // if(widget.payment.calling == true){
        //   setState(() {
        //     loading = false;
        //     Navigator.pop(context);
        //   });
        //
        //   // _service.call(widget.payment.phone);
        // }
        // else{
        //
        //   AllApi res = await checkBlacklist(widget.payment.ujenzi);
        //   print(res.code);
        //
        //   if (res != null && res.code != 400 ) {
        //     setState(() {
        //       loading = false;
        //       show = false;
        //       Navigator.of(context).pop();
        //     });
        //     showDialog<void>(
        //       context: context,
        //       builder: (BuildContext context) {
        //         return AlertDialog(
        //           title: Text('Fundi'),
        //           content:  Text('${res.data['message']}'),
        //           actions: <Widget>[
        //             FlatButton(
        //               child: Text('Ok'),
        //               onPressed: () {
        //                 Navigator.of(context).pop();
        //               },
        //             ),
        //           ],
        //         );
        //       },
        //     );
        //   }
        //
        // }
      } else {
        showToast('Payment Not Received');
        setState(() {

          loading = false;
        });
      }


    }
    else {
      setState(() {

        loading = false;
      });
      showToast('Unable to to get response');
    }

  }

  void confirmP(pass) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    accessToken = prefs.getString('accessToken');
    print(widget.payment.phoneNumber);

    AllApi resp = await confirmPass(formatPhoneNumber(widget.payment.phoneNumber),pass,Btype,accessToken);
    print(resp.code);


    if (resp != null || resp.code != 400 || resp.code != 401 ) {

      Map data = jsonDecode(resp.data);
      print(data['error']);
      print(data['message']);

      if (data['error'] != null && data['message'] == null) {
        showToast('Failed Confirmation!. Password error, Retry');
        setState(() {
          loading = false;
          loader = false;
        });
      }
      else if (data['message'] != null && data['error'] == null) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setBool('registered', false);
        loading = false;
        loader = false;
        setState(() {
          this.otpPage = true;
        });

        AllApi response = await depoFin(formatPhoneNumber(widget.payment.phoneNumber),
            _chosenValue,amount.text);
        print(response.code);

        if (response != null && response.code != 400 ) {
//        await instance.fetchCitizenPost(id, token);
        }
      }
    }

    else {
      setState(() {

        loading = false;
        loader = false;
      });
      showToast('Pin Combination error, Try again');
    }

  }


  void depositCash() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    accessToken = prefs.getString('accessToken');
    phones = prefs.getString('phone');
    AllApi response = await depoFin(formatPhoneNumber(widget.payment.phoneNumber),
        _chosenValue,amount.text);
    print(response.code);
    if (response != null && response.code != 400 ) {
      AllApi resp = await deposit(formatPhoneNumber(widget.payment.phoneNumber),amount.text,'2','stk_push','Deposit',
          _chosenValue, Btype,accessToken);
      print(resp.code);

      if (resp != null || resp.code != 400 || resp.code != 401 ) {
        setState(() {
          loaders = false;
          Navigator.pushReplacementNamed(context, '/');

        });

      }
      else {
        setState(() {

          loaders = false;
        });
        showToast('Please Try again');
      }

    }
    else {
      setState(() {

        loaders = false;
      });
      showToast('Please Try again');
    }

  }

  void pays(String amount) async {
    print('VALUUEEE' + _chosenValue);
    if (_chosenValue == null || _chosenValue == ''){
      showToast('Account Not selected');
      setState(() {
        loading = false;
      });

    } else {
      if(amount != null || amount != 0){
        this.depositCash();
      }
    }


  }
  //widget Loans
  Widget DropDown(List data, String text){
    print('*****************Data drop' + data.toString());

    if(data!=null)
    {
      return DropdownButton(
        items: data.map((item) {
          print('*****************Data drop' + item.toString());
          print('*****************Data drop' + item.toString());
          return new DropdownMenuItem(
            child: new Text(
              item['type']+' - '+item['no'],
              style: TextStyle(fontSize: 15.0),
            ),
            value: item['no'],
          );
        }).toList(),
        hint: Text(
          text,
          style: TextStyle(
              color: Colors.black45,
              fontSize: 12.0
          ),),
        onChanged: (newVal) {
          setState(() {
            _chosenValue = newVal;
            // customerid = newVal;
            // print('customrid:' + customerid.toString());
          });
        },
        value: _chosenValue,
      );
    }
    else{
      return new Center(
        child: CircularProgressIndicator(),
      );
    }
  }
  @override
  Widget build(BuildContext context) {
    visible_wallet = false;
    return Container(
      child: show
          ? otpPage
          ? ListView(
        children: <Widget>[
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: Text(
              'Enter OTP Code',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 50),
            child: TextFormField(
              controller: code,
              keyboardType: TextInputType.number,
              validator: (value) => validatePhone(value),
              decoration: InputDecoration(
                labelText: 'OTP code',
                hintText: '',
                filled: true,
                fillColor: Colors.white,
                labelStyle:
                TextStyle(color: Colors.blue, fontSize: 18.0),
                focusedBorder: const OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.blue),
                ),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue),
                    borderRadius: BorderRadius.circular(10)),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 30, right: 30, top: 10),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Center(
                child: SizedBox(
                  width: 300.0,
                  height: 40.0,
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    child: Text("Confirm "),
                    onPressed: () async {
                      String phoneNumber = '';

                      depositCash();
                    },
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          ),
        ],
      )
          : ListView(
        children: <Widget>[
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: Text(
              'Confirm Transaction',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 50),
            child: TextFormField(
              controller: pass,
              keyboardType: TextInputType.number,
              obscureText: true,
              validator: (value) => validatePhone(value),
              decoration: InputDecoration(
                labelText: 'Pin/Password',
                hintText: 'eg. ',
                filled: true,
                fillColor: Colors.white,
                labelStyle:
                TextStyle(color: Colors.blue, fontSize: 18.0),
                focusedBorder: const OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.blue),
                ),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue),
                    borderRadius: BorderRadius.circular(10)),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 30, right: 30, top: 10),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Center(
                child: loader
                    ? spinKit()
                    : SizedBox(
                  width: 300.0,
                  height: 40.0,
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    child: Text("Confirm "),
                    onPressed: () async {
                      String phoneNumber = '';
                      setState(() {
                        loader = true;
                      });
                      confirmP(pass.text);
                    },
                    shape: new RoundedRectangleBorder(
                      borderRadius:
                      new BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          ),
        ],
      )
          : ListView(
        children: <Widget>[
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: Text(
              'Share Capital  Mode',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 25,
                fontFamily: 'PoppinsMedium',
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          SizedBox(
            height: 5,
          ),
          SizedBox(
            height: 1,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                visible_wallet
                    ? SizedBox()
                    : LabeledRadio(
                  label: 'Deposit',
                  padding:
                  const EdgeInsets.symmetric(horizontal: 5.0),
                  value: 0,
                  groupValue: _wallet,
                  onChanged: (int val) {
                    setState(() {
                      _wallet = val;
                      visible_wallet = false;
                    });
                  },
                ),
              ],
            ),
          ),

          SizedBox(height: 1,),
          Padding(
            padding: EdgeInsets.fromLTRB(20,1,20,10),
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.black45,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(6))
              ),
              height: 7.8 * SizeConfig.heightSizeMultiplier,
              width: MediaQuery.of(context).size.width- 10,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: DropdownButtonHideUnderline(
                  child: DropDown(this.fosaAcc, 'Select Account'),
                ),
              ),
            ),
          ),

          SizedBox(
            height: 10,
          ),
          visible_wallet
              ? SizedBox()
              : Padding(
            padding: const EdgeInsets.only(left: 30),
            child: Text(
              'STK Push Payment Option',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
          ),
          visible_wallet
              ? SizedBox()
              : SizedBox(
            height: 10,
          ),
          visible_wallet
              ? SizedBox()
              : Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                LabeledRadio(
                  label: 'My Number',
                  padding:
                  const EdgeInsets.symmetric(horizontal: 5.0),
                  value: 0,
                  groupValue: _needCover,
                  onChanged: (int val) {
                    setState(() {
                      _needCover = val;
                      visible = false;
                    });
                  },
                ),
                LabeledRadio(
                  label: 'Other ',
                  padding:
                  const EdgeInsets.symmetric(horizontal: 5.0),
                  value: 1,
                  groupValue: _needCover,
                  onChanged: (int val) {
                    setState(() {
                      _needCover = val;
                      visible = true;
                    });
                  },
                ),
              ],
            ),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 50),
            child: TextFormField(
              controller: amount,
              keyboardType: TextInputType.number,
              validator: (value) => validatePhone(value),
              decoration: InputDecoration(
                labelText: 'Amount',
                hintText: 'eg. 100',
                filled: true,
                fillColor: Colors.white,
                labelStyle: TextStyle(color: Colors.blue, fontSize: 18.0),
                focusedBorder: const OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.blue),
                ),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue),
                    borderRadius: BorderRadius.circular(10)),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          !visible
              ? SizedBox()
              : Padding(
            padding: const EdgeInsets.only(left: 30, right: 50),
            child: TextFormField(
              controller: phone,
              keyboardType: TextInputType.number,
              validator: (value) => validatePhone(value),
              decoration: InputDecoration(
                labelText: 'Phone Number',
                hintText: 'eg. 2547********',
                filled: true,
                fillColor: Colors.white,
                labelStyle:
                TextStyle(color: Colors.blue, fontSize: 18.0),
                focusedBorder: const OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.blue),
                ),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue),
                    borderRadius: BorderRadius.circular(10)),
              ),
            ),
          ),
          visible_wallet
              ? SizedBox()
              : Container(
            margin: EdgeInsets.only(left: 30, right: 30, top: 10),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Center(
                child: loading
                    ? spinKit()
                    : SizedBox(
                  width: 300.0,
                  height: 40.0,
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    child: Text("Deposit "),
                    onPressed: () async {
                      String phoneNumber = '';
                      setState(() {
                        loading = true;
                      });
                      SharedPreferences prefs =
                      await SharedPreferences
                          .getInstance();
                      String user_id =
                      prefs.getString('user');
                      String token =
                      prefs.getString('accessToken');
                      if (visible == false) {
                        //Return String
                        String phm = prefs.getString('phone');
                        phoneNumber = phm;
                      } else {
                        phoneNumber = phone.text;
                      }

                      if (int.parse(amount.text)  < 20) {
                        setState(() {
                          loading = false;
                        });
                        showToast('Minimum Deposit is Ksh. 20/-');
                      } else {

                        pays(amount.text);
                      }
                    },
                    shape: new RoundedRectangleBorder(
                      borderRadius:
                      new BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          !visible_wallet
              ? SizedBox()
              : Container(
            margin: EdgeInsets.only(left: 30, right: 30, top: 10),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Center(
                child: loading
                    ? spinKit()
                    : SizedBox(
                  width: 300.0,
                  height: 40.0,
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    child: Text("Withdraw "),
                    onPressed: () async {
                      pays(amount.text);
                    },
                    shape: new RoundedRectangleBorder(
                      borderRadius:
                      new BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          ),
          SizedBox(),
        ],
      ),
    );
  }

}
