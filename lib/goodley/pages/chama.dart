import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/models/all_api.dart';
import 'package:travelx_v1/goodley/models/counties.dart';
import 'package:travelx_v1/goodley/models/funds.dart';
import 'package:travelx_v1/goodley/network/otp_api.dart';
import 'package:travelx_v1/goodley/network/payment.dart';
import 'package:cool_stepper/cool_stepper.dart';
import 'package:travelx_v1/goodley/utils/spinner.dart';
import 'package:travelx_v1/goodley/widgets/toast.dart';

import '../../size_config.dart';
import '../start.dart';

class Chama extends StatefulWidget {
  Chama({Key key, this.title}) : super(key: key);

  final String title;



  @override
  _SharesState createState() => _SharesState();
}


class _SharesState extends State<Chama> {

  List<FundsModel> lessons = [];
  String name ="";
  String user_id ="";
  String token ="";
  String email="";
  String residence="";
  String dob="";
  String idNumber="";
  String career="";
  String phoneNumber="";
  String member_no="";
  double total = 0.0;
  bool loading = false;
  bool loaded = false;
  DateTime date;
  DateFormat dateFormat = DateFormat("yyyy-MM-dd");

  List<Counties> sAagents;
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  Future<Null> refreshList() async {
    refreshKey.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 3));
    lessons.clear();
    total = 0.00;
    fetchSMS(user_id, token);
    setState(() {

    });

    return null;
  }


  Future<void> getSalesAgents(String btype) async {
    if (btype != null) {
      var response = await getSalesAgent(btype);

      if (response != null || response.isNotEmpty ) {
        this.sAagents = response;
        setState(() {
        });
      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }

  String Btype = '1a862df26f6943997cef90233877a4fe';

  Future<void> getProfile(phoneNumbers,accessToken,btype) async {
    if (btype != null) {
      var response = await profile(phoneNumbers,accessToken,btype);
      print('dataaaaaa'+ response.toString());
      if (response != null || response.code !=400 ) {

        setState(() {
          // phoneNumber = response.data['contact'];
          email = response.data['email'];
          member_no= response.data['member_number'];
          name = response.data['name'];
          residence = response.data['contribution'];



        });
        // this.counties = response;
        // setState(() {
        // });
      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }

  void getSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      phoneNumber = prefs.getString('phone');
      token = prefs.getString('accessToken');

    });

    getSalesAgents(Btype);
    getProfile(phoneNumber,token,Btype);



  }

  fetchSMS(user_id, token) async {
    print(token);
    var response = await checkFunds(user_id,token);
    if( response != null || response.isNotEmpty) {

      for (var i = 0; i < response.length; i++) {
        print(response[i].amount);
        if (response[i].type == 'asset'){
          print(response[i].amount);
          total =  total + double.parse(response[i].amount);
          date =  DateTime.parse(response[i].created_at);
          FundsModel fundsModel =  new FundsModel(
            transaction_id: response[i].transaction_id,
            type: response[i].type,
            amount: response[i].amount,
            created_at: response[i].created_at,
            phoneNumber: response[i].phoneNumber,
            mpesaReceiptNumber: response[i].mpesaReceiptNumber,
          );
          lessons.add(fundsModel);
        }
      }
      setState(() {
        loaded = true;
        loading = false;
      });
      print('sum'+ total.toString());

    }


  }


  submitClubInvest(groupName,location,regNumber,chairName,chairId,chairPin,
      chairPhone,treasurerName,treasurerId,treasurerPin,treasurerPhone,
      secretaryName,secretaryId,secretaryPin,secretaryPhone,
      contribution,agent) async {

    AllApi response = await chamaData(groupName,location,regNumber,chairName,chairId,chairPin,
        chairPhone,treasurerName,treasurerId,treasurerPin,treasurerPhone,
        secretaryName,secretaryId,secretaryPin,secretaryPhone,
        contribution,agent);

    if (response != null && response.code == 200 || response != null && response.code == 201 ) {
      loading = false;
      setState(() {
        showToast('Submitted Successfully');
        Navigator.pushReplacementNamed(context, '/');
      });
    } else {
      setState(() {
        loading = false;
      });
      showToast('Unable to verify.');
    }
    }


    @override
    void initState() {

      super.initState();
      getSharedPref();
    }
  final _formKey = GlobalKey<FormState>();
  String selectedRole = '';
  String selectedCycle= '';
  String agent;
  bool valuefirst = false;
  final TextEditingController _groupNameCtrl = TextEditingController();
  final TextEditingController _LocationCtrl = TextEditingController();
  final TextEditingController _RegNoCtrl = TextEditingController();
  final TextEditingController _chairNameCtrl = TextEditingController();
  final TextEditingController _chairIdCtrl = TextEditingController();
  final TextEditingController _chairPinCtrl = TextEditingController();
  final TextEditingController _chairPhoneCtrl = TextEditingController();
  final TextEditingController _treasurerNameCtrl = TextEditingController();
  final TextEditingController _treasurerIdCtrl = TextEditingController();
  final TextEditingController _treasurerPinCtrl = TextEditingController();
  final TextEditingController _treasurerPhoneCtrl = TextEditingController();
  final TextEditingController _secretaryNameCtrl = TextEditingController();
  final TextEditingController _secretaryIdCtrl = TextEditingController();
  final TextEditingController _secretaryPinCtrl = TextEditingController();
  final TextEditingController _secretaryPhoneCtrl = TextEditingController();
  final TextEditingController _contributionCtrl = TextEditingController();

  //widget SalesAgent
  Widget DropDownSales(List data, String text){
    if(data!=null)
    {
      return DropdownButton(
        items: data.map((item) {
          return new DropdownMenuItem(
            child: new Text(
              item.name,
              style: TextStyle(fontSize: 12.0),
            ),
            value: item.id,
          );
        }).toList(),
        hint: Text(
          text,
          style: TextStyle(
              color: Colors.black45,
              fontSize: 12.0
          ),),
        onChanged: (newVal) {
          setState(() {
            agent = newVal;
            // customerid = newVal;
            // print('customrid:' + customerid.toString());
          });
        },
        value: agent,
      );
    }
    else{
      return new Center(
        child: CircularProgressIndicator(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final steps = [
      CoolStep(
        title: 'Registration Form',
        subtitle: 'General Information',
        content: Form(
          key: _formKey,
          child: Column(
            children: [
              _buildTextField(
                labelText: 'Group Name',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Name is required';
                  }
                  return null;
                },
                controller: _groupNameCtrl,
              ),
              _buildTextField(
                labelText: 'Physical Location',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Location is required';
                  }
                  return null;
                },
                controller: _LocationCtrl,
              ),
              _buildTextField(
                labelText: 'Registration No (Optional)',
                validator: (value) {
                  return null;
                },
                controller: _RegNoCtrl,
              ),
            ],
          ),
        ),
        validation: () {
          if (!_formKey.currentState.validate()) {
            return 'Fill form correctly';
          }
          return null;
        },
      ),

      CoolStep(
        title: 'Registration Form',
        subtitle: 'CHAMA OFFICIALS',
        content: Form(
          key: _formKey,
          child: Column(
            children: [
              _buildTextField(
                labelText: 'Chairperson Names',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Name is required';
                  }
                  return null;
                },
                controller: _chairNameCtrl,
              ),
              _buildTextField(
                labelText: 'Chairperson ID Number',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'ID Number is required';
                  }
                  return null;
                },
                controller: _chairIdCtrl,
              ),
              _buildTextField(
                labelText: 'Chairperson PIN Number',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'PIN Number is required';
                  }
                  return null;
                },
                controller: _chairPinCtrl,
              ),
              _buildTextField(
                labelText: 'Chairperson Phone Number Number',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Phone Number is required';
                  }
                  return null;
                },
                controller: _chairPhoneCtrl,
              ),
            ],
          ),
        ),
        validation: () {
          if (!_formKey.currentState.validate()) {
            return 'Fill form correctly';
          }
          return null;
        },
      ),
      CoolStep(
        title: 'Registration Form',
        subtitle: 'CHAMA OFFICIALS',
        content: Form(
          key: _formKey,
          child: Column(
            children: [
              _buildTextField(
                labelText: 'Treasurer Names',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Name is required';
                  }
                  return null;
                },
                controller: _treasurerNameCtrl,
              ),
              _buildTextField(
                labelText: 'Treasurer ID Number',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'ID Number is required';
                  }
                  return null;
                },
                controller: _treasurerIdCtrl,
              ),
              _buildTextField(
                labelText: 'Treasurer PIN Number',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'PIN Number is required';
                  }
                  return null;
                },
                controller: _treasurerPinCtrl,
              ),
              _buildTextField(
                labelText: 'Treasurer Phone Number Number',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Phone Number is required';
                  }
                  return null;
                },
                controller: _treasurerPhoneCtrl,
              ),
            ],
          ),
        ),
        validation: () {
          if (!_formKey.currentState.validate()) {
            return 'Fill form correctly';
          }
          return null;
        },
      ),
      CoolStep(
        title: 'Registration Form',
        subtitle: 'CHAMA OFFICIALS',
        content: Form(
          key: _formKey,
          child: Column(
            children: [
              _buildTextField(
                labelText: 'Secretary Names',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Name is required';
                  }
                  return null;
                },
                controller: _secretaryNameCtrl,
              ),
              _buildTextField(
                labelText: 'Secretary ID Number',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'ID Number is required';
                  }
                  return null;
                },
                controller: _secretaryIdCtrl,
              ),
              _buildTextField(
                labelText: 'Secretary PIN Number',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'PIN Number is required';
                  }
                  return null;
                },
                controller: _secretaryPinCtrl,
              ),
              _buildTextField(
                labelText: 'Secretary Phone Number Number',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Phone Number is required';
                  }
                  return null;
                },
                controller: _secretaryPhoneCtrl,
              ),
            ],
          ),
        ),
        validation: () {
          if (!_formKey.currentState.validate()) {
            return 'Fill form correctly';
          }
          return null;
        },
      ),
      CoolStep(
        title: 'Registration Form',
        subtitle: 'COMMITMENT',
        content: Form(
          key: _formKey,
          child: Column(
            children: [
              _buildTextField(
                labelText: 'Amount to contribute per Month',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Amount is required';
                  }
                  return null;
                },
                controller: _contributionCtrl,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(5,10,5,10),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black45,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(6))
                  ),
                  height: 7.8 * SizeConfig.heightSizeMultiplier,
                  width: MediaQuery.of(context).size.width- 10,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: DropdownButtonHideUnderline(
                      child: DropDownSales(this.sAagents,'Sales Person who introduced you'),
                    ),
                  ),
                ),
              ),

              Row(
                children: <Widget>[
                  SizedBox(width: 10,),
                  Text('Agree to Terms of Finovate Sacco: ',style: TextStyle(fontSize: 17.0), ),
                  Checkbox(
                    checkColor: Colors.greenAccent,
                    activeColor: Colors.blue,
                    value: this.valuefirst,
                    onChanged: (bool value) {
                      setState(() {
                        this.valuefirst = value;
                      });
                    },
                  ),
                ],
              ),

              SizedBox(height: 50,),
              Center(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(20,4,50,4),
                  child: Container(
                      height: 5.1 * SizeConfig.heightSizeMultiplier,
                      width: 25.6 * SizeConfig.heightSizeMultiplier,
                      child: loading ? spinKit() :ElevatedButton(
                        onPressed: () {
                          setState(() {
                            loading = true;
                          });
                          submitClubInvest(_groupNameCtrl.text, _LocationCtrl.text,
                          _RegNoCtrl.text,_chairNameCtrl.text,_chairIdCtrl.text,_chairPinCtrl.text,
                          _chairPhoneCtrl.text, _treasurerNameCtrl.text,
                          _treasurerIdCtrl.text,_treasurerPinCtrl.text,_treasurerPhoneCtrl.text,_secretaryNameCtrl.text,
                          _secretaryIdCtrl.text,_secretaryPinCtrl.text,_secretaryPhoneCtrl.text,_contributionCtrl.text,
                          this.agent);
                        },
                        child: Text(
                          'SUBMIT ',
                          style: TextStyle(fontSize: 3.0 * SizeConfig.textMultiplier, fontWeight: FontWeight.bold, color: Colors.white),
                        ),

                      )),
                ),
              )
            ],
          ),
        ),
        validation: () {
          if (!_formKey.currentState.validate()) {
            return 'Fill form correctly';
          }
          return null;
        },
      ),
    ];

    final stepper = CoolStepper(
      showErrorSnackbar: false,
      onCompleted: () {
        print('Steps completed!');
      },
      steps: steps,
      config: CoolStepperConfig(
        backText: 'PREV',
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: stepper,
      ),
    );
  }

  Widget _buildTextField({
    String labelText,
    FormFieldValidator<String> validator,
    TextEditingController controller,
  }) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: TextFormField(
        decoration: InputDecoration(
          labelText: labelText,
        ),
        validator: validator,
        controller: controller,
      ),
    );
  }

  Widget _buildSelector({
    BuildContext context,
    String name,
  }) {
    final isActive = name == selectedRole;

    return Expanded(
      child: AnimatedContainer(
        duration: Duration(milliseconds: 200),
        curve: Curves.easeInOut,
        decoration: BoxDecoration(
          color: isActive ? Theme.of(context).primaryColor : null,
          border: Border.all(
            width: 0,
          ),
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: RadioListTile(
          value: name,
          activeColor: Colors.white,
          groupValue: selectedRole,
          onChanged: (String v) {
            setState(() {
              selectedRole = v;
            });
          },
          title: Text(
            name,
            style: TextStyle(
              color: isActive ? Colors.white : null,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSelectorCycle({
    BuildContext context,
    String name,
  }) {
    final isActive = name == selectedCycle;

    return Expanded(
      child: AnimatedContainer(
        duration: Duration(milliseconds: 200),
        curve: Curves.easeInOut,
        decoration: BoxDecoration(
          color: isActive ? Theme.of(context).primaryColor : null,
          border: Border.all(
            width: 0,
          ),
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: RadioListTile(
          value: name,
          activeColor: Colors.white,
          groupValue: selectedCycle,
          onChanged: (String v) {
            setState(() {
              selectedCycle = v;
            });
          },
          title: Text(
            name,
            style: TextStyle(
              color: isActive ? Colors.white : null,
            ),
          ),
        ),
      ),
    );
  }
}