import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/network/payment.dart';
import 'package:travelx_v1/goodley/pages/christmas_wallet.dart';
import 'package:travelx_v1/goodley/pages/e_wallet.dart';
import 'package:travelx_v1/goodley/pages/golden_wallet.dart';
import 'package:travelx_v1/goodley/pages/premium_wallet.dart';
import 'package:travelx_v1/goodley/pages/project_wallet.dart';
import 'package:travelx_v1/goodley/pages/shares_wallet.dart';

class Wallets extends StatefulWidget {
  @override
  _WalletsState createState() => _WalletsState();
}

class _WalletsState extends State<Wallets> {
  String user_id ="";
  String token ="";
  String ewallet ="";
  String shares ="";
  String premium ="";
  String project ="";
  String christmas ="";
  String golden ="";
  void getSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();


    user_id = prefs.getString('user');
    token = prefs.getString('accessToken');
    fetchWallet(user_id, token);
  }


  @override
  void initState()  {

    print('NextState');
    super.initState();

    getSharedPref();

  }

  fetchWallet(user_id, token) async {
    print(token);
    var response = await getTotalWallets(user_id,token);
    if( response != null && response.code == 200) {

      print('******************8');
      print(response.data);
      setState(() {
        ewallet =response.data['deposit'];
        shares =response.data['shares'];
        premium =response.data['premium'];
        project =response.data['project'];
        christmas =response.data['chritsmas'];
        golden =response.data['golden'];
      });
    }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'GOODLEY Wallets',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
      ),
      body: Container(
        child: Column(
          children: [
            SizedBox(height: 30,),
            Expanded(
              child: GridView.count(
                  crossAxisCount: 2,
                  childAspectRatio: 1.2,
                  padding: const EdgeInsets.fromLTRB(12.0, 20, 12, 20),
                  mainAxisSpacing: 10.0,
                  crossAxisSpacing: 1.0,
                  children: [
                    Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 10.0,
                          ),
                          Icon(
                            Icons.account_balance_wallet,
                            color: Theme.of(context).primaryColor,
                            size: 26.0,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            'Total Wallets',
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: 'PoppinsMedium',
                              color: Color(0xFF212121),
                            ),
                          ),

                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            'Total : KES ${ewallet == "" ? "0": ewallet}.00',
                            style: TextStyle(
                              fontSize: 13.5,
                              fontFamily: 'PoppinsMedium',
                              color: Color(0xFF212121),
                            ),
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    SharesWallet(title: 'Shares Wallet')));
                      },
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        elevation: 5,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 10.0,
                            ),
                            Icon(
                              Icons.group_work_outlined,
                              color: Theme.of(context).primaryColor,
                              size: 26.0,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              'Shares',
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xFF212121),
                              ),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              'Balance : KES ${shares == "" ? "0": shares}.00',
                              style: TextStyle(
                                fontSize: 13.5,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xFF212121),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    PremiumWallet(title: 'Premium Savings Wallet')));
                      },
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        elevation: 5,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 10.0,
                            ),
                            Icon(
                              Icons.addchart_rounded,
                              color: Theme.of(context).primaryColor,
                              size: 26.0,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              'Premium Savings',
                              style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xFF212121),
                              ),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              'Balance : KES ${premium == "" ? "0": premium}.00',
                              style: TextStyle(
                                fontSize: 13.5,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xFF212121),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    ProjectWallet(title: 'Project Savings Wallet')));
                      },
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        elevation: 5,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 10.0,
                            ),
                            Icon(
                              Icons.psychology_rounded,
                              color: Theme.of(context).primaryColor,
                              size: 26.0,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              'Project Savings',
                              style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xFF212121),
                              ),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              'Balance : KES ${project == "" ? "0": project}.00',
                              style: TextStyle(
                                fontSize: 13.5,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xFF212121),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    ChristmasWallet(title: 'Christmas Wallet')));
                      },
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        elevation: 5,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 10.0,
                            ),
                            Icon(
                              Icons.celebration,
                              color: Theme.of(context).primaryColor,
                              size: 26.0,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              'Christmas Savings',
                              style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xFF212121),
                              ),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              'Balance : KES ${christmas == "" ? "0": christmas}.00',
                              style: TextStyle(
                                fontSize: 13.5,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xFF212121),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    GoldenWallet(title: 'Golden Savings Wallet')));
                      },
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        elevation: 5,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 10.0,
                            ),
                            Icon(
                              Icons.save_outlined,
                              color: Theme.of(context).primaryColor,
                              size: 26.0,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              'Golden Savings',
                              style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xFF212121),
                              ),
                            ),
                            Text(
                              '(Locked)',
                              style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xFF212121),
                              ),
                            ),
                            Text(
                              'Balance : KES ${golden == "" ? "0": golden}.00',
                              style: TextStyle(
                                fontSize: 13.5,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xFF212121),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ]),
            ),

            SizedBox(height: 30,),
          ],
        ),
      ),
    );
  }
}
