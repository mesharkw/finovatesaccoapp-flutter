

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/models/all_api.dart';
import 'package:travelx_v1/goodley/network/payment.dart';
import 'package:travelx_v1/goodley/start.dart';
import 'package:travelx_v1/goodley/utils/common_utils.dart';
import 'package:travelx_v1/goodley/utils/spinner.dart';
import 'package:travelx_v1/goodley/widgets/radio_button.dart';
import 'package:travelx_v1/goodley/widgets/toast.dart';
import 'package:travelx_v1/size_config.dart';

class FundAccount extends StatefulWidget {
  @override
  _FundAccountState createState() => _FundAccountState();
}

class _FundAccountState extends State<FundAccount> {


  bool loading = false;
  bool show = false;
  final phone = new TextEditingController();
  final amount = new TextEditingController();
  final formKey_fund= GlobalKey<FormState>();
  String user;
  String token;
  bool _needCover = true;
  bool visible = false;
  bool changeScreen = false;

  /// Valid phone number
  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }

  void checkPay(phone) async {
    AllApi resp = await checkPayment(formatPhoneNumber(phone));
    print(resp.code);

    if (resp != null && resp.code != 400) {

      print(resp.data['status']);

      if (resp.data['status'] == true) {
        showToast('Payment Received');
      } else {
        showToast('Payment Not Received');
        setState(() {
          loading = false;
        });
      }
    }
    else {
      setState(() {
        loading = false;
      });
      showToast('Unable to to get response');
    }
  }

  void pay(String phone, String amount, String user, String token) async {

    print(amount);
      AllApi response = await makePayment(formatPhoneNumber(phone), amount, 'Premium', user, token);
      print(response.code);

      if (response != null && response.code != 400) {
//        await instance.fetchCitizenPost(id, token);
        setState(() {
          loading = false;
          show = true;
        });

        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => MainStart()));
      }
      else {
        setState(() {
          show = false;
          loading = false;
        });
        showToast('Unable to lock Mpesa STK Push, Use valid Number');
      }

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'Fund Account',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: false,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Form(
            key: formKey_fund,
            child: Column(

              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                SizedBox(
                  height: 25.0,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(20,4,50,4),
                  child: Text(
                    'To fund your Goodley wallet account, please use M-PESA with the following details:',
                    style: TextStyle(
                      color: Theme.of(context).accentColor,
                      fontSize: 2.58 * SizeConfig.textMultiplier,
                      fontFamily: 'PoppinsRegular',
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),

               Container(
                 height: MediaQuery.of(context).size.height/1.5,
                 child:ListView(
                   children: <Widget>[
                     SizedBox(height: 20,),
                     Padding(
                       padding: const EdgeInsets.only(left: 30),
                       child: Text('STK Push Payment Option',
                           style: TextStyle(
                               color: Theme.of(context).accentColor,
                               fontSize: 1.58 * SizeConfig.textMultiplier,
                               fontFamily: 'PoppinsRegular'
                           ),),
                     ),
                     SizedBox(
                       height: 5,
                     ),
                     Padding(
                       padding: const EdgeInsets.only(left: 10),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: <Widget>[
                           LabeledRadio(
                             label: 'My Number',
                             padding:
                             const EdgeInsets.symmetric(horizontal: 5.0),
                             value: true,
                             groupValue: _needCover,
                             onChanged: (bool val) {
                               setState(() {
                                 _needCover = val;
                                 visible = false;
                               });
                             },
                           ),
                           LabeledRadio(
                             label: 'Other ',
                             padding:
                             const EdgeInsets.symmetric(horizontal: 5.0),
                             value: false,
                             groupValue: _needCover,
                             onChanged: (bool val) {
                               setState(() {
                                 _needCover = val;
                                 visible = true;
                               });
                             },
                           ),
                         ],
                       ),
                     ),
                     Divider(),
                     Padding(
                       padding: const EdgeInsets.only(left: 30,right: 50),
                       child: TextFormField(
                         controller: amount,
                         keyboardType: TextInputType.number,
                         validator: (value) => validatePhone(value),
                         decoration: InputDecoration(
                           labelText: 'Amount',
                           hintText: 'eg. 100',
                           filled: true,
                           fillColor: Colors.white,
                           labelStyle:
                           TextStyle(color: Theme.of(context).primaryColor, fontSize: 18.0),
                           focusedBorder: const OutlineInputBorder(
                             borderSide: const BorderSide(color: Colors.blue),
                           ),
                           border: OutlineInputBorder(
                               borderSide: BorderSide(color: Colors.blue),
                               borderRadius: BorderRadius.circular(10)),),
                       ),
                     ),
                     SizedBox(
                       height: 10,
                     ),
                     !visible? SizedBox():
                     Padding(
                       padding: const EdgeInsets.only(left: 30,right: 50),
                       child: TextFormField(
                         controller: phone,
                         keyboardType: TextInputType.number,
                         validator: (value) => validatePhone(value),
                         decoration: InputDecoration(
                           labelText: 'Phone Number',
                           hintText: 'eg. 2547********',
                           filled: true,
                           fillColor: Colors.white,
                           labelStyle:
                           TextStyle(color: Theme.of(context).primaryColor, fontSize: 18.0),
                           focusedBorder: const OutlineInputBorder(
                             borderSide: const BorderSide(color: Colors.blue),
                           ),
                           border: OutlineInputBorder(
                               borderSide: BorderSide(color: Colors.blue),
                               borderRadius: BorderRadius.circular(10)),),
                       ),
                     ),

                     Container(

                       margin: EdgeInsets.only(left: 30, right: 30,top: 10),
                       child:  Padding(
                         padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                         child: Center(
                           child: loading ? spinKit() :
                           SizedBox(
                             width: 300.0,
                             height: 40.0,
                             child: RaisedButton(
                               textColor: Colors.white,
                               color: Theme.of(context).primaryColor,
                               child: Text("Fund Account "),
                               onPressed: () async {

                                 String phoneNumber = '';
                                 setState(() {
                                   loading = true;
                                 });
                                 SharedPreferences prefs = await SharedPreferences.getInstance();
                                 user = prefs.getString('user');
                                 token = prefs.getString('accessToken');
                                 print(user);
                                 if(visible ==false){

                                   //Return String
                                  String phm = prefs.getString('phone');
                                   phoneNumber = phm;
                                 } else{
                                   phoneNumber = phone.text;

                                 }

                                 pay(phoneNumber,amount.text,user, token);

                               },
                               shape: new RoundedRectangleBorder(
                                 borderRadius: new BorderRadius.circular(10.0),

                               ),
                             ),
                           ),

                         ),
                       ),
                       decoration: BoxDecoration(
                         borderRadius:
                         BorderRadius.all(Radius.circular(10.0)),
                       ),
                     ),

                     Container(
                         margin: EdgeInsets.only(top: 10),
                         padding: EdgeInsets.only(left: 30,top: 20),
                         child: Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: <Widget>[

                             Text('Normal Payment Instructions ',
                                 style: TextStyle(
                                     color: Theme.of(context).accentColor,
                                     fontSize: 2.18 * SizeConfig.textMultiplier,
                                     fontFamily: 'PoppinsRegular'
                                 ),),

                             SizedBox(height: 10,),
                             Divider(),

                             RichText(
                               text: TextSpan(
                                 text: 'Go to the MPESA menu, ',
                                 style: TextStyle(fontSize: 15.5, color: Colors.grey[700]),
                                 children: <TextSpan>[
                                   TextSpan(
                                     text: 'the MPESA menu, ',
                                   ),

                                 ],
                               ),
                             ),
                             SizedBox(height: 3,),
                             RichText(
                               text: TextSpan(
                                 text: 'Select  ',
                                 style: TextStyle(fontSize: 15.5, color: Colors.grey[700]),
                                 children: <TextSpan>[
                                   TextSpan(
                                     text: 'Lipa na Mpesa services ',
                                   ),

                                 ],
                               ),
                             ),
                             SizedBox(height: 3,),
                             RichText(
                               text: TextSpan(
                                 text: 'Choose  ',
                                 style: TextStyle(fontSize: 15.5, color: Colors.grey[700]),
                                 children: <TextSpan>[
                                   TextSpan(
                                       text: 'Pay Bill option ',
                                       style: TextStyle(fontWeight: FontWeight.bold)),

                                 ],
                               ),
                             ),
                             SizedBox(height: 3,),

                             RichText(
                               text: TextSpan(
                                 text: 'Enter ',
                                 style: TextStyle(fontSize: 15.5, color: Colors.grey[700]),
                                 children: <TextSpan>[
                                   TextSpan(
                                       text: '4062769 ',
                                       style: TextStyle(fontWeight: FontWeight.bold)),
                                   TextSpan(
                                     text: ' as the business number',
                                   )
                                 ],
                               ),
                             ),
                             SizedBox(height: 3,),
                             RichText(
                               text: TextSpan(
                                 text: 'Enter ',
                                 style: TextStyle(fontSize: 15.5, color: Colors.grey[700]),
                                 children: <TextSpan>[
                                   TextSpan(
                                       text: 'ID Number  ',
                                       style: TextStyle(fontWeight: FontWeight.bold)),
                                   TextSpan(
                                     text: ' as the account number',
                                   )
                                 ],
                               ),
                             ),
                             SizedBox(height: 3,),
                             RichText(
                               text: TextSpan(
                                 text: 'Enter ',
                                 style: TextStyle(fontSize: 15.5, color: Colors.grey[700]),
                                 children: <TextSpan>[

                                   TextSpan(
                                     text: ' the amount as',
                                   ),
                                   TextSpan(
                                       text: ' any amount ',
                                       style: TextStyle(fontWeight: FontWeight.bold)),
                                 ],
                               ),
                             ),
                             SizedBox(height: 3,),
                             RichText(
                               text: TextSpan(
                                 text: 'Enter ',
                                 style: TextStyle(fontSize: 15.5, color: Colors.grey[700]),
                                 children: <TextSpan>[

                                   TextSpan(
                                     text: ' your pin and press Ok',
                                   ),
                                 ],
                               ),
                             ),
                             SizedBox(height: 20,),
                           ],
                         )),
                   ],
                 ),
               )
              ],
            ),
          )
        ],
      ),
    );
  }


}
