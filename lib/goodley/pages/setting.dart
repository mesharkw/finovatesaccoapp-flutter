import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/screens/splash.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool lockInBackground = true;
  bool notificationsEnabled = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 30.0),
        child: SettingsList(
          // backgroundColor: Colors.orange,
          sections: [
            SettingsSection(
              title: Text('Settings'),
              // titleTextStyle: TextStyle(fontSize: 30),
              tiles: [
                SettingsTile(
                  title: Text('Language'),
                  description: Text('English'),
                  leading: Icon(Icons.language),
                ),
                SettingsTile(
                  title: Text('Environment'),
                  description: Text('Production'),
                  leading: Icon(Icons.cloud_queue),
                  onPressed: (context) => print('e'),
                ),
              ],
            ),
            SettingsSection(
              title: Text('Misc'),
              tiles: [
                SettingsTile(
                    title: Text('Terms of Service'),
                    leading: Icon(Icons.description)),
                SettingsTile(
                    title: Text('Open source licenses'),
                    leading: Icon(Icons.collections_bookmark)),
                SettingsTile(
                    title: Text('Share Application'),
                    leading: Icon(Icons.share_rounded),
                    onPressed: (context) async {
                      final RenderBox box = context.findRenderObject();
                      Share.share('check out my app https://play.google.com/store/apps/details?id=com.finovate_sacco', subject: 'Sacco!');

                    }
                ),
              ],
            ),
            SettingsSection(
              title: Text('Security'),
              tiles: [
                SettingsTile(
                  title: Text('Log Out'),
                  leading: Icon(Icons.phonelink_lock_sharp),
                  onPressed: (context) async {

                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    prefs.setBool('logged_in', false);
                    await prefs.clear();

                    Navigator.of(context, rootNavigator: true).pushReplacement(MaterialPageRoute(builder: (context) {

                      return new Splash();
                    }));

                    // Navigator.pushReplacement(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => Splash()));
                  },
                ),
              ],
            ),


          ],
        ),
      ),
    );
  }
}
