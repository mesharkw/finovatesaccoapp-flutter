import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/models/all_api.dart';
import 'package:travelx_v1/goodley/models/funds.dart';
import 'package:travelx_v1/goodley/network/payment.dart';
import 'package:travelx_v1/goodley/utils/common_utils.dart';
import 'package:travelx_v1/goodley/utils/new_radio.dart';
import 'package:travelx_v1/goodley/utils/spinner.dart';
import 'package:travelx_v1/goodley/widgets/toast.dart';

import '../../size_config.dart';
import '../start.dart';

class PremiumWallet extends StatefulWidget {
  PremiumWallet({Key key, this.title}) : super(key: key);

  final String title;



  @override
  _SharesState createState() => _SharesState();
}

class _SharesState extends State<PremiumWallet> {

  List<FundsModel> lessons = [];
  String name ="";
  String user_id ="";
  String token ="";
  String email="";
  String dob="";
  String idNumber="";
  String career="";
  String phoneNumber="";
  double total = 0.0;
  bool loading = true;
  bool loaded = false;
  DateTime date;
  DateFormat dateFormat = DateFormat("yyyy-MM-dd");


  void getSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();


    phoneNumber = prefs.getString('phoneNumber');
    user_id = prefs.getString('user');
    token = prefs.getString('accessToken');
    fetchSMS(user_id, token);




  }

  var refreshKey = GlobalKey<RefreshIndicatorState>();

  Future<Null> refreshList() async {
    refreshKey.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 3));
    lessons.clear();
    total = 0.00;
    fetchSMS(user_id, token);
    setState(() {

    });

    return null;
  }
  fetchSMS(user_id, token) async {
    print(token);
    var response = await checkFunds(user_id,token);
    if( response != null || response.isNotEmpty) {
      print(response.toString());
      for (var i = 0; i < response.length; i++) {

        if (response[i].type == 'Premium'){
          print(response[i].amount);
          total =  total + double.parse(response[i].amount);
          date =  DateTime.parse(response[i].created_at);
          FundsModel fundsModel =  new FundsModel(
            transaction_id: response[i].transaction_id,
            type: response[i].type,
            amount: response[i].amount,
            created_at: response[i].created_at,
            phoneNumber: response[i].phoneNumber,
            mpesaReceiptNumber: response[i].mpesaReceiptNumber,
          );
          lessons.add(fundsModel);
        }
      }
      setState(() {
        loaded = true;
        loading = false;
      });

    }


  }
    @override
    void initState() {

      super.initState();
      getSharedPref();
    }

  @override
  Widget build(BuildContext context) {
    ListTile makeListTile(FundsModel lesson) => ListTile(
      contentPadding:
      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      leading: Container(
        padding: EdgeInsets.only(right: 12.0),
        decoration: new BoxDecoration(
            border: new Border(
                right: new BorderSide(width: 1.0, color: Colors.white24))),
        child: Icon(Icons.account_balance_wallet, color: Colors.white),
      ),
      title: Text(
        lesson.transaction_id,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

      subtitle: Row(
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Padding(
                padding: EdgeInsets.only(left: 10.0),
                child: Text("Ksh. "+lesson.amount,
                    style: TextStyle(color: Colors.white))),
          )
        ],
      ),
      trailing:
      Text(lesson.mpesaReceiptNumber,
          style: TextStyle(color: Colors.white)),
      onTap: () {
        // Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //         builder: (context) => DetailPage(lesson: lesson)));
      },
    );

    Card makeCard(FundsModel lesson) => Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
        child: makeListTile(lesson),
      ),
    );

    final makeBody =Container(

      height: MediaQuery.of(context).size.height/2.2,
      decoration: BoxDecoration(
        color: Color(0xFF002642),
        borderRadius:
        BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(8.0,20,8,20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text('Premium Wallet History',
                  style: TextStyle(
                    color: Theme.of(context).accentColor,
                    fontSize: 2.28 * SizeConfig.textMultiplier,
                    fontFamily: 'PoppinsRegular',
                  ),
                ),
                // Icon(Icons.arrow_forward_rounded, size: 30, color: Colors.white,),

              ],
            ),
          ),
          Expanded(

            child: loading ?  Lottie.asset('assets/nodata.json',):ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: lessons.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(lessons[index].created_at,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.w400)),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text( lessons[index].transaction_id,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 15.0,
                                              fontWeight: FontWeight.w600)),

                                      Text(lessons[index].mpesaReceiptNumber == null ? 'Transfer':lessons[index].mpesaReceiptNumber ,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 13.0,
                                              fontWeight: FontWeight.w400))
                                    ],
                                  ),
                                )
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: Column(
                                children: [
                                  Text(lessons[index].amount+ '.00 Kes',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15.0,
                                          fontWeight: FontWeight.w600)),
                                  Text(lessons[index].type + '  Savings',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.w400))
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Divider(
                          color: Colors.white
                      )
                    ],
                  ),
                );
              },
            ),
          ),



        ],
      ),

    );

    final topAppBar = AppBar(
      backgroundColor: Colors.white,
      elevation: 0,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
              width: 30.0,
              height: 30.0,
              decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  image: new DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/iconapp.jpeg'),

                  )
              )),

        ],
      ),

      centerTitle: false,
    );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: topAppBar,
      body: loaded ? RefreshIndicator(
        key: refreshKey,
        onRefresh: refreshList,
        color: Theme.of(context).primaryColor,
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Text("KES ${total.toString()}",
                  style: TextStyle(
                    color: Color(0xFF002642),
                    fontWeight: FontWeight.bold,
                    fontSize: 6.48 * SizeConfig.textMultiplier,
                    fontFamily: 'PoppinsRegular',
                  ),),
                SizedBox(
                  height: 20.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,

                  children: [
                    RaisedButton(
                      onPressed: () {
                        bool status = false;
                        showModalBottomSheet(context: context, builder: (BuildContext bc)
                        {
                          Map fundiData = ModalRoute
                              .of(context)
                              .settings
                              .arguments;
                          return ModalPayment(payment: Payment(status));
                        }
                        );
                      },
                      child: RichText(
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Icon(Icons.wrap_text, size: 14, color: Colors.white,),
                            ),
                            TextSpan(
                              text: " Deposit ",
                            ),
                          ],
                        ),
                      ),
                      color: Theme.of(context).primaryColor,
                    ),
                    SizedBox(
                      width: 20.0,
                    ),
                    RaisedButton(
                      onPressed: () {
                        bool status = true;
                        showModalBottomSheet(context: context, builder: (BuildContext bc)
                        {
                          Map fundiData = ModalRoute
                              .of(context)
                              .settings
                              .arguments;
                          return ModalPayment(payment: Payment(status));
                        }
                        );
                      },
                      child: RichText(
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Icon(Icons.card_membership_outlined, size: 14, color: Colors.white,),
                            ),
                            TextSpan(
                              text: "  APPLY ",
                            ),
                          ],
                        ),
                      ),
                      color: Theme.of(context).primaryColor,
                    ),
                  ],
                ),
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Premium Wallet Features",
                        style: TextStyle(
                          color: Color(0xFF002642),
                          fontWeight: FontWeight.bold,
                          fontSize: 2.78 * SizeConfig.textMultiplier,
                          fontFamily: 'PoppinsRegular',
                        ),),
                      Text("1. This saving wallet is based on personal short-term goals. It enables one to work on priorities systematically.",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xFF002642),
                          fontWeight: FontWeight.normal,
                          fontSize: 1.78 * SizeConfig.textMultiplier,
                          fontFamily: 'PoppinsRegular',
                        ),),
                      Text("2. Minimum saving period 6 months",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xFF002642),
                          fontWeight: FontWeight.normal,
                          fontSize: 1.78 * SizeConfig.textMultiplier,
                          fontFamily: 'PoppinsRegular',
                        ),),
                      Text("3. Minimum saving  Kshs 50",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xFF002642),
                          fontWeight: FontWeight.normal,
                          fontSize: 1.78 * SizeConfig.textMultiplier,
                          fontFamily: 'PoppinsRegular',
                        ),),
                      Text("4. Interest Rate 5% p.a",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xFF002642),
                          fontWeight: FontWeight.normal,
                          fontSize: 1.78 * SizeConfig.textMultiplier,
                          fontFamily: 'PoppinsRegular',
                        ),),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                makeBody,

                SizedBox(height: 30,),
              ],
            ),
            decoration: BoxDecoration(
              borderRadius:
              BorderRadius.all(Radius.circular(10.0)),
            ),
          ),
        ),
      ) : Center(child: CircularProgressIndicator()),
      // bottomNavigationBar: makeBottom,
    );
  }
}




class Payment {
  final bool status;

  Payment(this.status);
}

class ModalPayment extends StatefulWidget {
  // Declare a field that holds the Person data
  final Payment payment;

  // In the constructor, require a Person
  ModalPayment({Key key, @required this.payment}) : super(key: key);

  @override
  _ModalPaymentState createState() => _ModalPaymentState();
}

class _ModalPaymentState extends State<ModalPayment> {

  bool loading = false;
  bool show = false;
  final _formKey_payment = GlobalKey<FormState>();
  final phone = new TextEditingController();
  final amount = new TextEditingController();
  int _needCover = 0;
  int _wallet = 0;
  bool visible = false;
  bool visible_wallet = false;
  bool changeScreen = false;
  @override
  Widget build(BuildContext context) {
    visible_wallet = widget.payment.status;
    return Container(
      child:
      show? ListView(
        children: <Widget>[
          SizedBox(height: 30,),
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: Text('Check Payment',
              style: TextStyle(fontWeight: FontWeight.w500),),
          ),
          SizedBox(
            height: 20,
          ),
          Divider(),

          Container(

            margin: EdgeInsets.only(left: 30, right: 30,top: 10),
            child:  Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Center(
                child: loading ? spinKit() :
                SizedBox(
                  width: 300.0,
                  height: 40.0,
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: Colors.blue[700],
                    child: Text("Proceed "),
                    onPressed: () async {
                      String phoneNumber = '';
                      setState(() {
                        loading = true;
                      });

                      if(visible ==false){
                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        //Return String
                        String phm = prefs.getString('phoneNo');
                        phoneNumber = phm;
                      } else{
                        phoneNumber = phone.text;
                      }
                      checkPay(phoneNumber);

                    },
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0),

                    ),
                  ),
                ),

              ),
            ),
            decoration: BoxDecoration(
              borderRadius:
              BorderRadius.all(Radius.circular(10.0)),
            ),
          ),

          Container(
              margin: EdgeInsets.only(top: 10),
              padding: EdgeInsets.only(left: 30,top: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  Text('NormalPayment Instructions ',
                    style: TextStyle(fontWeight: FontWeight.w500),),

                  SizedBox(height: 10,),
                  Divider(),
                  RichText(
                    text: TextSpan(
                      text: 'Go to the MPESA menu, ',
                      style: TextStyle(fontSize: 15.5, color: Colors.grey[700]),
                      children: <TextSpan>[
                        TextSpan(
                          text: 'the MPESA menu, ',
                        ),

                      ],
                    ),
                  ),
                  SizedBox(height: 3,),
                  RichText(
                    text: TextSpan(
                      text: 'Select  ',
                      style: TextStyle(fontSize: 15.5, color: Colors.grey[700]),
                      children: <TextSpan>[
                        TextSpan(
                          text: 'Lipa na Mpesa services ',
                        ),

                      ],
                    ),
                  ),
                  SizedBox(height: 3,),
                  RichText(
                    text: TextSpan(
                      text: 'Choose  ',
                      style: TextStyle(fontSize: 15.5, color: Colors.grey[700]),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'Pay Bill option ',
                            style: TextStyle(fontWeight: FontWeight.bold)),

                      ],
                    ),
                  ),
                  SizedBox(height: 3,),

                  RichText(
                    text: TextSpan(
                      text: 'Enter ',
                      style: TextStyle(fontSize: 15.5, color: Colors.grey[700]),
                      children: <TextSpan>[
                        TextSpan(
                            text: '12345 ',
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(
                          text: ' as the business number',
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 3,),
                  RichText(
                    text: TextSpan(
                      text: 'Enter ',
                      style: TextStyle(fontSize: 15.5, color: Colors.grey[700]),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'ID Number  ',
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(
                          text: ' as the account number',
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 3,),
                  RichText(
                    text: TextSpan(
                      text: 'Enter ',
                      style: TextStyle(fontSize: 15.5, color: Colors.grey[700]),
                      children: <TextSpan>[

                        TextSpan(
                          text: ' the amount ',
                        ),
                        // TextSpan(
                        //     text: ' Ksh.${widget.payment.amount}  ',
                        //     style: TextStyle(fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ),
                  SizedBox(height: 3,),
                  RichText(
                    text: TextSpan(
                      text: 'Enter ',
                      style: TextStyle(fontSize: 15.5, color: Colors.grey[700]),
                      children: <TextSpan>[

                        TextSpan(
                          text: ' tyour pin and press Ok',
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 3,),
                ],
              )),
        ],
      ):
      ListView(
        children: <Widget>[
          SizedBox(height: 30,),
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: Text('Premium Wallet Mode',
              style: TextStyle(fontWeight: FontWeight.w500),),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                visible_wallet?SizedBox():LabeledRadio(
                  label: 'Deposit',
                  padding:
                  const EdgeInsets.symmetric(horizontal: 5.0),
                  value: 0,
                  groupValue: _wallet,
                  onChanged: (int val) {
                    setState(() {
                      _wallet = val;
                      visible_wallet = false;
                    });
                  },
                ),
                !visible_wallet?SizedBox():LabeledRadio(
                  label: 'APPLY ',
                  padding:
                  const EdgeInsets.symmetric(horizontal: 5.0),
                  value: 1,
                  groupValue: _wallet,
                  onChanged: (int val) {
                    setState(() {
                      _wallet = val;
                      visible_wallet = true;
                    });
                  },
                ),
              ],
            ),
          ),
          SizedBox(height: 30,),
          visible_wallet?SizedBox(): Padding(
            padding: const EdgeInsets.only(left: 30),
            child: Text('STK Push Payment Option',
              style: TextStyle(fontWeight: FontWeight.w500),),
          ),
          visible_wallet?SizedBox():SizedBox(
            height: 20,
          ),
          visible_wallet?SizedBox():Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                LabeledRadio(
                  label: 'My Number',
                  padding:
                  const EdgeInsets.symmetric(horizontal: 5.0),
                  value: 0,
                  groupValue: _needCover,
                  onChanged: (int val) {
                    setState(() {
                      _needCover = val;
                      visible = false;
                    });
                  },
                ),
                LabeledRadio(
                  label: 'Other ',
                  padding:
                  const EdgeInsets.symmetric(horizontal: 5.0),
                  value: 1,
                  groupValue: _needCover,
                  onChanged: (int val) {
                    setState(() {
                      _needCover = val;
                      visible = true;
                    });
                  },
                ),
              ],
            ),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.only(left: 30,right: 50),
            child: TextFormField(
              controller: amount,
              keyboardType: TextInputType.number,
              validator: (value) => validatePhone(value),
              decoration: InputDecoration(
                labelText: 'Amount',
                hintText: 'eg. 100',
                filled: true,
                fillColor: Colors.white,
                labelStyle:
                TextStyle(color: Colors.blue, fontSize: 18.0),
                focusedBorder: const OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.blue),
                ),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue),
                    borderRadius: BorderRadius.circular(10)),),
            ),
          ),

          SizedBox(height: 20,),
          !visible? SizedBox():Padding(
            padding: const EdgeInsets.only(left: 30,right: 50),
            child: TextFormField(
              controller: phone,
              keyboardType: TextInputType.number,
              validator: (value) => validatePhone(value),
              decoration: InputDecoration(
                labelText: 'Phone Number',
                hintText: 'eg. 2547********',
                filled: true,
                fillColor: Colors.white,
                labelStyle:
                TextStyle(color: Colors.blue, fontSize: 18.0),
                focusedBorder: const OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.blue),
                ),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue),
                    borderRadius: BorderRadius.circular(10)),),
            ),
          ),
          visible_wallet?SizedBox():Container(

            margin: EdgeInsets.only(left: 30, right: 30,top: 10),
            child:  Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Center(
                child: loading ? spinKit() :
                SizedBox(
                  width: 300.0,
                  height: 40.0,
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    child: Text("Deposit "),
                    onPressed: () async {

                      String phoneNumber = '';
                      setState(() {
                        loading = true;
                      });
                      SharedPreferences prefs = await SharedPreferences.getInstance();
                      String user_id = prefs.getString('user');
                      String token = prefs.getString('accessToken');
                      if(visible ==false){

                        //Return String
                       String phm = prefs.getString('phone');
                        phoneNumber = phm;
                      } else{
                        phoneNumber = phone.text;
                      }

                      pay(phoneNumber,amount.text,user_id, token);

                    },
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0),

                    ),
                  ),
                ),

              ),
            ),
            decoration: BoxDecoration(
              borderRadius:
              BorderRadius.all(Radius.circular(10.0)),
            ),
          ),
          !visible_wallet?SizedBox():Container(

            margin: EdgeInsets.only(left: 30, right: 30,top: 10),
            child:  Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Center(
                child: loading ? spinKit() :
                SizedBox(
                  width: 300.0,
                  height: 40.0,
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    child: Text("APPLY "),
                    onPressed: () async {

                      // String phoneNumber = '';
                      // setState(() {
                      //   loading = true;
                      // });
                      //
                      // SharedPreferences prefs = await SharedPreferences.getInstance();
                      // String user_id = prefs.getString('user');
                      // String token = prefs.getString('accessToken');
                      // if(visible ==false){
                      //   //Return String
                      //   String phm = prefs.getString('phoneNo');
                      //   phoneNumber = phm;
                      // } else{
                      //   phoneNumber = phone.text;
                      // }
                      //
                      // pay(phoneNumber,amount.text,user_id, token);
                      showToast('Feature Not Activated');

                    },
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0),

                    ),
                  ),
                ),

              ),
            ),
            decoration: BoxDecoration(
              borderRadius:
              BorderRadius.all(Radius.circular(10.0)),
            ),
          ),

          SizedBox(),
        ],
      ),
    );
  }
  /// Valid phone number
  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }
  void checkPay(phone) async {

    AllApi resp = await checkPayment(formatPhoneNumber(phone));
    print(resp.code);

    if (resp != null && resp.code != 400 ) {
//        await instance.fetchCitizenPost(id, token);

      print(resp.data['status'].toString());
      if(resp.data['status'] == true) {

        // if(widget.payment.calling == true){
        //   setState(() {
        //     loading = false;
        //     Navigator.pop(context);
        //   });
        //
        //   // _service.call(widget.payment.phone);
        // }
        // else{
        //
        //   AllApi res = await checkBlacklist(widget.payment.ujenzi);
        //   print(res.code);
        //
        //   if (res != null && res.code != 400 ) {
        //     setState(() {
        //       loading = false;
        //       show = false;
        //       Navigator.of(context).pop();
        //     });
        //     showDialog<void>(
        //       context: context,
        //       builder: (BuildContext context) {
        //         return AlertDialog(
        //           title: Text('Fundi'),
        //           content:  Text('${res.data['message']}'),
        //           actions: <Widget>[
        //             FlatButton(
        //               child: Text('Ok'),
        //               onPressed: () {
        //                 Navigator.of(context).pop();
        //               },
        //             ),
        //           ],
        //         );
        //       },
        //     );
        //   }
        //
        // }
      } else {
        showToast('Payment Not Received');
        setState(() {

          loading = false;
        });
      }


    }
    else {
      setState(() {

        loading = false;
      });
      showToast('Unable to to get response');
    }

  }

  void pay(String phone, String amount, String user, String token) async {

    print(amount);
    AllApi response = await makePayment(formatPhoneNumber(phone), amount, 'Premium', user, token);
    print(response.code);

    if (response != null && response.code != 400) {
//        await instance.fetchCitizenPost(id, token);
      setState(() {
        loading = false;
        show = true;
      });

      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => MainStart()));
    }
    else {
      setState(() {
        show = false;
        loading = false;
      });
      showToast('Unable to lock Mpesa STK Push, Use valid Number');
    }

  }
}