//Sign Up Page
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/models/all_api.dart';
import 'package:travelx_v1/goodley/network/otp_api.dart';
import 'package:travelx_v1/goodley/network/payment.dart';
import 'package:travelx_v1/goodley/utils/common_utils.dart';
import 'package:travelx_v1/goodley/utils/spinner.dart';
import 'package:travelx_v1/goodley/widgets/toast.dart';
import 'package:travelx_v1/screens/forgot_password.dart';
import 'package:travelx_v1/size_config.dart';

import '../start.dart';

class FeedBack extends StatefulWidget {
  @override
  _FeedbackState createState() => _FeedbackState();
}

class _FeedbackState extends State<FeedBack> {
  bool loading = false;
  final _formKey_signup = GlobalKey<FormState>();
  final phoneNumber = new TextEditingController();
  final message = new TextEditingController();
  final name = new TextEditingController();
  final dob = new TextEditingController();
  final idNumber = new TextEditingController();
  final career = new TextEditingController();
  final email = new TextEditingController();
  final role = new TextEditingController();

  String user_id ="";
  String token ="";

  /// Valid phone number
  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Form(
            key: _formKey_signup,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //Logo
                Padding(
                  padding: const EdgeInsets.fromLTRB(15,70,8,8),
                  child:
                  Center(
                    child: Image(
                      height: 50,
                      width: 100,
                      image: AssetImage('assets/iconapp.jpeg'),
                      fit: BoxFit.cover,
                    ),
                  ),
                  // Image(
                  //   height: 80,
                  //   width: 80,
                  //   image: AssetImage('assets/save2.png'),
                  //
                  // ),
                ),
                Column(

                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 30.0,
                    ),
                    Center(
                      child: Text(
                        'Got any feedback?',
                        style: TextStyle(
                            color: Color(0xFFFF002642),
                            fontSize: 3.28 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                            fontWeight: FontWeight.bold
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          'Fill up the form below to let us know what you think about the Goodley and how we can make it better for you.',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 2.08 * SizeConfig.textMultiplier,
                              fontFamily: 'PoppinsRegular',
                              fontWeight: FontWeight.normal
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    //Email/phone
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 6.1 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width-10,
                        child: TextFormField(
                          controller: name,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.8 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: '  ',
                            labelText: ' Name',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.bookmark),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 6.1 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: email,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.8 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Email',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.email),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 6.1 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: phoneNumber,
                          validator: (value) => validatePhone(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.8 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Phone Number',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.call),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 7.5* SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          maxLines: 5,
                          minLines: 3,
                          keyboardType: TextInputType.multiline,
                          controller: message,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.8 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Message',


                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    //NextButton
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,4,20,4),
                      child: Container(
                          height: 6.1 * SizeConfig.heightSizeMultiplier,
                          width: MediaQuery.of(context).size.width-10,
                          child: loading ? spinKit() : RaisedButton(
                            onPressed: () async {
                              SharedPreferences prefs = await SharedPreferences.getInstance();
                              user_id = prefs.getString('user');
                              token = prefs.getString('accessToken');
                              if (_formKey_signup.currentState.validate()) {
                                setState(() {
                                  loading = true;
                                });

                                sendReg(name.text,email.text,phoneNumber.text, message.text, user_id,token);

                              }
                              print('Next');
                            },
                            child: Text(
                              'SUBMIT FEEDBACK',
                              style: TextStyle(fontSize: 2.6 * SizeConfig.textMultiplier, fontWeight: FontWeight.bold, color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                            color: Theme.of(context).primaryColor,
                          )),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> sendReg(String name, String email, String phoneNumber,String message, String user, token ) async {
    if (phoneNumber != null) {
      AllApi response = await addFeedback(name , email,  formatPhoneNumber(phoneNumber),message, user, token);
      // print(response.code);

      if (response != null && response.code != 400 ) {
//        await instance.fetchCitizenPost(id, token);


        setState(() {
          showToast('Feedback Added');
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MainStart()));
        });
      }
      else if (response.code == 400 ) {

        setState(() {

          loading = false;
        });
        print('here');
        print(response.data['error']['error']);
        showToast(response.data['error']['error']);
      }
    }

  }
}