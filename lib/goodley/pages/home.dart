import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/models/all_api.dart';
import 'package:travelx_v1/goodley/models/ewallet.dart';
import 'package:travelx_v1/goodley/models/fosa.dart';
import 'package:travelx_v1/goodley/models/funds.dart';
import 'package:travelx_v1/goodley/network/otp_api.dart';
import 'package:travelx_v1/goodley/network/payment.dart';
import 'package:travelx_v1/goodley/pages/chama.dart';
import 'package:travelx_v1/goodley/pages/club_invest.dart';
import 'package:travelx_v1/goodley/pages/enquiry.dart';
import 'package:travelx_v1/goodley/pages/feedback.dart';
import 'package:travelx_v1/goodley/pages/fees_wallet.dart';
import 'package:travelx_v1/goodley/pages/fund.dart';
import 'package:travelx_v1/goodley/pages/holiday_wallet.dart';
import 'package:travelx_v1/goodley/pages/jijenge_wallet.dart';
import 'package:travelx_v1/goodley/pages/project_wallet.dart';
import 'package:travelx_v1/goodley/pages/shares.dart';
import 'package:travelx_v1/goodley/pages/update_profile.dart';
import 'package:travelx_v1/goodley/pages/wallet.dart';
import 'package:travelx_v1/goodley/pages/withdraw.dart';
import 'package:travelx_v1/goodley/utils/common_utils.dart';
import 'package:travelx_v1/goodley/utils/spinner.dart';
import 'package:simple_speed_dial/simple_speed_dial.dart';
import 'package:travelx_v1/goodley/widgets/toast.dart';
import 'package:travelx_v1/screens/set_question.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';


import '../../size_config.dart';
import 'christmas_wallet.dart';
import 'donatewallet.dart';
import 'e_wallet.dart';
import 'golden_wallet.dart';



//slider Items
class Item1 extends StatelessWidget {
  const Item1({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [
              0.3,
              1
            ],
            colors: [
              Color(0xffffffff),
              Color(0xffffffff),
            ]),
      ),
      child: Stack(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height:MediaQuery.of(context).size.height/4.32,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage('assets/WhatsApp Image 2021-06-30 at 21.29.34.jpeg'),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 8.0, left: 20),
                child: Text('Acquire land in Flexible 36 monthly installments',
                  style: TextStyle(
                    color: Color(0xffffffff),
                    fontSize: 2.68 * SizeConfig.textMultiplier,
                    fontFamily: 'PoppinsRegular',
                  ),
                ),
              ),
            ),
          ]
      ),
    );
  }
}
class Item6 extends StatelessWidget {
  const Item6({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.3, 1],
            colors: [Color(0xffffffff),Color(0xffffffff),]),
      ),
      child: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height:MediaQuery.of(context).size.height/3.32,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage('assets/nairobi.jpeg'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
class Item7 extends StatelessWidget {
  const Item7({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.3, 1],
            colors: [Color(0xffffffff),Color(0xffffffff),]),
      ),
      child: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height:MediaQuery.of(context).size.height/3.32,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage('assets/lands.jpeg'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Item3 extends StatelessWidget {
  const Item3({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.3, 1],
            colors: [Color(0xffffffff), Color(0xffffffff)]),
      ),
      child: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height:MediaQuery.of(context).size.height/4.32,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage('assets/WhatsApp Image 2021-06-30 at 21.29.21.jpeg'),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Jiajiri ',
                    style: TextStyle(
                      color: Color(0xffffffff),
                      fontSize: 3.78 * SizeConfig.textMultiplier,
                      fontFamily: 'PoppinsRegular',
                    ),
                  ),
                  Text(' NA ',
                    style: TextStyle(
                      color: Color(0xffffffff),
                      fontSize: 3.78 * SizeConfig.textMultiplier,
                      fontFamily: 'PoppinsRegular',
                    ),
                  ),
                  Text(' Bike.',
                    style: TextStyle(
                      color: Color(0xffffffff),
                      fontSize: 3.78 * SizeConfig.textMultiplier,
                      fontFamily: 'PoppinsRegular',
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
//Forgot Password Page 3
class Item4 extends StatelessWidget {
  const Item4({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.3, 1],
            colors: [Color(0xffffffff), Color(0xffffffff)]),
      ),
      child: Stack(
          children: <Widget>[
            Container(
            width: MediaQuery.of(context).size.width,
            height:MediaQuery.of(context).size.height/4.32,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage('assets/WhatsApp Image 2021-06-30 at 21.29.06.jpeg'),
              ),
            ),
          ),
            Align(
              alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text('Club Invest:Choose a Plan & Live hustle free while we invest for you.',
                style: TextStyle(
                  color: Color(0xffffffff),
                  fontWeight: FontWeight.bold,
                  fontSize: 2.58 * SizeConfig.textMultiplier,
                  fontFamily: 'PoppinsRegular',
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
class Item5 extends StatelessWidget {
  const Item5({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.3, 1],
            colors: [Color(0xffffffff), Color(0xffffffff)]),
      ),
      child: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height:MediaQuery.of(context).size.height/4.32,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage('assets/hand.jpeg'),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: Text('Good Customer Experience is our DNA',
                style: TextStyle(
                  color: Color(0xffffffff),
                  fontSize: 2.58 * SizeConfig.textMultiplier,
                  fontFamily: 'PoppinsRegular',
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _currentIndex = 0;
  String fees ="";
  String holiday ="";
  String savings ="";
  String share ="";
  String clubInvest ="";
  String rent ="";
  String safari ="";
  String jijenge ="";
  String asset ="";
  String golden ="";
  int _counter = 0;

  bool loaded = true;

  var refreshKey = GlobalKey<RefreshIndicatorState>();

  Future<void> getProfile(phoneNumbers,accessToken,btype) async {
    if (btype != null) {
      var response = await profile(phoneNumbers,accessToken,btype);
      print('dataaaaaa'+ response.data.toString());
      if (response != null || response.code !=400 ) {
          if(response.data['kra'] == ''){
            print('Null Records');
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        UpdateProfile()));
          }
      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }

  Future<Null> refreshList() async {
    refreshKey.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 3));
    fetchWallet(phone, token, Btype);
    setState(() {
      loaded = false;
    });
    List cardList = [
      Item6(),
      Item1(),
      Item3(),
      Item4(),
      Item5(),
    ];

    return null;
  }


  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  List cardList = [
    Item6(),
    Item1(),
    Item3(),
    Item4(),
    Item5(),
  ];
  bool loading = false;
  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }
  SharedPreferences prefs;
  String name;
  String user_id ="";
  String token ="";
  String phone ="";
  bool security = false;
  bool kodis = false;
  String Btype = '1a862df26f6943997cef90233877a4fe';

  List<Fosa> fosa;
  List fosaAcc = [];
  List otherAcc = [];
  String greeting() {
    var hour = DateTime.now().hour;
    if (hour < 12) {
      return 'Morning '+ name;
    }
    if (hour < 17) {
      return 'Afternoon '+ name;
    }
    return 'Evening '+name;
  }

  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }

  void appVersion() async {

    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    String id_number = prefs.getString('id_number');

    AllApi response = await versionApp();

    if (response != null && response.code == 200 || response != null && response.code == 201 ) {
      String appName = packageInfo.appName;
      String packageName = packageInfo.packageName;
      String version = packageInfo.version;
      String buildNumber = packageInfo.buildNumber;
      print(response.code);
      print(response.data['data']['version_code']);
      print(buildNumber);
      if(int.parse(buildNumber) < int.parse(response.data['data']['version_code'])){
        AwesomeDialog(
            context: context,
            animType: AnimType.LEFTSLIDE,
            dismissOnTouchOutside: false,
            headerAnimationLoop: true,
            dialogType: DialogType.INFO,
            useRootNavigator: true,
            title: 'This is Ignored',
            body: Column(
              crossAxisAlignment:
              CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  'VERSION UPDATE',
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 18),
                ),
                SizedBox(
                  height: 10,
                ),
                Divider(),
                Center(
                  child: Text(
                    'Click OK to update to the latest Version',
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 18),
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
              ],
            ),
            desc: 'This is also Ignored',
            btnOkOnPress: () async {
              launch('https://play.google.com/store/apps/details?id=com.finovate_sacco');
            },
            btnOkIcon: Icons.check_circle,
            )
          ..show();
      }
    }
    else {
      setState(() {

        loading = false;
      });
      showToast('Unable to verify User. Contact Admin');
    }

  }

  void getSharedPref(BuildContext context) async {



    this.appVersion();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    setState(() {
      name = prefs.getString('fullName');
      security = prefs.getBool('security');
      kodis = prefs.getBool('kodi');
    });
    print('security'+security.toString());
    print('kodis'+kodis.toString());
    if(kodis == null || !kodis){
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool('logged_in', false);
      await prefs.clear();
      Navigator.pushReplacementNamed(context, '/login');

    }

    // token = prefs.getString('accessToken');
    token = '627bb5278ce6b800d59c4d8472637e37';
    phone = prefs.getString('phone');
    getProfile(phone, token, Btype);
    fetchWallet(phone, token, Btype);
    fetchOtherWallet(phone, token, Btype);


  }

  fetchWallet(phone, token, btype) async {
    print(token);
    var response = await fosaAccount(formatPhoneNumber(phone), token, btype);
    if( response != null) {
      this.fosaAcc = response;
      for (var i = 0; i < this.fosaAcc.length; i++) {
        if(this.fosaAcc[i].type == "FOSA"){
          setState(() {

            this.savings = (this.fosaAcc[i].bal).toStringAsFixed(2);
            loaded = true;

          });
        } else if(this.fosaAcc[i].type == "Club Invest"){
          setState(() {

            this.clubInvest = this.fosaAcc[i].bal.toString();
            loaded = true;

          });
        }else if(this.fosaAcc[i].type == "KODI"){
          setState(() {

            this.rent = (this.fosaAcc[i].bal).toStringAsFixed(2);
            loaded = true;

          });
        }else if(this.fosaAcc[i].type == "School Fees Savings"){
          setState(() {

            this.fees = (this.fosaAcc[i].bal).toStringAsFixed(2);
            loaded = true;

          });
        }else if(this.fosaAcc[i].type == "Holiday Savings"){
          setState(() {

            this.holiday = (this.fosaAcc[i].bal).toStringAsFixed(2);
            loaded = true;

          });
        }else if(this.fosaAcc[i].type == "Share Capital"){
          setState(() {

            this.share = (this.fosaAcc[i].bal).toStringAsFixed(2);
            loaded = true;

          });
        } else if(this.fosaAcc[i].type == "KADOGO"){
          setState(() {

            this.jijenge = (this.fosaAcc[i].bal).toStringAsFixed(2);
            loaded = true;

          });
        }

        print(this.fosaAcc[i].bal);
      }
      print('******************8');
      print(this.fosaAcc);

    }
  }
  fetchOtherWallet(phone, token, btype) async {
    print('token 2');
    print(token);
    var response = await accountUser(formatPhoneNumber(phone), token, btype);
    if( response != null) {
      this.otherAcc = response.data;
      print('******************822');
      print(response.data);
      print(this.otherAcc);

      var sum = 0.0;
      var Asum = 0.0;
      var Jsum = 0.0;
      for (var i = 0; i < this.otherAcc.length; i++) {

        print('Account'+this.otherAcc[i]['type']);
        if(this.otherAcc[i]['type'].contains("Kodi Facility")){
          if(this.otherAcc[i]['status'].contains("pending")){
            setState(() {
              // this.savings = this.fosaAcc[i].bal.toString();
              rent = '0';
              loaded = true;

            });
          } else {
            sum += (-double.parse(this.otherAcc[i]['bal'].toString()));
            print('Sum'+sum.toString());
            setState(() {
              // this.savings = this.fosaAcc[i].bal.toString();
              rent = sum.toString();
              loaded = true;

            });
          }

        } else if(this.otherAcc[i]['type'].contains("Asset")){
          if(this.otherAcc[i]['status'].contains("pending")){
            setState(() {
              // this.savings = this.fosaAcc[i].bal.toString();
              asset = '0';
              loaded = true;

            });
          } else {
            Asum += (-double.parse(this.otherAcc[i]['bal'].toString()));
            print('Sum' + Asum.toString());
            setState(() {
              // this.savings = this.fosaAcc[i].bal.toString();
              asset = Asum.toString();
              loaded = true;
            });
          }
        }

        print(this.otherAcc[i].bal);
      }
      print('******************8');
      print(this.otherAcc);

    }
  }

  final List<String> imgList = [
    'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
    'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
  ];
  int _current = 0;
  @override
  void initState()  {
    // TODO: implement initState
    super.initState();
    getSharedPref(context);
  }
  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Confirm'),
          content: Text('Do you want to exit the App'),
          actions: <Widget>[
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop(false); //Will not exit the App
              },
            ),
            FlatButton(
              child: Text('Yes'),
              onPressed: () {
                Navigator.of(context).pop(true); //Will exit the App
              },
            )
          ],
        );
      },
    ) ?? false;
  }
  @override
  Widget build(BuildContext context) {
    int count = 7;
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
                  appBar: AppBar(backgroundColor: Colors.black, toolbarHeight: 0,),
                  body: loaded ? RefreshIndicator(
                    key: refreshKey,
                    onRefresh: refreshList,
                    child: Container(
                      color: Colors.white30,
                      child: Column(
                        children: [
                          Container(
                            child: Stack(
                                children: [

                                  Padding(
                                    padding: EdgeInsets.only(top: 0.0),
                                    child: ImageSlideshow(

                                      /// Width of the [ImageSlideshow].
                                      width: double.infinity,

                                      /// Height of the [ImageSlideshow].
                                      height: 290,

                                      /// The page to show when first creating the [ImageSlideshow].
                                      initialPage: 0,

                                      /// The color to paint the indicator.
                                      indicatorColor: Colors.blue,

                                      /// The color to paint behind th indicator.
                                      indicatorBackgroundColor: Colors.grey,

                                      /// The widgets to display in the [ImageSlideshow].
                                      /// Add the sample image file into the images folder
                                      children: [
                                        Image.asset(
                                          'assets/land8.png',
                                          fit: BoxFit.fill,
                                        ),
                                        Image.asset(
                                          'assets/land7.png',
                                          fit: BoxFit.fill,
                                        ),
                                        Image.asset(
                                          'assets/land3.jpg',
                                          fit: BoxFit.cover,
                                        ),
                                        // Image.asset(
                                        //   'assets/land2.jpg',
                                        //   fit: BoxFit.cover,
                                        // ),
                                        // Image.asset(
                                        //   'assets/land1.jpg',
                                        //   fit: BoxFit.cover,
                                        // ),
                                        // Image.asset(
                                        //   'assets/land4.jpg',
                                        //   fit: BoxFit.cover,
                                        // ),
                                      ],

                                      /// Called whenever the page in the center of the viewport changes.
                                      onPageChanged: (value) {
                                        print('Page changed: $value');
                                      },

                                      /// Auto scroll interval.
                                      /// Do not auto scroll with null or 0.
                                      autoPlayInterval: 5000,

                                      /// Loops back to first slide.
                                      isLoop: true,
                                    ),

                                  ),
                                ]
                            ),
                          ),
                          SizedBox(height: 5,),
                          Expanded(
                            child: GridView.count(
                                crossAxisCount: 2,
                                childAspectRatio: 1.5,
                                padding: const EdgeInsets.fromLTRB(12.0, 1, 12, 18),
                                mainAxisSpacing: 10.0,
                                crossAxisSpacing: 1.0,
                                children: [
                                  //FOSA
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  EWallet(title: 'FOSA Wallet')));
                                    },
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                      elevation: 5,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.account_balance_wallet,
                                            color: Theme.of(context).primaryColorDark,
                                            size: 26.0,
                                          ),
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          Text(
                                            'FOSA',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          Text(
                                            'Ksh. ${savings == "" ? "0": savings}',
                                            style: TextStyle(
                                              fontSize: 13.5,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  //KODI
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  GoldenWallet(title: 'Kodi Facility')));
                                    },
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                      elevation: 5,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.sync_outlined,
                                            color: Theme.of(context).primaryColorDark,
                                            size: 26.0,
                                          ),
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(3.0),
                                            child: Text(
                                              'Kodi Facility',
                                              style: TextStyle(
                                                fontSize: 15,
                                                fontFamily: 'PoppinsMedium',
                                                color: Color(0xFF212121),
                                              ),
                                            ),

                                          ),
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          Text(
                                            'Ksh.  ${rent == "" ? "0": rent}',
                                            style: TextStyle(
                                              fontSize: 13.5,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                  ),
                                  //Fees
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  FeesWallet(
                                                      title:
                                                      'School Fees Wallet')));
                                    },
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                      elevation: 5,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            height: 1.0,
                                          ),
                                          Text(
                                            'School Fees',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),SizedBox(
                                            height: 10.0,
                                          ),
                                          Text(
                                            'Ksh. ${fees == "" ? "0": fees}',
                                            style: TextStyle(
                                              fontSize: 13.5,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  //Holiday
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  HolidayWallet(
                                                      title:
                                                      'Holiday Wallet')));
                                    },
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                      elevation: 5,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            height: 1.0,
                                          ),
                                          Text(
                                            'Leisure Account',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),SizedBox(
                                            height: 10.0,
                                          ),
                                          Text(
                                            'Ksh. ${holiday == "" ? "0": holiday}',
                                            style: TextStyle(
                                              fontSize: 13.5,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  //Asset
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ProjectWallet(title: 'Asset Investment Wallet')));
                                    },
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                      elevation: 5,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.apartment_sharp,
                                            color: Theme.of(context).primaryColorDark,
                                            size: 26.0,
                                          ),
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          Text(
                                            'Asset Investment',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          Text(
                                            'Ksh. ${asset == "" ? "0": asset}',
                                            style: TextStyle(
                                              fontSize: 13.5,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  //Kadogo
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ChristmasWallet(title: 'Kadogo Wallet')));
                                    },
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                      elevation: 5,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.motorcycle_sharp,
                                            color: Theme.of(context).primaryColorDark,
                                            size: 26.0,
                                          ),
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          Text(
                                            'Kadogo Plan',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          Text(
                                            'Ksh. ${jijenge == "" ? "0": jijenge}',
                                            style: TextStyle(
                                              fontSize: 13.5,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  // Jijenge
                                  GestureDetector(
                                    onTap: () {
                                      // showToast('Coming Soon');

                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  JijengeWallet(title: 'Jijenge Plan')));
                                    },
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                      elevation: 5,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.directions_bus_sharp,
                                            color: Theme.of(context).primaryColorDark,
                                            size: 26.0,
                                          ),
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          Text(
                                            'Jijenge Plan',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          // Text(
                                          //   'Ksh. ${clubInvest == "" ? "0": clubInvest}',
                                          //   style: TextStyle(
                                          //     fontSize: 13.5,
                                          //     fontFamily: 'PoppinsMedium',
                                          //     color: Color(0xFF212121),
                                          //   ),
                                          // ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  // Chama
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  Chama(title: 'Chama')));
                                    },
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                      elevation: 5,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Center(
                                            child: Icon(
                                              Icons.group,
                                              color: Theme.of(context).primaryColorDark,
                                              size: 26.0,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          Text(
                                            'Chama',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  //Share
                                  GestureDetector(
                                    onTap: () {
                                      // showToast('Coming Soon');
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ShareWallet(title: 'Share Capital')));
                                    },
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                      elevation: 5,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          Text(
                                            'Share Capital',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          Text(
                                            'Ksh. ${share == "" ? "0": share}',
                                            style: TextStyle(
                                              fontSize: 13.5,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  // //Donate
                                  GestureDetector(
                                    onTap: () {
                                      // showToast('Coming Soon');
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DonateWallet(title: 'Donate')));
                                    },
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                      elevation: 5,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.addchart_rounded,
                                            color: Theme.of(context).primaryColorDark,
                                            size: 26.0,
                                          ),
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          Text(
                                            'Donate',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontFamily: 'PoppinsMedium',
                                              color: Color(0xFF212121),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ]),
                          ),

                        ],
                      ),
                    ),
                  )
                      : Center(child: CircularProgressIndicator()
                  )),
    );

  }


}