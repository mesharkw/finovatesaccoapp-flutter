import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/models/ewallet.dart';
import 'package:travelx_v1/goodley/network/otp_api.dart';
import 'package:travelx_v1/goodley/network/payment.dart';
import 'package:travelx_v1/goodley/utils/common_utils.dart';
import 'package:travelx_v1/goodley/utils/spinner.dart';
import 'package:travelx_v1/goodley/widgets/toast.dart';

import '../../size_config.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {

  List lessons;
  String name ="";
  String accessToken ="";
  String target ="";
  String email="";
  String dob="";
  String idNumber="";
  String residence="";
  String phoneNumber="";
  String user_id ="";
  String token ="";
  String nextOfKinName ="";
  String nextOfKinPhone ="";
  String member_no ="";
  String Btype = '1a862df26f6943997cef90233877a4fe';
  String savings ="";
  List fosaAcc = [];
  @override
  void initState() {
    lessons = getLessons();
    super.initState();

    getSharedPref();
  }
  void getSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    setState(() {

      phoneNumber = prefs.getString('phone');
      accessToken = prefs.getString('accessToken');

    });

    getProfile(phoneNumber,accessToken,Btype);
    fetchWallet(phoneNumber, token, Btype);
  }
  Future<void> getProfile(phoneNumbers,accessToken,btype) async {
    if (btype != null) {
      var response = await profile(phoneNumbers,accessToken,btype);
      print('dataaaaaa'+ response.toString());
      if (response != null || response.code !=400 ) {

        setState(() {
          // phoneNumber = response.data['contact'];
          email = response.data['email'];
          member_no= response.data['member_number'];
          name = response.data['name'];
          residence = response.data['contribution'];
        });
        // this.counties = response;
        // setState(() {
        // });
      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }
  void updateUserData(name, email, career, phoneNumber,nextofkinNames, nextofkinPhone, user_id, token) async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    var response = await updateUser(name, email, career, phoneNumber,nextofkinNames,nextofkinPhone, user_id, token);
    if (response != null && response.code != 400) {
      setState(() {
        loading = false;
        prefs.setString('name', name);
        prefs.setString('email', email);
        // prefs.setString('career', career);
        prefs.setString('phone', phoneNumber);
        prefs.setString('next_of_kin_name', nextofkinNames);
        prefs.setString('next_of_kin_phone', nextofkinPhone);
        getSharedPref();
      });

    }
  }

  final _formKey_login = GlobalKey<FormState>();
  final phoneNumbers = new TextEditingController();
  final names= new TextEditingController();
  final careers = new TextEditingController();
  final emails = new TextEditingController();
  final member = new TextEditingController();
  final nextOfKinNames = new TextEditingController();
  final nextOfKinPhones = new TextEditingController();
  bool loading = false;
  void showDialogs() {
    showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (_, __, ___) {
        return Align(
          alignment: Alignment.center,
          child: Container(
            height: MediaQuery.of(context).size.height / 1.3,
            child: Material(
              child: Form(
                key: _formKey_login,
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,4,50,4),
                      child: Text(
                        'Update your personal details',
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontSize: 2.58 * SizeConfig.textMultiplier,
                          fontFamily: 'PoppinsRegular',
                        ),
                        textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(18,10,10,10),
                      child: Container(
                        height: 6.1 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: names,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.8 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: '  ',
                            labelText: ' Name',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.bookmark),
                            ),
                          ),

                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 6.1 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: careers,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.8 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Career',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.person),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 6.1 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: emails,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.8 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Email',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.email),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 6.1 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: phoneNumbers,
                          validator: (value) => validatePhone(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.8 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Phone Number',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.call),
                            ),
                          ),

                        ),
                      ),
                    ),

                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(18,10,10,10),
                      child: Container(
                        height: 6.1 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: nextOfKinNames,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.8 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: '  ',
                            labelText: ' Next of KIN Full Names',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.bookmark),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(18,10,10,10),
                      child: Container(
                        height: 6.1 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: nextOfKinPhones,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.8 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: '  ',
                            labelText: ' Next of KIN PhoneNumber',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.bookmark),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    //NextButton
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,4,20,4),
                      child: Container(
                          height: 6.1 * SizeConfig.heightSizeMultiplier,
                          width: MediaQuery.of(context).size.width-10,
                          child: loading ? spinKit() : RaisedButton(
                            onPressed: () async {

                              if (_formKey_login.currentState.validate()) {
                                setState(() {
                                  loading = true;
                                });

                                SharedPreferences prefs = await SharedPreferences.getInstance();
                                user_id = prefs.getString('user');
                                token = prefs.getString('accessToken');

                                updateUserData(names.text,emails.text,careers.text,phoneNumbers.text,nextOfKinNames.text,nextOfKinPhones.text,user_id, token);
                                Navigator.of(context, rootNavigator: true).pop();
                              }
                              print('Next');
                            },
                            child: Text(
                              'UPDATE ',
                              style: TextStyle(fontSize: 2.6 * SizeConfig.textMultiplier, fontWeight: FontWeight.bold, color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                            color: Theme.of(context).primaryColor,
                          )),
                    )
                  ],
                ),
              ),
            ),
            margin: EdgeInsets.only(bottom: 50, left: 12, right: 12),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(40),
            ),
          ),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }
  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }
  fetchWallet(phone, token, btype) async {
    print(token);
    var response = await fosaAccount(formatPhoneNumber(phone), token, btype);
    if( response != null) {
      this.fosaAcc = response;
      for (var i = 0; i < this.fosaAcc.length; i++) {
        if(this.fosaAcc[i].type == "FOSA"){
          setState(() {

            this.savings = (this.fosaAcc[i].no);


          });
        }

        print(this.fosaAcc[i].bal);
      }
      print('******************8');
      print(this.fosaAcc);

    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'Profile',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: false,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                width: 200.0,
                height: 80.0,
                decoration: new BoxDecoration(
                    image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage('assets/fsac.jpeg')
                    )
                )),
            Text(name == null ? "" :name,
              style: TextStyle(
                color: Color(0xFF002642),
                fontWeight: FontWeight.bold,
                fontSize: 2.58 * SizeConfig.textMultiplier,
                fontFamily: 'PoppinsRegular',
              ),),
            Text(email== null ? "" :email,
              style: TextStyle(
                color: Theme.of(context).accentColor,
                fontWeight: FontWeight.bold,
                fontSize: 2.08 * SizeConfig.textMultiplier,
                fontFamily: 'PoppinsRegular',
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: MediaQuery.of(context).size.height/1.7 ,
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 20.0,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Name',
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 2.08 * SizeConfig.textMultiplier,
                                    fontFamily: 'PoppinsRegular',
                                  ),),
                                Text(name== null ? "" :name,
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontSize: 2.08 * SizeConfig.textMultiplier,
                                    fontFamily: 'PoppinsRegular',
                                  ),),
                              ],
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Member no.',
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 2.08 * SizeConfig.textMultiplier,
                                    fontFamily: 'PoppinsRegular',
                                  ),),
                                Text(member_no == null ? "" :member_no,
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontSize: 2.08 * SizeConfig.textMultiplier,
                                    fontFamily: 'PoppinsRegular',
                                  ),),
                              ],
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('FOSA Account No.',
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 2.08 * SizeConfig.textMultiplier,
                                    fontFamily: 'PoppinsRegular',
                                  ),),
                                Text(savings == null ? "" :savings,
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontSize: 2.08 * SizeConfig.textMultiplier,
                                    fontFamily: 'PoppinsRegular',
                                  ),),
                              ],
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Email',
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 2.08 * SizeConfig.textMultiplier,
                                    fontFamily: 'PoppinsRegular',
                                  ),),
                                Text(email== null ? "" :name,
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontSize: 2.08 * SizeConfig.textMultiplier,
                                    fontFamily: 'PoppinsRegular',
                                  ),),
                              ],
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Phone Number',
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 2.08 * SizeConfig.textMultiplier,
                                    fontFamily: 'PoppinsRegular',
                                  ),),
                                Text(phoneNumber == " " ? " " :phoneNumber,
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontSize: 2.08 * SizeConfig.textMultiplier,
                                    fontFamily: 'PoppinsRegular',
                                  ),),
                              ],
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Next Of KIN FullNames',
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 2.08 * SizeConfig.textMultiplier,
                                    fontFamily: 'PoppinsRegular',
                                  ),),
                                Text(nextOfKinName == null ? "...": nextOfKinName,
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontSize: 2.08 * SizeConfig.textMultiplier,
                                    fontFamily: 'PoppinsRegular',
                                  ),),
                              ],
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Next of KIN mobile',
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 2.08 * SizeConfig.textMultiplier,
                                    fontFamily: 'PoppinsRegular',
                                  ),),
                                Text(nextOfKinPhone == null ? "...": nextOfKinPhone,
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontSize: 2.08 * SizeConfig.textMultiplier,
                                    fontFamily: 'PoppinsRegular',
                                  ),),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    decoration: BoxDecoration(
                      color: Color(0xFF002642),
                      borderRadius:
                      BorderRadius.all(Radius.circular(15.0)),
                    ),
                  ),
                ),


              ],
            ),

          ],
        ),

      ),
    );
  }

}
List getLessons() {
  return [
    Lesson(
        title: "Deposit",
        level: "12/12/2020",
        indicatorValue: 1.0,
        price: 20,
        content:
        "Start by taking a couple of minutes to read the info in this section. Launch your app and click on the Settings menu.  While on the settings page, click the Save button.  You should see a circular progress indicator display in the middle of the page and the user interface elements cannot be clicked due to the modal barrier that is constructed."),
    Lesson(
        title: "Deposit",
        level: "11/12/2020",
        indicatorValue: 1.0,
        price: 50,
        content:
        "Start by taking a couple of minutes to read the info in this section. Launch your app and click on the Settings menu.  While on the settings page, click the Save button.  You should see a circular progress indicator display in the middle of the page and the user interface elements cannot be clicked due to the modal barrier that is constructed."),
    Lesson(
        title: "Deposit",
        level: "09/12/2020",
        indicatorValue: 1.0,
        price: 30,
        content:
        "Start by taking a couple of minutes to read the info in this section. Launch your app and click on the Settings menu.  While on the settings page, click the Save button.  You should see a circular progress indicator display in the middle of the page and the user interface elements cannot be clicked due to the modal barrier that is constructed."),

    Lesson(
        title: "Deposit",
        level: "03/12/2020",
        indicatorValue: 1.0,
        price: 50,
        content:
        "Start by taking a couple of minutes to read the info in this section. Launch your app and click on the Settings menu.  While on the settings page, click the Save button.  You should see a circular progress indicator display in the middle of the page and the user interface elements cannot be clicked due to the modal barrier that is constructed."),
    Lesson(
        title: "Deposit",
        level: "01/12/2020",
        indicatorValue: 1.0,
        price: 50,
        content:
        "Start by taking a couple of minutes to read the info in this section. Launch your app and click on the Settings menu.  While on the settings page, click the Save button.  You should see a circular progress indicator display in the middle of the page and the user interface elements cannot be clicked due to the modal barrier that is constructed.  ")
  ];
}