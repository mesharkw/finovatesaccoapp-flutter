import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/models/all_api.dart';
import 'package:travelx_v1/goodley/models/funds.dart';
import 'package:travelx_v1/goodley/network/otp_api.dart';
import 'package:travelx_v1/goodley/network/payment.dart';
import 'package:cool_stepper/cool_stepper.dart';
import 'package:travelx_v1/goodley/utils/spinner.dart';
import 'package:travelx_v1/goodley/widgets/toast.dart';

import '../../size_config.dart';
import '../start.dart';

class ClubInvest extends StatefulWidget {
  ClubInvest({Key key, this.title}) : super(key: key);

  final String title;



  @override
  _SharesState createState() => _SharesState();
}


class _SharesState extends State<ClubInvest> {

  List<FundsModel> lessons = [];
  String name ="";
  String user_id ="";
  String token ="";
  String email="";
  String residence="";
  String dob="";
  String idNumber="";
  String career="";
  String phoneNumber="";
  String member_no="";
  double total = 0.0;
  bool loading = false;
  bool loaded = false;
  DateTime date;
  DateFormat dateFormat = DateFormat("yyyy-MM-dd");
  String accessToken = '';

  List jijengeaAcc = [];
  String loanName = '';
  String desc = '';
  String deposit = '';
  String requirement = '';
  String maxperiod = '';
  String loanAmount = '';
  String loanId = '';
  String loanRef = '';


  String Btype = '1a862df26f6943997cef90233877a4fe';

  Future<void> getProfile(phoneNumbers,accessToken,btype) async {
    if (btype != null) {
      var response = await profile(phoneNumbers,accessToken,btype);
      print('dataaaaaa Res '+ response.data.toString());
      if (response != null || response.code !=400 ) {

      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }

  void getSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      phoneNumber = prefs.getString('phone');
      token = prefs.getString('accessToken');

    });

    getProfile(phoneNumber,token,Btype);
    fetchLoans(phoneNumber,token,Btype);



  }


  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }

  void confirmP(pass) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    accessToken = prefs.getString('accessToken');
    phoneNumber = prefs.getString('phone');

    AllApi resp = await confirmPass(
        formatPhoneNumber(phoneNumber), pass, Btype, accessToken);
    print(resp.code);

    if (resp != null || resp.code != 400 || resp.code != 401 ) {

      Map data = jsonDecode(resp.data);
      print(data['error']);
      print(data['message']);

      if (data['error'] != null && data['message'] == null) {
        setState(() {


          loading = false;

        });
        showToast('Failed Confirmation, Go back and Check Password');
      }
      else if (data['message'] != null && data['error'] == null) {
        setState(() {
          loading = false;

        });
      }
    }

  }

  fetchLoans(phone, token, btype) async {
    print(token);
    var response = await loanFacility(formatPhoneNumber(phone), token, btype);
    print('data1' + response.toString());
    if (response != null) {
      this.jijengeaAcc = response;

      print('data1' + response.toString());
      for (var i = 0; i < this.jijengeaAcc.length; i++) {
        print('Loan Name ' + this.jijengeaAcc[i].name);
        print('Loan Id' + this.jijengeaAcc[i].id);
        print('Loan per' + this.jijengeaAcc[i].max_repay_period);
        if (this.jijengeaAcc[i].name.contains("Jijenge Normal")) {
          print(this.jijengeaAcc[i]);
          setState(() {
            loanName = this.jijengeaAcc[i].name;
            desc = this.jijengeaAcc[i].description;
            deposit = this.jijengeaAcc[i].deposit;
            requirement = this.jijengeaAcc[i].requirement;
            maxperiod = this.jijengeaAcc[i].max_repay_period;
            loanAmount = this.jijengeaAcc[i].set_loan_amount;
            loanId = this.jijengeaAcc[i].id;
          });
        }
      }
      print('******************8');
      print(this.jijengeaAcc);
    }
  }

  applyJijengeLoan() async {
    print(token);
    var response = await applyLoan(formatPhoneNumber(phoneNumber), _amountCtrl.text,
        loanId, _repayCtrl.text,_purposeCtrl.text,  _otpCtrl.text, token, Btype);
    if (response.code == 200 || response.code == 201) {
      setState(() {
        loanRef = response.data['ref'];
        loading = false;
      });
      showToast('Add Guarantors and business');
    } else {
      setState(() {
        loading = false;
      });
      showToast(response.data['error']);
    }
    return response.code;
  }

  applyNewJijengeLoan() async {
    print(token);
    var response = await applyJijengeLoanNew(formatPhoneNumber(phoneNumber),_pinCtrl.text, _amountCtrl.text,
        loanId, _repayCtrl.text, token, Btype,_nameGuarantorCtrl.text,
        _phoneGuarantorCtrl.text, _idNoCtrl.text, _kraCtrl.text,_memberNoCtrl.text,_nameGuarantorCtrl2.text,
        _phoneGuarantorCtrl2.text, _idNoCtrl2.text, _kraCtrl2.text,_memberNoCtrl2.text);
    if (response.code == 200 || response.code == 201) {
      setState(() {
        loanRef = response.data['ref'];
        loading = false;
      });
      Navigator.pushReplacementNamed(context, '/');
      // showToast('Successfully awaiting Approval');
    } else {
      setState(() {
        loading = false;
      });
      showToast(response.data['error']);
    }
  }

  addGuarantorsLoan(_nameGuarantor,phoneNumber, _idNo,_memberNo,  _kra) async {
    print(token);
    var response = await addGuarantor(formatPhoneNumber(phoneNumber), _nameGuarantor,
        formatPhoneNumber(phoneNumber), _idNo,_memberNo,  _kra, loanRef,token, Btype);

    if (response.code == 200 || response.code == 201) {

      showToast('Guarantors added, Proceed');
    } else {
      setState(() {
        loading = false;
      });
      showToast(response.data['message']);
    }
    // return response.code;
  }

  addBusinessLoan() async {
    print(token);
    var response = await addBusiness(formatPhoneNumber(phoneNumber), _bNameCtrl.text, _bAddressCtrl.text,
        _bLocationCtrl.text,  _bNatureCtrl.text, _bIncomeCtrl.text,token, Btype);

    if (response.code == 200 || response.code == 201) {

      showToast('Success');
      Navigator.pushReplacementNamed(context, '/');
    } else {
      setState(() {
        loading = false;
      });
      showToast(response.data['message']);
    }
    // return response.code;
  }

  @override
    void initState() {

      super.initState();
      getSharedPref();
    }
  final _formKeyLoan = GlobalKey<FormState>();
  final _formKeyOtp = GlobalKey<FormState>();
  final _formKeyBusiness = GlobalKey<FormState>();
  final _formKeyGuarantor = GlobalKey<FormState>();
  final _formKeyGuarantor2 = GlobalKey<FormState>();
  String selectedRole = '';
  String selectedCycle= '';
  final TextEditingController _amountCtrl = TextEditingController();
  final TextEditingController _repayCtrl = TextEditingController();
  final TextEditingController _purposeCtrl = TextEditingController();
  final TextEditingController _pinCtrl = TextEditingController();
  final TextEditingController _otpCtrl = TextEditingController();

  //Business
  final TextEditingController _bNameCtrl = TextEditingController();
  final TextEditingController _bPhoneCtrl = TextEditingController();
  final TextEditingController _bAddressCtrl = TextEditingController();
  final TextEditingController _bLocationCtrl = TextEditingController();
  final TextEditingController _bNatureCtrl = TextEditingController();
  final TextEditingController _bIncomeCtrl = TextEditingController();

  //Guarantor
  final TextEditingController _nameGuarantorCtrl = TextEditingController();
  final TextEditingController _phoneGuarantorCtrl = TextEditingController();
  final TextEditingController _idNoCtrl = TextEditingController();
  final TextEditingController _memberNoCtrl = TextEditingController();
  final TextEditingController _kraCtrl = TextEditingController();

  //Guarantor2
  final TextEditingController _nameGuarantorCtrl2 = TextEditingController();
  final TextEditingController _phoneGuarantorCtrl2= TextEditingController();
  final TextEditingController _idNoCtrl2 = TextEditingController();
  final TextEditingController _memberNoCtrl2 = TextEditingController();
  final TextEditingController _kraCtrl2 = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final steps = [
      CoolStep(
        title: '',
        subtitle: 'Applicant must meet the following requirements',
        content: Column(
          children: [
            Container(
              child: Row(
                children: <Widget>[
                  Flexible(
                    child: _buildText(
                      labelText: '1. The applicant must fill all the details in the loan application form. ',
                    ),
                  )

                ],
              ),
            ),
            SizedBox(height: 30,),
            Container(
              child: Row(
                children: <Widget>[
                  Flexible(
                    child: _buildText(
                      labelText: '2. One must be an active member for 4 months.  ',
                    ),
                  )

                ],
              ),
            ),
            SizedBox(height: 30,),
            Container(
              child: Row(
                children: <Widget>[
                  Flexible(
                    child: _buildText(
                      labelText: '3. We give four times the savings.',
                    ),
                  )

                ],
              ),
            ),
            SizedBox(height: 30,),
            Container(
              child: Row(
                children: <Widget>[
                  Flexible(
                    child: _buildText(
                      labelText: '4. Repayment period maximum 48 months. ',
                    ),
                  )

                ],
              ),
            ),
            SizedBox(height: 30,),
            Container(
              child: Row(
                children: <Widget>[
                  Flexible(
                    child: _buildText(
                      labelText: '5. At least three guarantors required.',
                    ),
                  )

                ],
              ),
            ),
            SizedBox(height: 30,),
          ],
        ),
        validation: () {
          return null;
        },
      ),
      CoolStep(
        title: 'Application Form',
        subtitle: 'APPLICANT’S INFORMATION. ',
        content: Form(
          key: _formKeyLoan,
          child: Column(
            children: [
              _buildTextField(
                labelText: 'Amount of loan applied',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'amount required';
                  }
                  return null;
                },
                controller: _amountCtrl,
              ),
              _buildTextField(
                labelText: 'Repayments Period(Months)',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'repayment is required';
                  }
                  return null;
                },
                controller: _repayCtrl,
              ),
              _buildTextField(
                labelText: 'Purpose of Loan',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'purpose is required';
                  }
                  return null;
                },
                controller: _purposeCtrl,
              ),
              _buildTextField(
                labelText: 'User Pin',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Pin is required';
                  }
                  return null;
                },
                controller: _pinCtrl,
              )
            ],
          ),
        ),
        validation: () {
          if (!_formKeyLoan.currentState.validate()) {
            return 'Fill form correctly';
          }
          confirmP(_pinCtrl.text);
          return null;
        },
      ),
      CoolStep(
        title: 'Application Form',
        subtitle: 'OTP PAGE. ',
        content: Form(
          key: _formKeyOtp,
          child: Column(
            children: [
              _buildTextField(
                labelText: 'Enter OTP received',
                validator: (value)  {
                  if (value.isEmpty) {
                    return 'otp required';
                  }
                  return null;
                },
                controller: _otpCtrl,
              ),
            ],
          ),
        ),
        validation: () {
          if (!_formKeyOtp.currentState.validate()) {
            return 'Fill form correctly';
          } else {
            return null;
          }


          // print('code');
          // print(code.toString());
          // if(code == 200){
          //   return null;
          // } else{
          //   return 'Error Proceeding, Application Failed';
          // }

        },
      ),
      CoolStep(
        title: 'Application Form',
        subtitle: 'First GUARANTOR DETAILS',
        content: Form(
          key: _formKeyGuarantor,
          child: Column(
            children: [
              _buildTextField(
                labelText: 'Guarantor Name ',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'name required';
                  }
                  return null;
                },
                controller: _nameGuarantorCtrl,
              ),
              _buildTextField(
                labelText: 'Guarantor National ID No ',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'id is required';
                  }
                  return null;
                },
                controller: _idNoCtrl,
              ),
              _buildTextField(
                labelText: 'Guarantor Phone No ',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'phone is required';
                  }
                  return null;
                },
                controller: _phoneGuarantorCtrl,
              ),
              _buildTextField(
                labelText: 'Guarantor Member No ',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'member No  is required';
                  }
                  return null;
                },
                controller: _memberNoCtrl,
              ),
              SizedBox(height: 10,),
              _buildTextField(
                labelText: 'Guarantor KRA Pin ',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Pin  is required';
                  }
                  return null;
                },
                controller: _kraCtrl,
              ),
            ],
          ),
        ),
        validation: () {
          if (!_formKeyGuarantor.currentState.validate()) {
            return 'Fill form correctly';
          } else {
            // addGuarantorsLoan(_nameGuarantorCtrl.text, _phoneGuarantorCtrl.text, _idNoCtrl.text,
            //     _memberNoCtrl.text,  _kraCtrl.text);
            // if(code == 200){
            //   return null;
            // } else{
            //   return 'Error Proceeding';
            // }
            return null;
          }

        },
      ),
      CoolStep(
        title: 'Application Form',
        subtitle: ' Second GUARANTOR DETAILS',
        content: Form(
          key: _formKeyGuarantor2,
          child: Column(
            children: [
              _buildTextField(
                labelText: 'Guarantor Name ',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'name required';
                  }
                  return null;
                },
                controller: _nameGuarantorCtrl2,
              ),
              _buildTextField(
                labelText: 'Guarantor National ID No ',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'id is required';
                  }
                  return null;
                },
                controller: _idNoCtrl2,
              ),
              _buildTextField(
                labelText: 'Guarantor Phone No ',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'phone is required';
                  }
                  return null;
                },
                controller: _phoneGuarantorCtrl2,
              ),
              _buildTextField(
                labelText: 'Guarantor Member No ',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'member No  is required';
                  }
                  return null;
                },
                controller: _memberNoCtrl2,
              ),
              SizedBox(height: 10,),
              _buildTextField(
                labelText: 'Guarantor KRA Pin ',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Pin  is required';
                  }
                  return null;
                },
                controller: _kraCtrl2,
              ),
            ],
          ),
        ),
        validation: () {
          if (!_formKeyGuarantor2.currentState.validate()) {
            return 'Fill form correctly';
          } else {
            // addGuarantorsLoan(_nameGuarantorCtrl2.text, _phoneGuarantorCtrl2.text, _idNoCtrl2.text,
            //     _memberNoCtrl2.text,  _kraCtrl2.text);
            // if(code == 200){
            //   return null;
            // } else{
            //   return 'Error Proceeding';
            // }
            var response =  applyNewJijengeLoan();
            if(response !=200 || response !=201){
              // showToast(response.data['error']);
              return 'Unable to proceed';
            } else {
              showToast('Proceed with Application');
              return null;
            }
            return null;
          }

        },
      ),
      CoolStep(
        title: 'Application Form',
        subtitle: 'BUSINESS DETAILS.',
        content: Form(
          key: _formKeyBusiness,
          child: Column(
            children: [
              _buildTextField(
                labelText: 'Name of Business ',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'business is required';
                  }
                  return null;
                },
                controller: _bNameCtrl,
              ),
              _buildTextField(
                labelText: 'Business Address ',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'address required';
                  }
                  return null;
                },
                controller: _bAddressCtrl,
              ),
              _buildTextField(
                labelText: 'Location of the business ',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'business is required';
                  }
                  return null;
                },
                controller: _bLocationCtrl,
              ),
              _buildTextField(
                labelText: 'Nature of Business',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'nature is required';
                  }
                  return null;
                },
                controller: _bNatureCtrl,
              ),
              _buildTextField(
                labelText: 'Business Income Per Month (Apprx)',
                validator: (value) {
                  if (value.isEmpty) {
                    return 'income is required';
                  }
                  return null;
                },
                controller: _bIncomeCtrl,
              ),
            ],
          ),
        ),
        validation: () {
          if (!_formKeyBusiness.currentState.validate()) {
            return 'Fill form correctly';
          }
          addBusinessLoan();
          return null;
        },
      ),
    ];

    final stepper = CoolStepper(
      showErrorSnackbar: false,
      onCompleted: () {
        print('Steps completed!');
        Navigator.pop(
            context, MaterialPageRoute(builder: (context) => MainStart()));
      },
      steps: steps,
      config: CoolStepperConfig(
        backText: 'PREV',
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: stepper,
      ),
    );
  }

  Widget _buildText({
    String labelText,
    FormFieldValidator<String> validator,
  }) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Text(labelText, style:TextStyle(
        fontSize: 2.48 * SizeConfig.textMultiplier,
        fontFamily: 'PoppinsRegular',
      )
      ),
    );
  }

  Widget _buildTextField({
    String labelText,
    FormFieldValidator<String> validator,
    TextEditingController controller,
  }) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: TextFormField(
        decoration: InputDecoration(
          labelText: labelText,
        ),
        validator: validator,
        controller: controller,
      ),
    );
  }

  Widget _buildSelector({
    BuildContext context,
    String name,
  }) {
    final isActive = name == selectedRole;

    return Expanded(
      child: AnimatedContainer(
        duration: Duration(milliseconds: 200),
        curve: Curves.easeInOut,
        decoration: BoxDecoration(
          color: isActive ? Theme.of(context).primaryColor : null,
          border: Border.all(
            width: 0,
          ),
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: RadioListTile(
          value: name,
          activeColor: Colors.white,
          groupValue: selectedRole,
          onChanged: (String v) {
            setState(() {
              selectedRole = v;
            });
          },
          title: Text(
            name,
            style: TextStyle(
              color: isActive ? Colors.white : null,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSelectorCycle({
    BuildContext context,
    String name,
  }) {
    final isActive = name == selectedCycle;

    return Expanded(
      child: AnimatedContainer(
        duration: Duration(milliseconds: 200),
        curve: Curves.easeInOut,
        decoration: BoxDecoration(
          color: isActive ? Theme.of(context).primaryColor : null,
          border: Border.all(
            width: 0,
          ),
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: RadioListTile(
          value: name,
          activeColor: Colors.white,
          groupValue: selectedCycle,
          onChanged: (String v) {
            setState(() {
              selectedCycle = v;
            });
          },
          title: Text(
            name,
            style: TextStyle(
              color: isActive ? Colors.white : null,
            ),
          ),
        ),
      ),
    );
  }
}