import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/network/payment.dart';
import 'package:travelx_v1/goodley/utils/spinner.dart';
import 'package:travelx_v1/goodley/widgets/toast.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../size_config.dart';
import 'package:intl/intl.dart';
import '../start.dart';

class Reports extends StatefulWidget {
  @override
  _ReportsState createState() => _ReportsState();
}

class _ReportsState extends State<Reports> {
  DateTime startDate = DateTime.now();
  DateTime enddate = DateTime.now();
  bool loading = false;
  String token = "";
  String user_id = "";


  Future<Null> _selectDate(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();


    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: startDate,
        firstDate: DateTime(2021),
        lastDate: DateTime(2101));
    if (picked != null && picked != startDate)
      setState(() {
        startDate = picked;
        prefs.setString('fromDate', DateFormat('yyyy-MM-dd hh:mm').format(picked));


      });
  }

  Future<Null> _endDate(BuildContext context) async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: enddate,
        firstDate: DateTime(2021),
        lastDate: DateTime(2101));
    if (picked != null && picked != enddate)
      setState(() {
        enddate = picked;
        prefs.setString('todate', DateFormat('yyyy-MM-dd hh:mm').format(picked));
      });
  }



  void getReports(user_id, token) async {
    print(DateFormat('yyyy-MM-dd hh:mm').format(startDate));
    print(DateFormat('yyyy-MM-dd hh:mm').format(enddate));
    String start = DateFormat('yyyy-MM-dd hh:mm').format(startDate);
    String end = DateFormat('yyyy-MM-dd hh:mm').format(enddate);
    String _openResult = 'Unknown';
    var response = await getReport(start, end, user_id, token);
    if (response != null && response.code != 400) {
//        await instance.fetchCitizenPost(id, token);
      setState(() {
        loading = false;
      });
      print(response.data);
      showToast('Fetching file');
      _launchURL(response.data);

    }
  }

  _launchURL(urls) async {
    var url = urls;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'Asset Projects',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: false,
      ),
      body: SingleChildScrollView(
        child:Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Card(
                  color: Color(0xffe0e0e0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 5,
                  child: Column(
                    children: [

                      InkWell(
                          child: Image(
                            image: AssetImage('assets/waterF.jpeg'),
                            fit: BoxFit.cover,
                          ),
                          onTap: () => launch('https://maps.app.goo.gl/q2biyqjRVixaDtn26')
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: InkWell(
                            child: Text(
                              'Waterfront Gardens Kithimani',
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xff2a2ad9),
                              ),
                            ),
                            onTap: () => launch('https://www.google.com/maps?q=-1.0221557,36.533441&z=17&hl=en')
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '50 X 100 Plots or 100 x 100 plots',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Colors.green,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 1.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '330K - 800K',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 1.0,
                      ),

                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          'Payment Plan 3 Months - 36 Months',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      // RaisedButton(
                      //   shape: new RoundedRectangleBorder(
                      //     borderRadius: new BorderRadius.circular(10.0),
                      //
                      //   ),
                      //   color: Theme.of(context).primaryColor,
                      //   onPressed: () {
                      //     showModalBottomSheet(context: context, builder: (BuildContext bc){
                      //       Map fundiData = ModalRoute.of(context).settings.arguments;
                      //       return ModalPayment();
                      //     }
                      //     );
                      //   },
                      //   child: Text(
                      //     'Donate',
                      //     style: TextStyle(
                      //       color: Colors.white,
                      //       fontSize: 14,
                      //       fontFamily: 'PoppinsRegular',
                      //     ),
                      //   ),
                      // ),

                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 10,),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Card(
                  color: Color(0xffe0e0e0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 5,
                  child: Column(
                    children: [

                      InkWell(
                          child: Image(
                            image: AssetImage('assets/kth.jpeg'),
                            fit: BoxFit.cover,
                          ),
                          onTap: () => launch('https://maps.app.goo.gl/q2biyqjRVixaDtn26')
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: InkWell(
                            child: Text(
                              'Waterfront Gardens Kithimani',
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xff2a2ad9),
                              ),
                            ),
                            onTap: () => launch('https://www.google.com/maps?q=-1.0221557,36.533441&z=17&hl=en')
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '50 X 100 Plots or 100 x 100 plots',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Colors.green,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 1.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '360K - 800K',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 1.0,
                      ),

                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          'Payment Plan 3 Months - 36 Months',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      // RaisedButton(
                      //   shape: new RoundedRectangleBorder(
                      //     borderRadius: new BorderRadius.circular(10.0),
                      //
                      //   ),
                      //   color: Theme.of(context).primaryColor,
                      //   onPressed: () {
                      //     showModalBottomSheet(context: context, builder: (BuildContext bc){
                      //       Map fundiData = ModalRoute.of(context).settings.arguments;
                      //       return ModalPayment();
                      //     }
                      //     );
                      //   },
                      //   child: Text(
                      //     'Donate',
                      //     style: TextStyle(
                      //       color: Colors.white,
                      //       fontSize: 14,
                      //       fontFamily: 'PoppinsRegular',
                      //     ),
                      //   ),
                      // ),

                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 10,),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Card(
                  color: Color(0xffe0e0e0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 5,
                  child: Column(
                    children: [

                      InkWell(
                          child: Image(
                            image: AssetImage('assets/sikunjemag.jpeg'),
                            fit: BoxFit.cover,
                          ),
                          onTap: () => launch('https://www.google.com/maps?q=-1.2933447,37.4645359&z=17&hl=en')
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: InkWell(
                            child: Text(
                              'SIku Njema gardens(Kithimani)',
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xff2a2ad9),
                              ),
                            ),
                            onTap: () => launch('https://www.google.com/maps?q=-1.0221557,36.533441&z=17&hl=en')
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '50 X 100 Plots',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Colors.green,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 1.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '200K',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 1.0,
                      ),

                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          'Payment Plan 36 Months',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      // RaisedButton(
                      //   shape: new RoundedRectangleBorder(
                      //     borderRadius: new BorderRadius.circular(10.0),
                      //
                      //   ),
                      //   color: Theme.of(context).primaryColor,
                      //   onPressed: () {
                      //     showModalBottomSheet(context: context, builder: (BuildContext bc){
                      //       Map fundiData = ModalRoute.of(context).settings.arguments;
                      //       return ModalPayment();
                      //     }
                      //     );
                      //   },
                      //   child: Text(
                      //     'Donate',
                      //     style: TextStyle(
                      //       color: Colors.white,
                      //       fontSize: 14,
                      //       fontFamily: 'PoppinsRegular',
                      //     ),
                      //   ),
                      // ),

                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 10,),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Card(
                  color: Color(0xffe0e0e0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 5,
                  child: Column(
                    children: [

                      SizedBox(
                        height: 1.0,
                      ),
                      InkWell(
                          child: Image(
                            height: 400,
                            width: MediaQuery.of(context).size.width-10,
                            image: AssetImage('assets/sikunjema.png' ),
                            fit: BoxFit.cover,
                          ),
                          onTap: () => launch('https://www.google.com/maps?q=-1.2933447,37.4645359&z=17&hl=en')
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: InkWell(
                            child: Text(
                              'Siku Njema gardens',
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xff2a2ad9),
                              ),
                            ),
                            onTap: () => launch('https://www.google.com/maps')
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: InkWell(
                            child: Text(
                              'Kithimani',
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xff2a2ad9),
                              ),
                            ),
                            onTap: () => launch('https://www.google.com/maps')
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '50 X 100 Plots',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '200K',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          'Payment Plan 36 Months',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Colors.green,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),

                      // RaisedButton(
                      //   shape: new RoundedRectangleBorder(
                      //     borderRadius: new BorderRadius.circular(10.0),
                      //
                      //   ),
                      //   color: Theme.of(context).primaryColor,
                      //   onPressed: () {
                      //     showModalBottomSheet(context: context, builder: (BuildContext bc){
                      //       Map fundiData = ModalRoute.of(context).settings.arguments;
                      //       return ModalPayment();
                      //     }
                      //     );
                      //   },
                      //   child: Text(
                      //     'Donate',
                      //     style: TextStyle(
                      //       color: Colors.white,
                      //       fontSize: 14,
                      //       fontFamily: 'PoppinsRegular',
                      //     ),
                      //   ),
                      // ),

                    ],
                  ),
                ),
              ),
            ),

            SizedBox(height: 10,),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Card(
                  color: Color(0xffe0e0e0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 5,
                  child: Column(
                    children: [

                      InkWell(
                          child: Image(
                            image: AssetImage('assets/birikamap.png'),
                            fit: BoxFit.cover,
                          ),
                          onTap: () => launch('https://www.google.com/maps?q=-1.5708741,36.8323974&z=17&hl=en')
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: InkWell(
                            child: Text(
                              'Birika Gardens(Kisaju near Ostritch Farm)',
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xff2a2ad9),
                              ),
                            ),
                            onTap: () => launch('https://www.google.com/maps?q=-1.0221557,36.533441&z=17&hl=en')
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '50 X 100 Plots',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Colors.green,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 1.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '560K',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 1.0,
                      ),

                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          'Payment Plan 36 Months',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      // RaisedButton(
                      //   shape: new RoundedRectangleBorder(
                      //     borderRadius: new BorderRadius.circular(10.0),
                      //
                      //   ),
                      //   color: Theme.of(context).primaryColor,
                      //   onPressed: () {
                      //     showModalBottomSheet(context: context, builder: (BuildContext bc){
                      //       Map fundiData = ModalRoute.of(context).settings.arguments;
                      //       return ModalPayment();
                      //     }
                      //     );
                      //   },
                      //   child: Text(
                      //     'Donate',
                      //     style: TextStyle(
                      //       color: Colors.white,
                      //       fontSize: 14,
                      //       fontFamily: 'PoppinsRegular',
                      //     ),
                      //   ),
                      // ),

                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 10,),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Card(
                  color: Color(0xffe0e0e0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 5,
                  child: Column(
                    children: [

                      InkWell(
                          child: Image(
                            image: AssetImage('assets/finovateland.jpeg'),
                            fit: BoxFit.cover,
                          ),
                          onTap: () => launch('https://www.google.com/maps?q=-1.5708741,36.8323974&z=17&hl=en')
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: InkWell(
                            child: Text(
                              'Birika Gardens(Kisaju near Ostritch Farm)',
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xff2a2ad9),
                              ),
                            ),
                            onTap: () => launch('https://www.google.com/maps?q=-1.0221557,36.533441&z=17&hl=en')
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '50 X 100 Plots',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Colors.green,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 1.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '560K',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 1.0,
                      ),

                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          'Payment Plan 36 Months',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      // RaisedButton(
                      //   shape: new RoundedRectangleBorder(
                      //     borderRadius: new BorderRadius.circular(10.0),
                      //
                      //   ),
                      //   color: Theme.of(context).primaryColor,
                      //   onPressed: () {
                      //     showModalBottomSheet(context: context, builder: (BuildContext bc){
                      //       Map fundiData = ModalRoute.of(context).settings.arguments;
                      //       return ModalPayment();
                      //     }
                      //     );
                      //   },
                      //   child: Text(
                      //     'Donate',
                      //     style: TextStyle(
                      //       color: Colors.white,
                      //       fontSize: 14,
                      //       fontFamily: 'PoppinsRegular',
                      //     ),
                      //   ),
                      // ),

                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 10,),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Card(
                  color: Color(0xffe0e0e0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 5,
                  child: Column(
                    children: [

                      SizedBox(
                        height: 10.0,
                      ),
                      InkWell(
                          child: Image(
                            image: AssetImage('assets/naivasha.jpeg'),
                            fit: BoxFit.cover,
                          ),
                          onTap: () => launch('https://www.google.com/maps?q=-1.0221557,36.533441&z=17&hl=en')
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: InkWell(
                            child: Text(
                              'Naivasha dry port plots',
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xff2a2ad9),
                              ),
                            ),
                            onTap: () => launch('https://www.google.com/maps?q=-1.0221557,36.533441&z=17&hl=en')
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '50 X 100 Plots',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Colors.green,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 1.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '350K',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 1.0,
                      ),

                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          'Payment Plan 36 Months',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      // RaisedButton(
                      //   shape: new RoundedRectangleBorder(
                      //     borderRadius: new BorderRadius.circular(10.0),
                      //
                      //   ),
                      //   color: Theme.of(context).primaryColor,
                      //   onPressed: () {
                      //     showModalBottomSheet(context: context, builder: (BuildContext bc){
                      //       Map fundiData = ModalRoute.of(context).settings.arguments;
                      //       return ModalPayment();
                      //     }
                      //     );
                      //   },
                      //   child: Text(
                      //     'Donate',
                      //     style: TextStyle(
                      //       color: Colors.white,
                      //       fontSize: 14,
                      //       fontFamily: 'PoppinsRegular',
                      //     ),
                      //   ),
                      // ),

                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 10,),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Card(
                  color: Color(0xffe0e0e0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 5,
                  child: Column(
                    children: [

                      SizedBox(
                        height: 10.0,
                      ),
                      InkWell(
                          child: Image(
                            height: 250,
                            image: AssetImage('assets/WhatsApp Image 2021-06-10 at 17.35.56.jpeg' ),
                            fit: BoxFit.cover,
                          ),
                          onTap: () => launch('https://www.google.com/maps')
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: InkWell(
                            child: Text(
                              'Siku Njema gardens',
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xff2a2ad9),
                              ),
                            ),
                            onTap: () => launch('https://www.google.com/maps?q=-1.2933447,37.4645359&z=17&hl=en')
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: InkWell(
                            child: Text(
                              'Kithimani',
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'PoppinsMedium',
                                color: Color(0xff2a2ad9),
                              ),
                            ),
                            onTap: () => launch('https://www.google.com/maps')
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '50 X 100 Plots',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          '200K',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Color(0xFF212121),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          'Payment Plan 36 Months',
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'PoppinsMedium',
                            color: Colors.green,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),

                      // RaisedButton(
                      //   shape: new RoundedRectangleBorder(
                      //     borderRadius: new BorderRadius.circular(10.0),
                      //
                      //   ),
                      //   color: Theme.of(context).primaryColor,
                      //   onPressed: () {
                      //     showModalBottomSheet(context: context, builder: (BuildContext bc){
                      //       Map fundiData = ModalRoute.of(context).settings.arguments;
                      //       return ModalPayment();
                      //     }
                      //     );
                      //   },
                      //   child: Text(
                      //     'Donate',
                      //     style: TextStyle(
                      //       color: Colors.white,
                      //       fontSize: 14,
                      //       fontFamily: 'PoppinsRegular',
                      //     ),
                      //   ),
                      // ),

                    ],
                  ),
                ),
              ),
            ),

            SizedBox(height: 10,),
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   child: Padding(
            //     padding: const EdgeInsets.all(5.0),
            //     child: Card(
            //       color: Color(0xffe0e0e0),
            //       shape: RoundedRectangleBorder(
            //         borderRadius: BorderRadius.circular(10.0),
            //       ),
            //       elevation: 5,
            //       child: Column(
            //         children: [
            //
            //           SizedBox(
            //             height: 10.0,
            //           ),
            //           InkWell(
            //               child: Image(
            //                 height: 250,
            //                 image: AssetImage('assets/WhatsApp Image 2021-06-08 at 14.12.54.jpeg' ),
            //                 fit: BoxFit.cover,
            //               ),
            //               onTap: () => launch('https://www.google.com/maps')
            //           ),
            //           SizedBox(
            //             height: 20.0,
            //           ),
            //           Padding(
            //             padding: const EdgeInsets.all(5.0),
            //             child: InkWell(
            //                 child: Text(
            //                   'Nanyuki',
            //                   style: TextStyle(
            //                     fontSize: 15,
            //                     fontFamily: 'PoppinsMedium',
            //                     color: Color(0xff2a2ad9),
            //                   ),
            //                 ),
            //                 onTap: () => launch('https://www.google.com/maps')
            //             ),
            //           ),
            //           SizedBox(
            //             height: 10.0,
            //           ),
            //           Padding(
            //             padding: const EdgeInsets.all(5.0),
            //             child: Text(
            //               '50 X 100 Plots',
            //               style: TextStyle(
            //                 fontSize: 15,
            //                 fontFamily: 'PoppinsMedium',
            //                 color: Color(0xFF212121),
            //               ),
            //             ),
            //           ),
            //           SizedBox(
            //             height: 10.0,
            //           ),
            //           Padding(
            //             padding: const EdgeInsets.all(5.0),
            //             child: Text(
            //               '180K',
            //               style: TextStyle(
            //                 fontSize: 15,
            //                 fontFamily: 'PoppinsMedium',
            //                 color: Color(0xFF212121),
            //               ),
            //             ),
            //           ),
            //           SizedBox(
            //             height: 10.0,
            //           ),
            //           Padding(
            //             padding: const EdgeInsets.all(5.0),
            //             child: Text(
            //               'Payment Plan 36 Months',
            //               style: TextStyle(
            //                 fontSize: 15,
            //                 fontFamily: 'PoppinsMedium',
            //                 color: Colors.green,
            //               ),
            //             ),
            //           ),
            //           SizedBox(
            //             height: 10.0,
            //           ),
            //
            //           // RaisedButton(
            //           //   shape: new RoundedRectangleBorder(
            //           //     borderRadius: new BorderRadius.circular(10.0),
            //           //
            //           //   ),
            //           //   color: Theme.of(context).primaryColor,
            //           //   onPressed: () {
            //           //     showModalBottomSheet(context: context, builder: (BuildContext bc){
            //           //       Map fundiData = ModalRoute.of(context).settings.arguments;
            //           //       return ModalPayment();
            //           //     }
            //           //     );
            //           //   },
            //           //   child: Text(
            //           //     'Donate',
            //           //     style: TextStyle(
            //           //       color: Colors.white,
            //           //       fontSize: 14,
            //           //       fontFamily: 'PoppinsRegular',
            //           //     ),
            //           //   ),
            //           // ),
            //
            //         ],
            //       ),
            //     ),
            //   ),
            // ),
            // SizedBox(height: 10,),fjujaF
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   child: Padding(
            //     padding: const EdgeInsets.all(5.0),
            //     child: Card(
            //       color: Color(0xffe0e0e0),
            //       shape: RoundedRectangleBorder(
            //         borderRadius: BorderRadius.circular(10.0),
            //       ),
            //       elevation: 5,
            //       child: Column(
            //         children: [
            //
            //           SizedBox(
            //             height: 10.0,
            //           ),
            //           InkWell(
            //               child: Image(
            //                 height: 250,
            //                 image: AssetImage('assets/WhatsApp Image 2021-06-08 at 14.12.54.jpeg' ),
            //                 fit: BoxFit.cover,
            //               ),
            //               onTap: () => launch('https://www.google.com/maps')
            //           ),
            //           SizedBox(
            //             height: 20.0,
            //           ),
            //           Padding(
            //             padding: const EdgeInsets.all(5.0),
            //             child: InkWell(
            //                 child: Text(
            //                   'JujaFarm-Athi',
            //                   style: TextStyle(
            //                     fontSize: 15,
            //                     fontFamily: 'PoppinsMedium',
            //                     color: Color(0xff2a2ad9),
            //                   ),
            //                 ),
            //                 onTap: () => launch('https://www.google.com/maps')
            //             ),
            //           ),
            //           SizedBox(
            //             height: 10.0,
            //           ),
            //           Padding(
            //             padding: const EdgeInsets.all(5.0),
            //             child: Text(
            //               '40 X 90 Plots',
            //               style: TextStyle(
            //                 fontSize: 15,
            //                 fontFamily: 'PoppinsMedium',
            //                 color: Color(0xFF212121),
            //               ),
            //             ),
            //           ),
            //           SizedBox(
            //             height: 10.0,
            //           ),
            //           Padding(
            //             padding: const EdgeInsets.all(5.0),
            //             child: Text(
            //               '450K',
            //               style: TextStyle(
            //                 fontSize: 15,
            //                 fontFamily: 'PoppinsMedium',
            //                 color: Color(0xFF212121),
            //               ),
            //             ),
            //           ),
            //           SizedBox(
            //             height: 10.0,
            //           ),
            //           Padding(
            //             padding: const EdgeInsets.all(5.0),
            //             child: Text(
            //               'Payment Plan 36 Months',
            //               style: TextStyle(
            //                 fontSize: 15,
            //                 fontFamily: 'PoppinsMedium',
            //                 color: Colors.green,
            //               ),
            //             ),
            //           ),
            //           SizedBox(
            //             height: 10.0,
            //           ),
            //
            //           // RaisedButton(
            //           //   shape: new RoundedRectangleBorder(
            //           //     borderRadius: new BorderRadius.circular(10.0),
            //           //
            //           //   ),
            //           //   color: Theme.of(context).primaryColor,
            //           //   onPressed: () {
            //           //     showModalBottomSheet(context: context, builder: (BuildContext bc){
            //           //       Map fundiData = ModalRoute.of(context).settings.arguments;
            //           //       return ModalPayment();
            //           //     }
            //           //     );
            //           //   },
            //           //   child: Text(
            //           //     'Donate',
            //           //     style: TextStyle(
            //           //       color: Colors.white,
            //           //       fontSize: 14,
            //           //       fontFamily: 'PoppinsRegular',
            //           //     ),
            //           //   ),
            //           // ),
            //
            //         ],
            //       ),
            //     ),
            //   ),
            // ),
            // SizedBox(height: 10,),
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   child: Padding(
            //     padding: const EdgeInsets.all(5.0),
            //     child: Card(
            //       color: Color(0xffe0e0e0),
            //       shape: RoundedRectangleBorder(
            //         borderRadius: BorderRadius.circular(10.0),
            //       ),
            //       elevation: 5,
            //       child: Column(
            //         children: [
            //
            //
            //           InkWell(
            //               child: Image(
            //                 height: 250,
            //                 image: AssetImage('assets/plot.jpeg' ),
            //                 fit: BoxFit.cover,
            //               ),
            //               onTap: () => launch('https://www.google.com/maps?q=0.3552126,36.5542937&z=17&hl=en')
            //           ),
            //           SizedBox(
            //             height: 20.0,
            //           ),
            //           Padding(
            //             padding: const EdgeInsets.all(5.0),
            //             child: InkWell(
            //                 child: Text(
            //                   'Siku njema gardens Rumuruti',
            //                   style: TextStyle(
            //                     fontSize: 15,
            //                     fontFamily: 'PoppinsMedium',
            //                     color: Color(0xff2a2ad9),
            //                   ),
            //                 ),
            //                 onTap: () => launch('https://www.google.com/maps?q=0.3552126,36.5542937&z=17&hl=en')
            //             ),
            //           ),
            //           SizedBox(
            //             height: 10.0,
            //           ),
            //           Padding(
            //             padding: const EdgeInsets.all(5.0),
            //             child: Text(
            //               '100 X 100 Plots',
            //               style: TextStyle(
            //                 fontSize: 15,
            //                 fontFamily: 'PoppinsMedium',
            //                 color: Color(0xFF212121),
            //               ),
            //             ),
            //           ),
            //           SizedBox(
            //             height: 10.0,
            //           ),
            //           Padding(
            //             padding: const EdgeInsets.all(5.0),
            //             child: Text(
            //               '160K',
            //               style: TextStyle(
            //                 fontSize: 15,
            //                 fontFamily: 'PoppinsMedium',
            //                 color: Color(0xFF212121),
            //               ),
            //             ),
            //           ),
            //           SizedBox(
            //             height: 10.0,
            //           ),
            //           Padding(
            //             padding: const EdgeInsets.all(5.0),
            //             child: Text(
            //               'Payment Plan 36 Months',
            //               style: TextStyle(
            //                 fontSize: 15,
            //                 fontFamily: 'PoppinsMedium',
            //                 color: Colors.green,
            //               ),
            //             ),
            //           ),
            //           SizedBox(
            //             height: 10.0,
            //           ),
            //
            //           // RaisedButton(
            //           //   shape: new RoundedRectangleBorder(
            //           //     borderRadius: new BorderRadius.circular(10.0),
            //           //
            //           //   ),
            //           //   color: Theme.of(context).primaryColor,
            //           //   onPressed: () {
            //           //     showModalBottomSheet(context: context, builder: (BuildContext bc){
            //           //       Map fundiData = ModalRoute.of(context).settings.arguments;
            //           //       return ModalPayment();
            //           //     }
            //           //     );
            //           //   },
            //           //   child: Text(
            //           //     'Donate',
            //           //     style: TextStyle(
            //           //       color: Colors.white,
            //           //       fontSize: 14,
            //           //       fontFamily: 'PoppinsRegular',
            //           //     ),
            //           //   ),
            //           // ),
            //
            //         ],
            //       ),
            //     ),
            //   ),
            // ),

          ],
        ),
        // GestureDetector(
        //   behavior: HitTestBehavior.opaque,
        //   onTap: () {
        //     FocusScope.of(context).unfocus();
        //   },
        //   child: Column(
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: [
        //       Column(
        //
        //         crossAxisAlignment: CrossAxisAlignment.start,
        //         children: [
        //
        //           SizedBox(
        //             height: 25.0,
        //           ),
        //           Padding(
        //             padding: EdgeInsets.fromLTRB(20,4,50,4),
        //             child: Text(
        //               'Download your savings statement from your app.',
        //               style: TextStyle(
        //                 color: Theme.of(context).accentColor,
        //                 fontSize: 2.58 * SizeConfig.textMultiplier,
        //                 fontFamily: 'PoppinsRegular',
        //               ),
        //               textAlign: TextAlign.center,
        //             ),
        //           ),
        //           SizedBox(
        //             height: 20.0,
        //           ),
        //           //Email/phone
        //           Padding(
        //             padding: EdgeInsets.fromLTRB(20,10,50,10),
        //             child: Container(
        //               decoration: BoxDecoration(
        //                   border: Border.all(
        //                     color: Theme.of(context).primaryColor,
        //                   ),
        //                   borderRadius:
        //                   BorderRadius.all(Radius.circular(20.0))),
        //               margin: EdgeInsets.all(7.0),
        //               height: 7.8 * SizeConfig.heightSizeMultiplier,
        //               width: 54.6 * SizeConfig.heightSizeMultiplier,
        //               child: GestureDetector(
        //                 behavior: HitTestBehavior.opaque,
        //                 onTap: () {
        //                   _selectDate(context);
        //                 },
        //                 child: Column(
        //                   crossAxisAlignment: CrossAxisAlignment.center,
        //                   mainAxisAlignment: MainAxisAlignment.center,
        //                   children: <Widget>[
        //                     Text(
        //                       'Start Date',
        //                       style: TextStyle(
        //                           color: Color(0xFF5E6D77),
        //                           fontSize: 16.0,
        //                           fontFamily: 'PoppinsRegular'),
        //                     ),
        //                     Text(
        //                       "${startDate.toLocal()}".split(' ')[0],
        //                       style: TextStyle(
        //                           color: Theme.of(context).accentColor,
        //                           fontSize: 16.0,
        //                           fontFamily: 'PoppinsRegular'),
        //                     ),
        //                   ],
        //                 ),
        //               ),
        //             ),
        //           ),
        //           //Email/phone
        //           Padding(
        //             padding: EdgeInsets.fromLTRB(20,10,50,10),
        //             child: Container(
        //               decoration: BoxDecoration(
        //                   border: Border.all(
        //                     color: Theme.of(context).primaryColor,
        //                   ),
        //                   borderRadius:
        //                   BorderRadius.all(Radius.circular(20.0))),
        //               margin: EdgeInsets.all(7.0),
        //               height: 7.8 * SizeConfig.heightSizeMultiplier,
        //               width: 54.6 * SizeConfig.heightSizeMultiplier,
        //               child: GestureDetector(
        //                 behavior: HitTestBehavior.opaque,
        //                 onTap: () {
        //                   _endDate(context);
        //                 },
        //                 child: Column(
        //                   crossAxisAlignment: CrossAxisAlignment.center,
        //                   mainAxisAlignment: MainAxisAlignment.center,
        //                   children: <Widget>[
        //                     Text(
        //                       'End Date',
        //                       style: TextStyle(
        //                           color: Color(0xFF5E6D77),
        //                           fontSize: 16.0,
        //                           fontFamily: 'PoppinsRegular'),
        //                     ),
        //                     Text(
        //                       "${enddate.toLocal()}".split(' ')[0],
        //                       style: TextStyle(
        //                           color: Theme.of(context).accentColor,
        //                           fontSize: 16.0,
        //                           fontFamily: 'PoppinsRegular'),
        //                     ),
        //                   ],
        //                 ),
        //               ),
        //             ),
        //           ),
        //           SizedBox(
        //             height: 7.0,
        //           ),
        //           //NextButton
        //           Padding(
        //             padding: EdgeInsets.fromLTRB(20,4,20,4),
        //             child: Container(
        //                 height: 6.1 * SizeConfig.heightSizeMultiplier,
        //                 width: MediaQuery.of(context).size.width-10,
        //                 child: loading ? spinKit() :RaisedButton(
        //                   onPressed: ()  async {
        //                     setState(() {
        //                       loading = true;
        //                     });
        //                     SharedPreferences prefs = await SharedPreferences.getInstance();
        //
        //                     user_id = prefs.getString('user');
        //                     token = prefs.getString('accessToken');
        //                     getReports(user_id, token);
        //
        //                   },
        //                   child: Row(
        //                     mainAxisAlignment: MainAxisAlignment.center,
        //                     children: [
        //                       Padding(
        //                         padding: const EdgeInsets.fromLTRB(5,0,15,0),
        //                         child: Icon(Icons.cloud_download_sharp, size: 30, color: Colors.white,),
        //                       ),
        //                       Text(
        //                         'DOWNLOAD REPORT ',
        //                         style: TextStyle(fontSize: 2.1 * SizeConfig.textMultiplier, fontWeight: FontWeight.bold, color: Colors.white),
        //                       ),
        //
        //
        //                     ],
        //                   ),
        //                   color: Theme.of(context).primaryColor,
        //                 )),
        //           ),
        //
        //
        //         ],
        //       )
        //     ],
        //   ),
        // ),
      ),
    );
  }
}
