import 'package:flutter/material.dart';

import '../../size_config.dart';

class LabeledRadio extends StatelessWidget {
  const LabeledRadio({
    this.label,
    this.padding,
    this.groupValue,
    this.value,
    this.onChanged,
  });

  final String label;
  final EdgeInsets padding;
  final bool groupValue;
  final bool value;
  final Function onChanged;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (value != groupValue)
          onChanged(value);
      },
      child: Container(
        height: 30,
        padding: padding,
        child: Row(
          children: <Widget>[
            Radio<bool>(
              groupValue: groupValue,
              value: value,
              onChanged: (bool newValue) {
                onChanged(newValue);
              },
            ),
            Text(label,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 2.58 * SizeConfig.textMultiplier,
                fontFamily: 'PoppinsRegular'
            ),),
          ],
        ),
      ),
    );
  }
}