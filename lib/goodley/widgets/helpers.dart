import 'package:flutter/material.dart';
final inputDecoration = InputDecoration(
    border: OutlineInputBorder(
      borderRadius: const BorderRadius.all(Radius.circular(5)),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: const BorderRadius.all(Radius.circular(5)),
    ),
    contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5)
);
String textValidator(String input, String message) {
  if (input.toString().isEmpty) {
    return message;
  }
  return null;
}