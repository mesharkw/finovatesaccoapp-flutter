import 'package:flutter/material.dart';

Widget input({hint = '', controller}) {
  return Container(
      height: 40,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.grey[100],
          border: Border.all(width: 0, color: Colors.grey)),
      padding: EdgeInsets.only(left: 15),
      margin: EdgeInsets.only(top: 5, bottom: 10),
      child: Center(
        child: TextFormField(
          controller: controller,
            cursorColor: Colors.black,
            keyboardType: TextInputType.text,
            decoration: new InputDecoration(
              isDense: true,
              hintText: hint,
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
//            hintText: 'sLabel'
            )),
      ));
}
