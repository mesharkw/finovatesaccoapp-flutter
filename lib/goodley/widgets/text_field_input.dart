import 'package:flutter/material.dart';
import 'package:travelx_v1/goodley/widgets/helpers.dart';
class TextFieldInput extends StatefulWidget {
  final IconData icon;
  final String hint;
  final Key fieldKey;
  final TextInputType inputType;
  final bool obscured;
  final int maxLength;
  final FormFieldSetter<dynamic> onSaved;
  final FormFieldValidator<dynamic> validator;
  final ValueChanged<dynamic> onFieldSubmitted;
  final TextEditingController controller;
  TextFieldInput({
    this.fieldKey,
    this.icon,
    this.hint,
    this.inputType,
    this.obscured = false,
    this.maxLength,
    this.onSaved,
    this.validator,
    this.onFieldSubmitted,
    this.controller
  });
  @override
  _TextFieldInputState createState() => _TextFieldInputState();
}
class _TextFieldInputState extends State<TextFieldInput> {
  final TextStyle _inputTextFieldStyle = TextStyle(
      color: Colors.grey[900],
      fontSize: 17,
      letterSpacing: 1
  );
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLines: 1,
      keyboardType: widget.inputType ?? TextInputType.text,
      style: _inputTextFieldStyle,
      textAlignVertical: TextAlignVertical.center,
      onSaved: widget.onSaved,
      controller: widget.controller,
      validator: widget.validator,
      obscureText: widget.obscured,
      maxLength: widget.maxLength,
      onFieldSubmitted: widget.onFieldSubmitted,
      decoration: inputDecoration,
    );
  }
}