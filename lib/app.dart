import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:travelx_v1/goodley/start.dart';
import 'package:travelx_v1/screens/forgot_password.dart';
import 'package:travelx_v1/screens/reset_pass.dart';
import 'package:travelx_v1/screens/set_question.dart';
import 'package:travelx_v1/size_config.dart';
import 'screens/splash.dart';
import 'package:travelx_v1/goodley/pages/splashScreen.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            return MaterialApp(
              theme: ThemeData(
                scaffoldBackgroundColor: Colors.white,
                primaryColor: Color(0xFF00008b),
                accentColor: Color(0xFF9B9FA6),
                fontFamily: 'poetsen-one',
              ),
              title: "",
              debugShowCheckedModeBanner: false,
              initialRoute: '/',
              routes: {
                '/': (context) => Splash(),
                '/home': (context) => MainStart(),
                '/pay': (context) => PayPage(),
                '/confirmPay': (context) => ConfirmPayPage(),
                '/login': (context) => ForgotPassword(),
                '/splash': (context) => SplashScreen(),
                '/signup': (context) => SignUp(),
                '/otp': (context) => ForgotPasswordOtp(),
                '/reset': (context) => ResetPass(),
                '/security': (context) => SetQuestion(),
              },
            );
          },
        );
      },
    );
  }
}
