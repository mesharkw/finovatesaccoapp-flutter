import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/pages/splashScreen.dart';
import 'package:travelx_v1/goodley/start.dart';
import 'package:travelx_v1/screens/sign_up.dart';
import 'package:travelx_v1/size_config.dart';
import 'forgot_password.dart';
import 'login.dart';
import 'onboarding.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {

  getPrefsr () async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    bool  loggged = prefs.getBool('logged_in');
    // Navigator.pushReplacementNamed(context, '/login');
    if (loggged == null) {


      Navigator.pushReplacementNamed(context, '/login');
    } else if (loggged) {
      Navigator.pushReplacementNamed(context, '/splash');
    }
  }
  @override
  Widget build(BuildContext context) {
    getPrefsr();
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        left: false,
        right: false,
        bottom: false,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding:
                  EdgeInsets.only(top: 25 * SizeConfig.heightSizeMultiplier),
              child: Column(
                children: [
                  ClipRRect(

                      child: Image(
                        height: 100,
                        width: 100,
                        image: AssetImage('assets/fsac.jpeg'),
                        fit: BoxFit.cover,
                      ),

                  ),
                  FittedBox(
                    child: Text(
                      ' App',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 5* SizeConfig.textMultiplier,
                          fontFamily: 'PoetsenOne'),
                    ),
                  ),
                  FittedBox(
                    child: Text(
                      'Empower & Connect',
                      style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontSize: 2.5 * SizeConfig.textMultiplier,
                          fontFamily: 'PoppinsRegular'),
                    ),
                  ),
                ],
              ),
            ),
            Column(

              children: [
                Container(

                    constraints: BoxConstraints(
                      minHeight: 6.2 * SizeConfig.heightSizeMultiplier,
                      maxHeight: 9.3 * SizeConfig.heightSizeMultiplier,
                      minWidth: 43.7 * SizeConfig.heightSizeMultiplier,
                      maxWidth: 46.8 * SizeConfig.heightSizeMultiplier,
                    ),
                    decoration: BoxDecoration(
//                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.all(Radius.circular(
                          6.2 * SizeConfig.heightSizeMultiplier)),
                    ),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => ForgotPassword()));
                        print('SIGN IN');
                      },
                      elevation: 2.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(
                              6.2 * SizeConfig.heightSizeMultiplier),
                        ),
                      ),
                      color: Theme.of(context).primaryColor,
                      child: Text(
                        'SIGN IN',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 2.8 * SizeConfig.textMultiplier,
                          fontFamily: 'PoppinsRegular',
                        ),
                      ),
                    )),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 40,)

                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
