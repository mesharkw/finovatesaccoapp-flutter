//ResetPass Page
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:travelx_v1/goodley/models/all_api.dart';
import 'package:travelx_v1/goodley/models/counties.dart';
import 'package:travelx_v1/goodley/network/otp_api.dart';
import 'package:travelx_v1/goodley/utils/common_utils.dart';
import 'package:travelx_v1/goodley/utils/spinner.dart';
import 'package:travelx_v1/goodley/widgets/toast.dart';
import 'package:travelx_v1/screens/forgot_password.dart';
import 'package:travelx_v1/size_config.dart';

class ResetPass extends StatefulWidget {
  @override
  _ResetPassState createState() => _ResetPassState();
}

class Item {
  const Item(this.name,this.id);
  final String name;
  final String id;
}
class Company {
  int id;
  String name;

  Company(this.id, this.name);

  static List<Company> getCompanies() {
    return <Company>[
      Company(1, 'Apple'),
      Company(2, 'Google'),
      Company(3, 'Samsung'),
      Company(4, 'Sony'),
      Company(5, 'LG'),
    ];
  }
}

class _ResetPassState extends State<ResetPass> {
  bool loading = false;
  final _formKey_signup = GlobalKey<FormState>();
  final f = new DateFormat('yyyy-MM-dd');

  final fname = new TextEditingController();
  final lname = new TextEditingController();
  final address = new TextEditingController();
  final contact = new TextEditingController();
  final email = new TextEditingController();
  final dob = new TextEditingController();
  final imname = new TextEditingController();
  final imphone = new TextEditingController();
  final dno = new TextEditingController();
  final marital = new TextEditingController();
  final kra = new TextEditingController();
  final county = new TextEditingController();
  final source = new TextEditingController();
  final contri = new TextEditingController();
  final contrieff = new TextEditingController();
  final detail = new TextEditingController();
  final answer = new TextEditingController();

  String business;
  String  employed;
  String  dtype;
  String  phoneNumber;

  int _groupValue = -1;
  String gender;
  String countyID;
  String countyName;
  String _chosenValue;
  List docType = [];
  bool _show = true;
  bool _accept = false;
  List<Counties> security;
  List<Counties> counties;
  List<Counties> sAagents;
  String Btype = '1a862df26f6943997cef90233877a4fe';
  void _handleRadioValueChanged(int value) {
    setState(() {
      this._groupValue = value;
    });
  }


  Future<void> getMaritalStatus(String phone,String btype) async {
    if (btype != null) {
      var response = await getSecurityQ(formatPhoneNumber(phone),btype);

      if (response != null || response.isNotEmpty ) {

        this.security = response;
        setState(() {

        });
      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }
  String _mySelection;
  String _mySelectionM;
  String agent;
  //widget Counties
  Widget DropDown(List data, String text){
    if(data!=null)
    {
      return DropdownButton(
        items: data.map((item) {
          return new DropdownMenuItem(
            child: new Text(
              item.name,
              style: TextStyle(fontSize: 12.0),
            ),
            value: item.id,
          );
        }).toList(),
        hint: Text(
          text,
          style: TextStyle(
              color: Colors.black45,
              fontSize: 12.0
          ),),
        onChanged: (newVal) {
          setState(() {
            _mySelection = newVal;
            // customerid = newVal;
            // print('customrid:' + customerid.toString());
          });
        },
        value: _mySelection,
      );
    }
    else{
      return new Center(
        child: CircularProgressIndicator(),
      );
    }
  }

  //widget Marital
  Widget DropDownM(List data, String text){
    if(data!=null)
    {
      return DropdownButton(
        items: data.map((item) {
          return new DropdownMenuItem(
            child: new Text(
              item.name,
              style: TextStyle(fontSize: 12.0),
            ),
            value: item.id,
          );
        }).toList(),
        hint: Text(
          text,
          style: TextStyle(
              color: Colors.black45,
              fontSize: 12.0
          ),),
        onChanged: (newVal) {
          setState(() {
            _mySelectionM = newVal;
            // customerid = newVal;
            // print('customrid:' + customerid.toString());
          });
        },
        value: _mySelectionM,
      );
    }
    else{
      return new Center(
        child: CircularProgressIndicator(),
      );
    }
  }

  //widget SalesAgent
  Widget DropDownSales(List data, String text){
    if(data!=null)
    {
      return DropdownButton(
        items: data.map((item) {
          return new DropdownMenuItem(
            child: new Text(
              item.name,
              style: TextStyle(fontSize: 12.0),
            ),
            value: item.id,
          );
        }).toList(),
        hint: Text(
          text,
          style: TextStyle(
              color: Colors.black45,
              fontSize: 12.0
          ),),
        onChanged: (newVal) {
          setState(() {
            agent = newVal;
            // customerid = newVal;
            // print('customrid:' + customerid.toString());
          });
        },
        value: agent,
      );
    }
    else{
      return new Center(
        child: CircularProgressIndicator(),
      );
    }
  }

  Future<void> getDocuType(String btype) async {
    if (btype != null) {
      var response = await getDocType(btype);

      if (response != null || response.isNotEmpty ) {
        this.docType = response;
        for (var i = 0; i < this.docType.length; i++) {

          print(this.docType[i].name);
        }
        setState(() {
          this.dtype =  this.docType[1].id;
        });
      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }
  Future<void> getSales(String btype) async {
    if (btype != null) {
      var response = await getSalesAgent(btype);

      if (response != null || response.isNotEmpty ) {
        this.sAagents = response;
        for (var i = 0; i < this.docType.length; i++) {

          print(this.sAagents[i].name);
        }
        setState(() {
          this.agent =  this.sAagents[1].id;
        });
      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }

  Future<void> sendReg(contact,btype,answer,agent) async {


    if (contact != null) {
      AllApi response = await resetPass(formatPhoneNumber(contact) ,btype,answer,agent);
      print(response.code);
      print(response.data);

      if (response != null && response.code == 200 || response != null && response.code == 201 ) {
//        await instance.fetchCitizenPost(id, token);
        print('200,201');
        showToast('Your New Pass is : ${response.data['newpass']}');
        setState(() {
          Navigator.pushReplacementNamed(context, '/login');
        });

      } else if (response.code == 400 ) {
        print('400');

        setState(() {

          loading = false;
        });
        print('here');
      } else if(response.code == 401){
        print('401');
        showToast('Unable to verify User');

        setState(() {

          loading = false;
        });

      }
    }

  }

  /// Valid phone number
  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }
//Radio button (Gender)
  _genderRadio(int groupValue, handleRadioValueChanged) =>
      Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
        Text(
          'Gender',
          style: new TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal),
        ),
        Row(
          children: <Widget>[
            Radio(
                value: 0,
                groupValue: groupValue,
                onChanged: handleRadioValueChanged),
            Text(
              "Male",
              style: new TextStyle(
                fontSize: 13.0,
              ),
            ),
            Radio(
                value: 1,
                groupValue: groupValue,
                onChanged: handleRadioValueChanged),
            Text(
              "Female",
              style: new TextStyle(
                fontSize: 13.0,
              ),
            ),
          ],
        )
      ]);


  @override
  void initState()  {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Form(
            key: _formKey_signup,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //Logo
                Padding(
                  padding: const EdgeInsets.fromLTRB(15,50,8,8),
                  child:
                  Center(
                    child: Image(
                      height: 70,
                      width:  MediaQuery.of(context).size.width/2,
                      image: AssetImage('assets/fsac.jpeg'),
                      fit: BoxFit.cover,
                    ),
                  ),

                  // Image(
                  //   height: 80,
                  //   width: 80,
                  //   image: AssetImage('assets/save2.png'),
                  //
                  // ),
                ),
                Center(
                  child: Text(
                    'Reset Pass',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 4.2 * SizeConfig.textMultiplier,
                      fontFamily: 'PoppinsRegular',
                    ),
                    textAlign: TextAlign.start,
                  ),
                ),
                Column(

                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 30.0,
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    //Phone
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 7.8 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: contact,
                          keyboardType: TextInputType.number,
                          validator: (value) => validatePhone(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.5 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Phone Number',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.call),
                            ),
                          ),

                        ),
                      ),
                    ),
                    _accept ? Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black45,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(6))
                        ),
                        height: 7.8 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: DropdownButtonHideUnderline(
                            child: DropDownSales(this.security,'Select Security Question'),
                          ),
                        ),
                      ),
                    ) :  SizedBox(),
                    SizedBox(
                      height: 7.0,
                    ),
                    _accept ? Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 7.8* SizeConfig.heightSizeMultiplier,
                        width: 54.6 * SizeConfig.heightSizeMultiplier,
                        child: TextFormField(
                          controller: answer,
                          keyboardType: TextInputType.text,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.7 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' ',
                            labelText: ' Answer',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.security),
                            ),
                          ),

                        ),
                      ),
                    ):SizedBox(),
                    // //gender
                    // Padding(
                    //   padding: EdgeInsets.fromLTRB(20,1,20,10),
                    //   child: Container(
                    //     height: 9.0 * SizeConfig.heightSizeMultiplier,
                    //     width: MediaQuery.of(context).size.width- 10,
                    //     child:  _genderRadio(_groupValue, _handleRadioValueChanged),
                    //   ),
                    // ),
                    SizedBox(
                      height: 10.0,
                    ),
                    _accept ? SizedBox(): Padding(
                      padding: EdgeInsets.fromLTRB(20,4,20,4),
                      child: Container(
                          height: 6* SizeConfig.heightSizeMultiplier,
                          width: MediaQuery.of(context).size.width-10,
                          child: RaisedButton(
                            onPressed: () {

                              if (_formKey_signup.currentState.validate()) {
                                setState(() {
                                  _accept = true;
                                  getMaritalStatus(contact.text,Btype);
                                });

                              }
                              print('Next');
                            },
                            child: Text(
                              'Next ',
                              style: TextStyle(fontSize: 2.2 * SizeConfig.textMultiplier, fontWeight: FontWeight.bold, color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                            color: Theme.of(context).primaryColor,
                          )),
                    ),
                    //NextButton
                    _accept ? Padding(
                      padding: EdgeInsets.fromLTRB(20,4,20,4),
                      child: Container(
                          height: 6* SizeConfig.heightSizeMultiplier,
                          width: MediaQuery.of(context).size.width-10,
                          child: loading ? spinKit() : RaisedButton(
                            onPressed: () {

                              if (_formKey_signup.currentState.validate()) {
                                setState(() {
                                  loading = true;
                                });
                                sendReg(contact.text,Btype,answer.text,agent);

                              }
                              print('Next');
                            },
                            child: Text(
                              'Reset PIN ',
                              style: TextStyle(fontSize: 2.2 * SizeConfig.textMultiplier, fontWeight: FontWeight.bold, color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                            color: Theme.of(context).primaryColor,
                          )),
                    ) :SizedBox(),
                    SizedBox(
                      height: 40.0,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: new Container(
        height: 50.0,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ForgotPassword()));
              },
              child: Text(
                'Have an account? Sign in',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 1.4 * SizeConfig.textMultiplier,
                    fontFamily: 'PoppinsRegular',
                    fontWeight: FontWeight.bold
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30,0,30,2),
              child: Text(
                'By signing up you indicate that you have read and agreed to the Terms of Service',
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 1.1 * SizeConfig.textMultiplier,
                  fontFamily: 'PoppinsRegular',
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

}