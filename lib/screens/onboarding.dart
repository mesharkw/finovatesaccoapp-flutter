import 'dart:async';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:travelx_v1/screens/emnam/slide_item.dart';
import 'package:travelx_v1/screens/emnam/slid_dots.dart';
import 'package:travelx_v1/screens/forgot_password.dart';

class Sliders extends StatefulWidget {
  @override
  _SlidersState createState() => _SlidersState();
}

class _SlidersState extends State<Sliders> {
  int _currentPage = 0;
  PageController _pageController = PageController(initialPage: 0);

  @override
  void initState() {
    super.initState();
    Timer.periodic(Duration(seconds: 5), (Timer timer) {
      if (_currentPage < 2) {
        _currentPage++;
      } else {
        _currentPage = 0;
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  _onPageChanged(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: <Widget>[
                PageView.builder(
                  onPageChanged: _onPageChanged,
                  controller: _pageController,
                  scrollDirection: Axis.horizontal,
                  itemCount: silderList.length,
                  itemBuilder: (context, index) {
                    return SlideItem(index);
                  },
                ),
                Stack(
                  alignment: AlignmentDirectional.topStart,
                  children: <Widget>[
                    Positioned(
                      top: MediaQuery.of(context).size.height / 2,
                      left: MediaQuery.of(context).size.width / 3,
                      child: Container(
                        margin: EdgeInsets.only(bottom: 35),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            for (int i = 0; i < silderList.length; i++)
                              if (i == _currentPage)
                                SlidDots(true)
                              else
                                SlidDots(false)
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                Align(
                    alignment: Alignment.bottomRight,
                    child: FlatButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ForgotPassword(),
                            ));
                        print('Next');
                      },
                      child: Text(
                        'Next',
                        style: TextStyle(
                            fontFamily: 'PoppinsMedium',
                            color: Theme.of(context).primaryColor),
                      ),
                    ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
