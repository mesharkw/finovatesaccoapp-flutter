import 'dart:convert';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:numeric_keyboard/numeric_keyboard.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travelx_v1/goodley/models/all_api.dart';
import 'package:travelx_v1/goodley/models/counties.dart';
import 'package:travelx_v1/goodley/network/otp_api.dart';
import 'package:travelx_v1/goodley/start.dart';
import 'package:travelx_v1/goodley/utils/common_utils.dart';
import 'package:travelx_v1/goodley/utils/spinner.dart';
import 'package:travelx_v1/goodley/widgets/toast.dart';

import '../size_config.dart';


//Sign Up Page
class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class Item {
  const Item(this.name,this.id);
  final String name;
  final String id;
}
class Company {
  int id;
  String name;

  Company(this.id, this.name);

  static List<Company> getCompanies() {
    return <Company>[
      Company(1, 'Apple'),
      Company(2, 'Google'),
      Company(3, 'Samsung'),
      Company(4, 'Sony'),
      Company(5, 'LG'),
    ];
  }
}

class _SignUpState extends State<SignUp> {
  bool loading = false;
  final _formKey_signup = GlobalKey<FormState>();
  final f = new DateFormat('yyyy-MM-dd');

  final fname = new TextEditingController();
  final lname = new TextEditingController();
  final address = new TextEditingController();
  final contact = new TextEditingController();
  final email = new TextEditingController();
  final dob = new TextEditingController();
  final imname = new TextEditingController();
  final imphone = new TextEditingController();
  final dno = new TextEditingController();
  final marital = new TextEditingController();
  final kra = new TextEditingController();
  final county = new TextEditingController();
  final source = new TextEditingController();
  final contri = new TextEditingController();
  final contrieff = new TextEditingController();
  final detail = new TextEditingController();
  final ppin = new TextEditingController();

  String business;
  String  employed;
  String  dtype;

  int _groupValue = -1;
  int typeMember;
  String gender;
  String countyID;
  String countyName;
  String _chosenValue;
  List docType = [];
  bool _show = true;
  bool _accept = true;
 List<Counties> maritals;
 List<Counties> mType;
 List<Counties> counties;
 List<Counties> sAagents;
  String Btype = '1a862df26f6943997cef90233877a4fe';
  void _handleRadioValueChanged(int value) {
    setState(() {
      this._groupValue = value;
    });
  }



  Future<void> getCounties(String btype) async {
    if (btype != null) {
      var response = await getCounty(btype);

      if (response != null || response.isNotEmpty ) {
        this.counties = response;
        setState(() {
        });
      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }
  Future<void> getSalesAgents(String btype) async {
    if (btype != null) {
      var response = await getSalesAgent(btype);

      if (response != null || response.isNotEmpty ) {
        this.sAagents = response;
        setState(() {
        });
      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }
  Future<void> getMemberTypes(String btype) async {
    if (btype != null) {
      var response = await getMemberType(btype);

      if (response != null || response.isNotEmpty ) {

        this.mType = response;

        print(this.mType.toString());
        for(int i = 0; i<this.mType.length; i++){
          if(this.mType[i].name.contains('Individual')){
            setState(() {
              this.typeMember = int.parse(this.mType[i].id);
            });
          }
        }
        setState(() {

        });
      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }
  Future<void> getMaritalStatus(String btype) async {
    if (btype != null) {
      var response = await getMarital(btype);

      if (response != null || response.isNotEmpty ) {

        this.maritals = response;
        setState(() {

        });
      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }
  String _mySelection;
  String _mySelectionM;
  String agent;
  //widget Counties
  Widget DropDown(List data, String text){
    if(data!=null)
    {
      return DropdownButton(
        items: data.map((item) {
          return new DropdownMenuItem(
            child: new Text(
              item.name,
              style: TextStyle(fontSize: 12.0),
            ),
            value: item.id,
          );
        }).toList(),
        hint: Text(
          text,
          style: TextStyle(
            color: Colors.black45,
              fontSize: 12.0
          ),),
        onChanged: (newVal) {
          setState(() {
            _mySelection = newVal;
            // customerid = newVal;
            // print('customrid:' + customerid.toString());
          });
        },
        value: _mySelection,
      );
    }
    else{
      return new Center(
        child: CircularProgressIndicator(),
      );
    }
  }

  //widget Marital
  Widget DropDownM(List data, String text){
    if(data!=null)
    {
      return DropdownButton(
        items: data.map((item) {
          return new DropdownMenuItem(
            child: new Text(
              item.name,
              style: TextStyle(fontSize: 12.0),
            ),
            value: item.id,
          );
        }).toList(),
        hint: Text(
          text,
          style: TextStyle(
            color: Colors.black45,
              fontSize: 12.0
          ),),
        onChanged: (newVal) {
          setState(() {
            _mySelectionM = newVal;
            // customerid = newVal;
            // print('customrid:' + customerid.toString());
          });
        },
        value: _mySelectionM,
      );
    }
    else{
      return new Center(
        child: CircularProgressIndicator(),
      );
    }
  }

  //widget SalesAgent
  Widget DropDownSales(List data, String text){
    if(data!=null)
    {
      return DropdownButton(
        items: data.map((item) {
          return new DropdownMenuItem(
            child: new Text(
              item.name,
              style: TextStyle(fontSize: 12.0),
            ),
            value: item.id,
          );
        }).toList(),
        hint: Text(
          text,
          style: TextStyle(
              color: Colors.black45,
              fontSize: 12.0
          ),),
        onChanged: (newVal) {
          setState(() {
            agent = newVal;
            // customerid = newVal;
            // print('customrid:' + customerid.toString());
          });
        },
        value: agent,
      );
    }
    else{
      return new Center(
        child: CircularProgressIndicator(),
      );
    }
  }

  Future<void> getDocuType(String btype) async {
    if (btype != null) {
      var response = await getDocType(btype);

      if (response != null || response.isNotEmpty ) {
        this.docType = response;
        for (var i = 0; i < this.docType.length; i++) {

          print(this.docType[i].name);
        }
        setState(() {
          this.dtype =  this.docType[1].id;
        });
      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }
  Future<void> getSales(String btype) async {
    if (btype != null) {
      var response = await getSalesAgent(btype);

      if (response != null || response.isNotEmpty ) {
        this.sAagents = response;
        for (var i = 0; i < this.docType.length; i++) {

          print(this.sAagents[i].name);
        }
        setState(() {
          this.agent =  this.sAagents[1].id;
        });
      }
      else  {

        setState(() {

        });
        print('here');
        showToast('Please check Internet connection');
      }
    }

  }

  Future<void> sendReg(String name ,address ,contact ,Btype,email,dob,dtype,dno,
      county,ppin,agent, imname, imphone) async {


    if (agent != null || agent != "") {
      AllApi response = await RegUser( name ,address ,formatPhoneNumber(contact) ,Btype,email,dob,dtype,dno,
          county,ppin,agent, typeMember, imname, imphone);
      print(response.code);
      print(response.data);
      print(response.data['error']);

      if (response != null && response.code == 200 || response != null && response.code == 201 ) {
//        await instance.fetchCitizenPost(id, token);
        print('200,201');

        showToast('Please Pay Registration Fee to Complete Process');
         setState(() {
           Navigator.pushReplacementNamed(context, '/pay');
         });

      }
      else if (response.code == 400 ) {
        print('400');

        setState(() {

          loading = false;
        });
        print('here');
        showToast(response.data['error']);
      } else if(response.code == 401){
        print('401');

        loading = false;
          showToast('Record already in existence. Phone number or\/and email already registered!');

          setState(() {
            Navigator.pushReplacementNamed(context, '/login');
          });

      }
    } else {
      showToast("Please fill in all required fields");
    }

  }

  /// Valid phone number
  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }
//Radio button (Gender)
  _genderRadio(int groupValue, handleRadioValueChanged) =>
      Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
        Text(
          'Gender',
          style: new TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal),
        ),
        Row(
          children: <Widget>[
            Radio(
                value: 0,
                groupValue: groupValue,
                onChanged: handleRadioValueChanged),
            Text(
              "Male",
              style: new TextStyle(
                fontSize: 13.0,
              ),
            ),
            Radio(
                value: 1,
                groupValue: groupValue,
                onChanged: handleRadioValueChanged),
            Text(
              "Female",
              style: new TextStyle(
                fontSize: 13.0,
              ),
            ),
          ],
        )
      ]);


  @override
  void initState()  {
    // TODO: implement initState
    super.initState();
    getCounties(Btype);
    getMaritalStatus(Btype);
    getDocuType(Btype);
    getSalesAgents(Btype);
    getMemberTypes(Btype);
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Form(
            key: _formKey_signup,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //Logo
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,

                  children: [
                    SizedBox.fromSize(),
                    Padding(
                      padding: const EdgeInsets.only(top:50,right: 25.0),
                      child: GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ForgotPassword()));
                        },
                        child: Text(
                          'Sign In',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 2.6 * SizeConfig.textMultiplier,
                              fontFamily: 'PoppinsRegular',
                              fontWeight: FontWeight.bold
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15,10,8,8),
                  child:
                  Center(
                    child: Image(
                      height: 70,
                      width:  MediaQuery.of(context).size.width/2,
                      image: AssetImage('assets/fsac.jpeg'),
                      fit: BoxFit.cover,
                    ),
                  ),
                  // Image(
                  //   height: 80,
                  //   width: 80,
                  //   image: AssetImage('assets/save2.png'),
                  //
                  // ),
                ),
                Column(

                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 30.0,
                    ),
                    //Email/phone
                    Row(
                      children: [
                        //full Names
                        Padding(
                          padding: EdgeInsets.fromLTRB(18,10,2,10),
                          child: Container(
                            height: 7.8* SizeConfig.heightSizeMultiplier,
                            width: MediaQuery.of(context).size.width/2.2 -10,
                            child: TextFormField(
                              controller: fname,
                              validator: (value) => validatorEmpty(value),
                              style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontSize: 1.5 * SizeConfig.textMultiplier,
                                fontFamily: 'PoppinsRegular',
                              ),
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context).accentColor)),
                                hintText: '  ',
                                labelText: ' First Name',

                                prefixIcon: Padding(
                                  padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                                  child: Icon(Icons.bookmark),
                                ),
                              ),

                            ),
                          ),
                        ),

                        Padding(
                          padding: EdgeInsets.fromLTRB(18,10,2,10),
                          child: Container(
                            height: 7.8* SizeConfig.heightSizeMultiplier,
                            width: MediaQuery.of(context).size.width/2.2 -10,
                            child: TextFormField(
                              controller: lname,
                              validator: (value) => validatorEmpty(value),
                              style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontSize: 1.5 * SizeConfig.textMultiplier,
                                fontFamily: 'PoppinsRegular',
                              ),
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context).accentColor)),
                                hintText: '  ',
                                labelText: ' Last Name',

                                prefixIcon: Padding(
                                  padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                                  child: Icon(Icons.bookmark),
                                ),
                              ),

                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: 7.0,
                    ),
                    //DOB
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 7.8* SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width - 10,
                        child: TextFormField(
                          controller: dob,
                          validator: (value) => validatorEmpty(value),
                          onTap: () async{
                            DateTime date = DateTime(1900);
                            FocusScope.of(context).requestFocus(new FocusNode());

                            date = await showDatePicker(
                                context: context,
                                initialDate:DateTime.now(),
                                firstDate:DateTime(1900),
                                lastDate: DateTime(2100));

                            dob.text = f.format(date);},
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.5* SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'D.O.B',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.calendar_today),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    //national ID
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 7.8* SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: dno,
                          keyboardType: TextInputType.number,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.5 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'ID Number',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.account_box_outlined),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    //Email
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 7.8 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: email,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.5 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Email',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.email),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    //Phone
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 7.8 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: contact,
                          keyboardType: TextInputType.number,
                          validator: (value) => validatePhone(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.5 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Phone Number',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.call),
                            ),
                          ),

                        ),
                      ),
                    ),
                    // SizedBox(
                    //   height: 7.0,
                    // ),
                    // //Area of Residence
                    // Padding(
                    //   padding: EdgeInsets.fromLTRB(20,10,20,10),
                    //   child: Container(
                    //     height: 5.8 * SizeConfig.heightSizeMultiplier,
                    //     width: MediaQuery.of(context).size.width- 10,
                    //     child: TextFormField(
                    //       controller: kra,
                    //       validator: (value) => validatorEmpty(value),
                    //       style: TextStyle(
                    //         color: Theme.of(context).accentColor,
                    //         fontSize: 1.5 * SizeConfig.textMultiplier,
                    //         fontFamily: 'PoppinsRegular',
                    //       ),
                    //       decoration: InputDecoration(
                    //         border: OutlineInputBorder(
                    //             borderSide: BorderSide(
                    //                 color: Theme.of(context).accentColor)),
                    //         hintText: ' .... ',
                    //         labelText: 'KRA PIN',
                    //
                    //         prefixIcon: Padding(
                    //           padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                    //           child: Icon(Icons.credit_card),
                    //         ),
                    //       ),
                    //
                    //     ),
                    //   ),
                    // ),
                    SizedBox(
                      height: 7.0,
                    ),
                    //Area of Residence
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 7.8 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: address,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.5 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Address',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.location_on),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    //Area of Residence
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black45,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(6))
                        ),
                        height: 7.8 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: DropdownButtonHideUnderline(
                            child: DropDown(this.counties, 'Select County'),
                          ),
                        ),
                      ),
                    ),
                    // SizedBox(
                    //   height: 7.0,
                    // ),
                    // Padding(
                    //   padding: EdgeInsets.fromLTRB(20,10,20,10),
                    //   child: Container(
                    //     decoration: BoxDecoration(
                    //         border: Border.all(
                    //           color: Colors.black45,
                    //         ),
                    //         borderRadius: BorderRadius.all(Radius.circular(6))
                    //     ),
                    //     height: 5.8 * SizeConfig.heightSizeMultiplier,
                    //     width: MediaQuery.of(context).size.width- 10,
                    //     child: Padding(
                    //       padding: const EdgeInsets.all(10.0),
                    //       child: DropdownButtonHideUnderline(
                    //         child: DropDownM(this.maritals,'Select Marital Status'),
                    //   ),
                    //     ),
                    //   ),
                    // ),
                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black45,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(6))
                        ),
                        height: 5.8 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              items: <String>['Yes', 'No'].map((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: new Text(value,  style: TextStyle(fontSize: 12.0),),

                                );
                              }).toList(),
                              hint: Text(
                                'Are You employed?',
                                style: TextStyle(
                                  color: Colors.black45,
                                    fontSize: 12.0
                                ),),
                              onChanged: (newVal) {
                                setState(() {
                                  this.employed = newVal;
                                  // customerid = newVal;
                                  // print('customrid:' + customerid.toString());
                                });
                              },
                              value: this.employed,
                            ),
                          ),
                        ),
                      ),
                    ),
                    // SizedBox(
                    //   height: 7.0,
                    // ),
                    // Padding(
                    //   padding: EdgeInsets.fromLTRB(20,10,20,10),
                    //   child: Container(
                    //     decoration: BoxDecoration(
                    //         border: Border.all(
                    //           color: Colors.black45,
                    //         ),
                    //         borderRadius: BorderRadius.all(Radius.circular(6))
                    //     ),
                    //     height: 5.8 * SizeConfig.heightSizeMultiplier,
                    //     width: MediaQuery.of(context).size.width- 10,
                    //     child: Padding(
                    //       padding: const EdgeInsets.all(10.0),
                    //       child: DropdownButtonHideUnderline(
                    //         child: DropdownButton<String>(
                    //           items: <String>['Yes', 'No'].map((String value) {
                    //             return DropdownMenuItem<String>(
                    //               value: value,
                    //               child: new Text(value,  style: TextStyle(fontSize: 12.0),),
                    //
                    //             );
                    //           }).toList(),
                    //           hint: Text(
                    //             'Do you Have  Business?',
                    //             style: TextStyle(
                    //                 color: Colors.black45,
                    //                 fontSize: 12.0
                    //             ),),
                    //           onChanged: (newVal) {
                    //             setState(() {
                    //               this.business = newVal;
                    //               // customerid = newVal;
                    //               // print('customrid:' + customerid.toString());
                    //             });
                    //           },
                    //           value: this.business,
                    //         ),
                    //       ),
                    //     ),
                    //   ),
                    // ),
                    // SizedBox(
                    //   height: 7.0,
                    // ),
                    //Area of Residence
                    // Padding(
                    //   padding: EdgeInsets.fromLTRB(20,10,20,10),
                    //   child: Container(
                    //     height: 5.8 * SizeConfig.heightSizeMultiplier,
                    //     width: MediaQuery.of(context).size.width- 10,
                    //     child: TextFormField(
                    //       controller: source,
                    //       validator: (value) => validatorEmpty(value),
                    //       style: TextStyle(
                    //         color: Theme.of(context).accentColor,
                    //         fontSize: 1.5 * SizeConfig.textMultiplier,
                    //         fontFamily: 'PoppinsRegular',
                    //       ),
                    //       decoration: InputDecoration(
                    //         border: OutlineInputBorder(
                    //             borderSide: BorderSide(
                    //                 color: Theme.of(context).accentColor)),
                    //         hintText: ' .... ',
                    //         labelText: 'Source of Income',
                    //
                    //         prefixIcon: Padding(
                    //           padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                    //           child: Icon(Icons.money),
                    //         ),
                    //       ),
                    //
                    //     ),
                    //   ),
                    // ),
                    // SizedBox(
                    //   height: 7.0,
                    // ),
                    //Area of Residence
                    // Padding(
                    //   padding: EdgeInsets.fromLTRB(20,10,20,10),
                    //   child: Container(
                    //     height: 5.8 * SizeConfig.heightSizeMultiplier,
                    //     width: MediaQuery.of(context).size.width- 10,
                    //     child: TextFormField(
                    //       controller: contri,
                    //       validator: (value) => validatorEmpty(value),
                    //       style: TextStyle(
                    //         color: Theme.of(context).accentColor,
                    //         fontSize: 1.5 * SizeConfig.textMultiplier,
                    //         fontFamily: 'PoppinsRegular',
                    //       ),
                    //       decoration: InputDecoration(
                    //         border: OutlineInputBorder(
                    //             borderSide: BorderSide(
                    //                 color: Theme.of(context).accentColor)),
                    //         hintText: ' .... ',
                    //         labelText: 'Contribution Per Month',
                    //
                    //         prefixIcon: Padding(
                    //           padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                    //           child: Icon(Icons.money),
                    //         ),
                    //       ),
                    //
                    //     ),
                    //   ),
                    // ),
                    // Padding(
                    //   padding: EdgeInsets.fromLTRB(20,10,20,10),
                    //   child: Container(
                    //     height: 5.8 * SizeConfig.heightSizeMultiplier,
                    //     width: MediaQuery.of(context).size.width- 10,
                    //     child: TextFormField(
                    //       controller: contrieff,
                    //       validator: (value) => validatorEmpty(value),
                    //       onTap: () async{
                    //         DateTime date = DateTime(1900);
                    //         FocusScope.of(context).requestFocus(new FocusNode());
                    //
                    //         date = await showDatePicker(
                    //             context: context,
                    //             initialDate:DateTime.now(),
                    //             firstDate:DateTime(1900),
                    //             lastDate: DateTime(2100));
                    //
                    //         contrieff.text = f.format(date);},
                    //       style: TextStyle(
                    //         color: Theme.of(context).accentColor,
                    //         fontSize: 1.5* SizeConfig.textMultiplier,
                    //         fontFamily: 'PoppinsRegular',
                    //       ),
                    //       decoration: InputDecoration(
                    //         border: OutlineInputBorder(
                    //             borderSide: BorderSide(
                    //                 color: Theme.of(context).accentColor)),
                    //         hintText: ' .... ',
                    //         labelText: 'Contribution Date',
                    //
                    //         prefixIcon: Padding(
                    //           padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                    //           child: Icon(Icons.calendar_today),
                    //         ),
                    //       ),
                    //
                    //     ),
                    //   ),
                    // ),
                    // SizedBox(
                    //   height: 7.0,
                    // ),
                    // //Area of Residence
                    // Padding(
                    //   padding: EdgeInsets.fromLTRB(20,10,20,10),
                    //   child: Container(
                    //     height: 5.8 * SizeConfig.heightSizeMultiplier,
                    //     width: MediaQuery.of(context).size.width- 10,
                    //     child: TextFormField(
                    //       controller: detail,
                    //       validator: (value) => validatorEmpty(value),
                    //       style: TextStyle(
                    //         color: Theme.of(context).accentColor,
                    //         fontSize: 1.5 * SizeConfig.textMultiplier,
                    //         fontFamily: 'PoppinsRegular',
                    //       ),
                    //       decoration: InputDecoration(
                    //         border: OutlineInputBorder(
                    //             borderSide: BorderSide(
                    //                 color: Theme.of(context).accentColor)),
                    //         hintText: ' .... ',
                    //         labelText: 'Any Other detail or comment',
                    //
                    //         prefixIcon: Padding(
                    //           padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                    //           child: Icon(Icons.info),
                    //         ),
                    //       ),
                    //
                    //     ),
                    //   ),
                    // ),
                    // SizedBox(
                    //   height: 7.0,
                    // ),
                    // //Area of Residence
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 5.8 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: imname,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.5 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Next Of Kin Name',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.person),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    //Area of Residence
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 5.8 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: TextFormField(
                          controller: imphone,
                          validator: (value) => validatorEmpty(value),
                          keyboardType: TextInputType.phone,
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.5 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: 'Next Of kin Phone Number',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.phone),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black45,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(6))
                        ),
                        height: 7.8 * SizeConfig.heightSizeMultiplier,
                        width: MediaQuery.of(context).size.width- 10,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: DropdownButtonHideUnderline(
                            child: DropDownSales(this.sAagents,'Select Referral'),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,20,10),
                      child: Container(
                        height: 7.8* SizeConfig.heightSizeMultiplier,
                        width: 54.6 * SizeConfig.heightSizeMultiplier,
                        child: TextFormField(
                          controller: ppin,
                          keyboardType: TextInputType.phone,
                          obscureText: true,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.7 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' ******* ',
                            labelText: ' Prefered PIN',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.security),
                            ),
                          ),

                        ),
                      ),
                    ),
                    // //gender
                    // Padding(
                    //   padding: EdgeInsets.fromLTRB(20,1,20,10),
                    //   child: Container(
                    //     height: 9.0 * SizeConfig.heightSizeMultiplier,
                    //     width: MediaQuery.of(context).size.width- 10,
                    //     child:  _genderRadio(_groupValue, _handleRadioValueChanged),
                    //   ),
                    // ),
                    SizedBox(
                      height: 10.0,
                    ),
                    //NextButton
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,4,20,4),
                      child: Container(
                          height: 6* SizeConfig.heightSizeMultiplier,
                          width: MediaQuery.of(context).size.width-10,
                          child: loading ? spinKit() : RaisedButton(
                            onPressed: () {

                              if (_formKey_signup.currentState.validate()) {
                                setState(() {
                                  loading = true;
                                });
                                sendReg(fname.text+' '+lname.text,address.text,contact.text,Btype,email.text,dob.text,dtype,dno.text,
                                    _mySelection,ppin.text,agent,imname.text, imphone.text);

                              }
                              print('Next');
                            },
                            child: Text(
                              'SIGN UP ',
                              style: TextStyle(fontSize: 2.2 * SizeConfig.textMultiplier, fontWeight: FontWeight.bold, color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                            color: Theme.of(context).primaryColor,
                          )),
                    ),
                    SizedBox(
                      height: 40.0,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: new Container(
        height: 50.0,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ForgotPassword()));
              },
              child: Text(
                'Have an account? Sign in',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 1.4 * SizeConfig.textMultiplier,
                    fontFamily: 'PoppinsRegular',
                    fontWeight: FontWeight.bold
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30,0,30,2),
              child: Text(
                'By signing up you indicate that you have read and agreed to the Terms of Service',
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 1.1 * SizeConfig.textMultiplier,
                  fontFamily: 'PoppinsRegular',
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  }



//Sign In Page
class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}


class _ForgotPasswordState extends State<ForgotPassword> {
  String email;
  bool loading = false;
  final _formKey_login = GlobalKey<FormState>();
  final username = new TextEditingController();
  final pass = new TextEditingController();
  /// Valid phone number
  ///
  ///

  bool _passwordVisible = false;
  @override
  void initState()  {
    // TODO: implement initState
    super.initState();
    getSharedPref();
  }

  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }
  void getSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String phone = prefs.getString('phone');
    String id_number = prefs.getString('id_number');
    bool reg = prefs.getBool('registered');

    // if (reg) {
    //   Navigator.pushReplacementNamed(context, '/otp');
    // } else{
    //
    // }
  }
  void login(String phone, String pass, btype) async {
    if (phone != null) {
      print(phone + pass);
      AllApi response = await loginUser(formatPhoneNumber(phone), pass, btype);
      print('ewsss>' + response.code.toString());

      if (response != null && response.code == 200 || response.code == 201 ) {
//        await instance.fetchCitizenPost(id, token);


        setState(() {
          showToast('User Verified');
          loading = false;
          Navigator.pushReplacementNamed(context, '/otp');
        });
      } else if(response.code == 401) {
        setState(() {

          loading = false;
        });
        print('Login Res'+response.data.toString());
        if(response.data.contains('Failed login!')){
          showToast('Incorrect Phone/Pin cobination');
        } else if(response.data.contains('Confirm Payment of Registration!')) {
          showToast('Please Confirm payment');
          AllApi responses = await confirmPay(formatPhoneNumber(phone),'stk_push', '500',Btype);
          print('COnfirm Res'+responses.data.toString());
          print('COnfirm ResCode'+responses.code.toString());

          if (responses != null && responses.code == 200 || responses.code == 201 ) {
//        await instance.fetchCitizenPost(id, token);
            showToast('Payment Confirmed , Login to proceed');

            Navigator.pushReplacementNamed(context, '/home');


          } else {
            showToast('Payment Not Confirmed , Please Pay Registration Fee');

            SharedPreferences prefs = await SharedPreferences.getInstance();
            prefs.setString('phone', phone)     ;
            prefs.setString('accountNo', phone);
            setState(() {
              showToast('Confirming payment');
              loading = false;
              Navigator.pushReplacementNamed(context, '/pay');
              // Navigator.pushReplacementNamed(context, '/home');

            });
          }
        }


      }else {
        setState(() {

          loading = false;
        });
        showToast('Unable to verify User. Contact Admin');
      }
    }
  }

  void sendOtps(String phone) async {
    if (phone != null) {
      AllApi response = await sendOTP(formatPhoneNumber(phone));
      // print(response.code);

      if (response != null && response.code != 400 ) {
//        await instance.fetchCitizenPost(id, token);

        setState(() {
          loading = false;
          showToast('Kindly Enter  OTP sent');
          Navigator.pushReplacementNamed(context, '/otp');
        });
      }
      else {
        setState(() {

          loading = false;
        });
        showToast('Unable to verify User. Contact Admin');
      }
    }
  }
  String Btype = '1a862df26f6943997cef90233877a4fe';



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Form(
            key: _formKey_login,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //Logo
                SizedBox(height: 30,),
                Padding(
                  padding: EdgeInsets.only(top: 1 * SizeConfig.heightSizeMultiplier),
                  child: Center(
                    child: Container(
                      width: MediaQuery.of(context).size.width/2,
                      height:80,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: AssetImage('assets/fsac.jpeg'),
                        ),
                      ),
                    ),
                  ),
                ),
                Column(

                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15,20,20,9),
                      child: Column(

                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [

                          Center(
                            child: Text(
                              'Empower &',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontSize: 3.2 * SizeConfig.textMultiplier,
                                fontFamily: 'PoppinsRegular',
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Text(
                            'Connect',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 3.2 * SizeConfig.textMultiplier,
                              fontFamily: 'PoppinsRegular',
                            ),
                            textAlign: TextAlign.center,
                          ),

                        ],
                      ),
                    ),

                    SizedBox(
                      height: 25.0,
                    ),
                    //Email/phone
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,50,10),
                      child: Container(
                        height: 6.5 * SizeConfig.heightSizeMultiplier,
                        width: 54.6 * SizeConfig.heightSizeMultiplier,
                        child: TextFormField(
                          controller: username,
                          keyboardType: TextInputType.phone,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.7 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' .... ',
                            labelText: ' Phone Number',

                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.person),
                            ),
                          ),

                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,10,50,10),
                      child: Container(
                        height: 6.5* SizeConfig.heightSizeMultiplier,
                        width: 54.6 * SizeConfig.heightSizeMultiplier,
                        child: TextFormField(
                          controller: pass,
                          keyboardType: TextInputType.text,
                          obscureText: !_passwordVisible,
                          validator: (value) => validatorEmpty(value),
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 1.7 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).accentColor)),
                            hintText: ' ******* ',
                            labelText: ' Pin',
                            suffixIcon: IconButton(
                              icon:Icon(
                                _passwordVisible? Icons.visibility
                                    : Icons.visibility_off,
                                color: Theme.of(context).primaryColorDark,
                              ),
                              onPressed: () {
                                // Update the state i.e. toogle the state of passwordVisible variable
                                setState(() {
                                  _passwordVisible = !_passwordVisible;
                                });
                              },

                            ),
                            prefixIcon: Padding(
                              padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                              child: Icon(Icons.security),
                            ),
                          ),
                        ),
                      ),
                    ),

                    SizedBox(
                      height: 7.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 48.0),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () {
                            Navigator.pushReplacementNamed(context, '/reset');
                          },
                          child: Text(
                            'Forgot Pin ?',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 1.8 * SizeConfig.textMultiplier,
                                fontFamily: 'PoppinsRegular',
                                fontWeight: FontWeight.bold
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    //NextButton
                    Center(
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(20,4,50,4),
                        child: Container(
                            height: 5.1 * SizeConfig.heightSizeMultiplier,
                            width: 25.6 * SizeConfig.heightSizeMultiplier,
                            child: loading ? spinKit() :RaisedButton(
                              onPressed: () {
                                if (_formKey_login.currentState.validate()) {
                                  setState(() {
                                    loading = true;
                                  });
                                  login(username.text, pass.text, Btype);
                                  // sendOtps(phone.text);
                                }
                                print('Next');
                              },
                              child: Row(
                                children: [
                                  Text(
                                    'SIGN IN ',
                                    style: TextStyle(fontSize: 2.0 * SizeConfig.textMultiplier, fontWeight: FontWeight.bold, color: Colors.white),
                                  ),

                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(15,0,0,0),
                                    child: Icon(Icons.arrow_forward_rounded, size: 30, color: Colors.white,),
                                  ),
                                ],
                              ),
                              color: Theme.of(context).primaryColor,
                            )),
                      ),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,

                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top:20,right: 25.0),
                          child: GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () {
                              Navigator.pushReplacementNamed(context, '/signup');
                            },
                            child: Text(
                              'Sign Up',
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 2.9 * SizeConfig.textMultiplier,
                                  fontFamily: 'PoppinsRegular',
                                  fontWeight: FontWeight.bold
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: new Container(
        height: 70.0,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                Navigator.pushReplacementNamed(context, '/signup');
              },
              child: Text(
                'Don`t have an account? Sign Up',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 1.6 * SizeConfig.textMultiplier,
                  fontFamily: 'PoppinsRegular',
                  fontWeight: FontWeight.bold
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30,0,30,2),
              child: Text(
                'By signing up you indicate that you have read and agreed to the Terms of Service',
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 1.4 * SizeConfig.textMultiplier,
                  fontFamily: 'PoppinsRegular',
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}




//OTP Page
class ForgotPasswordOtp extends StatefulWidget {
  @override
  _ForgotPasswordOtpState createState() => _ForgotPasswordOtpState();
}

class _ForgotPasswordOtpState extends State<ForgotPasswordOtp> {
  String text = '';
  final username = new TextEditingController();
  final otp = new TextEditingController();

  bool loading = false;
  String Btype = '1a862df26f6943997cef90233877a4fe';

  @override
  void initState()  {
    // TODO: implement initState
    super.initState();
    getSharedPref();
  }
  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }
  void getSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String phone = prefs.getString('phone');
    print('>>>Phone'+phone);

    setState(() {
      username.text = phone;
    });
  }


  void regPayment(String phone) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    String id_number = prefs.getString('id_number');

    AllApi response = await subscribe(phone);
    print(response.code);

    if (response != null && response.code != 400 ) {
//        await instance.fetchCitizenPost(id, token);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool('registered', true);
      prefs.setBool('logged_in', true);
      loading = false;
      setState(() {
        showToast('Wait for Mpesa stk Push to pay');
        Navigator.pushReplacementNamed(context, '/login');
      });
    }
    else {
      setState(() {

        loading = false;
      });
      showToast('Unable to verify User. Contact Admin');
    }

  }

  void verifyOTPs( phone, otp, btype) async {
      AllApi response = await verifyOTPURL(formatPhoneNumber(phone), otp, btype);
      print('reess'+ response.code.toString());

      if (response != null && response.code != 400 && response.code != 401 ) {
//        await instance.fetchCitizenPost(id, token);

        loading = false;
        setState(() {
          showToast('User Validated');
          Navigator.pushReplacementNamed(context, '/home');
        });
      }
      else {
        setState(() {

          loading = false;
        });
        showToast('Unable to verify User. Contact Admin');
      }

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => ForgotPassword()));
          },
          icon: Icon(
            Icons.chevron_left,
            color: Theme.of(context).primaryColor,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //Logo
            Padding(
              padding: const EdgeInsets.fromLTRB(8,1,20,9),
              child: Column(

                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(1,10,8,8),
                    child:
                    Image(
                      height: 50,
                      width: 150,
                      image: AssetImage('assets/fsac.jpeg'),
                      fit: BoxFit.cover,
                    ),
                  ),
                  Text(
                    'Empower &',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 4.2 * SizeConfig.textMultiplier,
                      fontFamily: 'PoppinsRegular',
                    ),
                    textAlign: TextAlign.start,
                  ),
                  Text(
                    'Connect',
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 4.2 * SizeConfig.textMultiplier,
                      fontFamily: 'PoppinsRegular',
                    ),
                    textAlign: TextAlign.start,
                  ),

                SizedBox(
                  height: 10.0,
                ),
                // Text(
                //   'Complete Registration by paying a fee of Ksh. 500/=',
                //   style: TextStyle(
                //     color: Theme.of(context).accentColor,
                //     fontSize: 2.28 * SizeConfig.textMultiplier,
                //     fontFamily: 'PoppinsRegular',
                //   ),
                //   textAlign: TextAlign.start,
                // ),
                SizedBox(
                  height: 10.0,
                ),
                //Email/phone
                  Padding(
                    padding: EdgeInsets.fromLTRB(0,10,20,10),
                    child: Container(
                      height: 7 * SizeConfig.heightSizeMultiplier,
                      width: MediaQuery.of(context).size.width- 10,
                      child: TextFormField(
                        controller: username,
                        keyboardType: TextInputType.phone,
                        validator: (value) => validatePhone(value),
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontSize: 1.8 * SizeConfig.textMultiplier,
                          fontFamily: 'PoppinsRegular',
                        ),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context).accentColor)),
                          hintText: ' .... ',
                          labelText: 'Phone Number',

                          prefixIcon: Padding(
                            padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                            child: Icon(Icons.call),
                          ),
                        ),

                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  //Email/phone
                  Padding(
                    padding: EdgeInsets.fromLTRB(0,10,20,10),
                    child: Container(
                      height: 7 * SizeConfig.heightSizeMultiplier,
                      width: MediaQuery.of(context).size.width- 10,
                      child: TextFormField(
                        controller: otp,
                        keyboardType: TextInputType.number,
                        validator: (value) => validatePhone(value),
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontSize: 1.8 * SizeConfig.textMultiplier,
                          fontFamily: 'PoppinsRegular',
                        ),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context).accentColor)),
                          hintText: ' .... ',
                          labelText: 'OTP CODE',

                          prefixIcon: Padding(
                            padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                            child: Icon(Icons.sms),
                          ),
                        ),

                      ),
                    ),
                  ),
                SizedBox(
                  height: 30.0,
                ),
                //NextButton
                Container(
                    height: 6.1 * SizeConfig.heightSizeMultiplier,
                    width: 25.6 * SizeConfig.heightSizeMultiplier,
                    child: loading ? spinKit() :RaisedButton(
                      onPressed: () {
                        setState(() {

                          loading = true;
                        });
                        verifyOTPs(username.text, otp.text, Btype);


                        print('Next');
                      },
                      child: Row(
                        children: [
                          Text(
                            'VERIFY ',
                            style: TextStyle(fontSize: 2.6 * SizeConfig.textMultiplier, fontWeight: FontWeight.bold, color: Colors.white),
                          ),

                          Padding(
                            padding: const EdgeInsets.fromLTRB(15,0,0,0),
                            child: Icon(Icons.arrow_forward_rounded, size: 30, color: Colors.white,),
                          ),
                        ],
                      ),
                      color: Theme.of(context).primaryColor,
                    ),


                ),
                  SizedBox(
                    height: 20.0,
                  ),


              ],
            )
      )
    ]
        ),
      ),
      bottomNavigationBar: new Container(
        height: 100.0,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30,0,30,10),
              child: Text(
                'By signing up you indicate that you have read and agreed to the Terms of Service',
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 1.7 * SizeConfig.textMultiplier,
                  fontFamily: 'PoppinsRegular',
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}


//Pay Page
class PayPage extends StatefulWidget {
  @override
  _PayPageState createState() => _PayPageState();
}

class _PayPageState extends State<PayPage>  {
  String text = '';
  final username = new TextEditingController();
  final otp = new TextEditingController();

  bool loading = false;
  bool paid = false;
  String Btype = '1a862df26f6943997cef90233877a4fe';
  String accountNo;

  @override
  void initState()  {
    // TODO: implement initState
    super.initState();
    getSharedPref();
  }
  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }
  void getSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String phone = prefs.getString('contact');
    String CheckoutRequestID = prefs.getString('CheckoutRequestID');
    accountNo = prefs.getString('accountNo');
    if(CheckoutRequestID != null){

      this.checkPayment(CheckoutRequestID);
    } else{
      setState(() {
        paid = true;
      });
    }
    setState(() {
      username.text = phone;
    });
  }
  void checkPayment(String CheckoutRequestID) async {

    AllApi response = await checkPay(CheckoutRequestID);
    print(response.code);
    print(response.data);
    print(response.data['data']['CheckoutRequestID']);

    if (response != null && response.code == 200 || response != null && response.code == 201 ) {

      if(response.data['data']['mpesaReceiptNumber'] == null){
        setState(() {
          paid = true;
        });
      } else {
        setState(() {

          Navigator.pushReplacementNamed(context, '/confirmPay');
          paid = false;
        });
      }

    }
    else {
      setState(() {
        paid = true;
        Navigator.pushReplacementNamed(context, '/confirmPay');
        loading = false;
      });
      showToast('Unable to verify User. Contact Admin');
    }

  }

  void regPayment(String phone) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    String id_number = prefs.getString('id_number');

    AllApi response = await pay(formatPhoneNumber(phone),accountNo);
    print(response.code);
    print(response.data['CheckoutRequestID']);

    if (response != null && response.code == 200 || response != null && response.code == 201 ) {
//        await instance.fetchCitizenPost(id, token);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool('registered', true);
      prefs.setString('CheckoutRequestID', response.data['CheckoutRequestID']);
      loading = false;
      setState(() {
        showToast('Wait for Mpesa stk Push to pay');
        Navigator.pushReplacementNamed(context, '/confirmPay');
      });
    }
    else {
      setState(() {

        loading = false;
      });
      showToast('Unable to verify User. Contact Admin');
    }

  }

  void verifyOTPs( phone, otp, btype) async {
    AllApi response = await verifyOTPURL(formatPhoneNumber(phone), otp, btype);
    print('reess'+ response.code.toString());

    if (response != null && response.code != 400 && response.code != 401 ) {
//        await instance.fetchCitizenPost(id, token);

      loading = false;
      setState(() {
        showToast('User Validated');
        Navigator.pushReplacementNamed(context, '/home');
      });
    }
    else {
      setState(() {

        loading = false;
      });
      showToast('Unable to verify User. Contact Admin');
    }

  }


  @override
  Widget build(BuildContext context) {
    return paid ? Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => ForgotPassword()));
          },
          icon: Icon(
            Icons.chevron_left,
            color: Theme.of(context).primaryColor,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Logo
              Padding(
                  padding: const EdgeInsets.fromLTRB(8,1,20,9),
                  child: Column(

                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(1,10,8,8),
                        child:
                        Image(
                          height: 50,
                          width: 150,
                          image: AssetImage('assets/fsac.jpeg'),
                          fit: BoxFit.cover,
                        ),
                      ),
                      Text(
                        'Empower &',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 4.2 * SizeConfig.textMultiplier,
                          fontFamily: 'PoppinsRegular',
                        ),
                        textAlign: TextAlign.start,
                      ),
                      Text(
                        'Connect',
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 4.2 * SizeConfig.textMultiplier,
                          fontFamily: 'PoppinsRegular',
                        ),
                        textAlign: TextAlign.start,
                      ),

                      SizedBox(
                        height: 10.0,
                      ),
                      // Text(
                      //   'Complete Registration by paying a fee of Ksh. 500/=',
                      //   style: TextStyle(
                      //     color: Theme.of(context).accentColor,
                      //     fontSize: 2.28 * SizeConfig.textMultiplier,
                      //     fontFamily: 'PoppinsRegular',
                      //   ),
                      //   textAlign: TextAlign.start,
                      // ),
                      SizedBox(
                        height: 10.0,
                      ),
                      //Email/phone
                      Padding(
                        padding: EdgeInsets.fromLTRB(20,10,20,10),
                        child: Container(
                          height: 7 * SizeConfig.heightSizeMultiplier,
                          width: MediaQuery.of(context).size.width- 10,
                          child: TextFormField(
                            controller: username,
                            keyboardType: TextInputType.phone,
                            validator: (value) => validatePhone(value),
                            style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 1.8 * SizeConfig.textMultiplier,
                              fontFamily: 'PoppinsRegular',
                            ),
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Theme.of(context).accentColor)),
                              hintText: ' .... ',
                              labelText: 'Phone Number',

                              prefixIcon: Padding(
                                padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                                child: Icon(Icons.call),
                              ),
                            ),

                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      //NextButton
                      Container(
                        height: 6.1 * SizeConfig.heightSizeMultiplier,
                        width: 25.6 * SizeConfig.heightSizeMultiplier,
                        child: loading ? spinKit() :RaisedButton(
                          onPressed: () {
                            setState(() {

                              loading = true;
                            });
                            regPayment(username.text);


                            print('Next');
                          },
                          child: Row(
                            children: [
                              Text(
                                'Pay Registration Fee (500/-) ',
                                style: TextStyle(fontSize: 2.6 * SizeConfig.textMultiplier, fontWeight: FontWeight.bold, color: Colors.white),
                              ),

                              Padding(
                                padding: const EdgeInsets.fromLTRB(15,0,0,0),
                                child: Icon(Icons.arrow_forward_rounded, size: 30, color: Colors.white,),
                              ),
                            ],
                          ),
                          color: Theme.of(context).primaryColor,
                        ),


                      ),
                      SizedBox(
                        height: 20.0,
                      ),


                    ],
                  )
              )
            ]
        ),
      ),
      bottomNavigationBar: new Container(
        height: 50.0,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30,0,30,10),
              child: Text(
                'By signing up you indicate that you have read and agreed to the Terms of Service',
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 1.3 * SizeConfig.textMultiplier,
                  fontFamily: 'PoppinsRegular',
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    ): Container( color:Colors.white,child: Center(child: CircularProgressIndicator()));
  }
}

//Confirm Pay Page
class ConfirmPayPage extends StatefulWidget {
  @override
  _ConfirmPayPageState createState() => _ConfirmPayPageState();
}

class _ConfirmPayPageState extends State<ConfirmPayPage>  {
  String text = '';
  final username = new TextEditingController();
  final otp = new TextEditingController();

  bool loading = false;
  String Btype = '1a862df26f6943997cef90233877a4fe';
  String accountNo;
  String phone;

  @override
  void initState()  {
    // TODO: implement initState
    super.initState();
    getSharedPref();
  }
  static String formatPhoneNumber(String number) {
    String phoneNumber = number.replaceAll(" ", "");

    if (phoneNumber.startsWith("+")) phoneNumber = phoneNumber.substring(1);

    if (phoneNumber.startsWith("0"))
      phoneNumber = phoneNumber.replaceFirst("0", "254");

    if (phoneNumber.startsWith("7") || phoneNumber.startsWith("1"))
      phoneNumber = "254$phoneNumber";

    return phoneNumber;
  }
  void getSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    phone = prefs.getString('contact');
    accountNo = prefs.getString('accountNo');
    print('>>>Phone'+phone);

    setState(() {
      username.text = phone;
    });
  }


  void regPayment(String code) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    String id_number = prefs.getString('id_number');

    AllApi response = await confirmPay(formatPhoneNumber(phone),'stk_push', '500',Btype);
    print(response.code);

    if (response != null && response.code == 200 || response != null && response.code == 201) {
//        await instance.fetchCitizenPost(id, token);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool('registered', false);
      loading = false;
      setState(() {
        showToast('Confirmed Payment');
        Navigator.pushReplacementNamed(context, '/login');
      });
    }
    else {
      showToast('Veryfying.....Login and Proceed');
      Navigator.pushReplacementNamed(context, '/login');
      setState(() {

        loading = false;
      });
      // showToast('Unable to verify Payment, Check gain');
    }

  }

  void verifyOTPs( phone, otp, btype) async {
    AllApi response = await verifyOTPURL(formatPhoneNumber(phone), otp, btype);
    print('reess'+ response.code.toString());

    if (response != null && response.code != 400 && response.code != 401 ) {
//        await instance.fetchCitizenPost(id, token);

      loading = false;
      setState(() {
        showToast('User Validated');
        Navigator.pushReplacementNamed(context, '/home');
      });
    }
    else {
      setState(() {

        loading = false;
      });
      showToast('Unable to verify User. Contact Admin');
    }

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => ForgotPassword()));
          },
          icon: Icon(
            Icons.chevron_left,
            color: Theme.of(context).primaryColor,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //Logo
                Padding(
                    padding: const EdgeInsets.fromLTRB(8,1,20,9),
                    child: Column(

                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(1,10,8,8),
                          child:
                          Image(
                            height: 50,
                            width: 150,
                            image: AssetImage('assets/fsac.jpeg'),
                            fit: BoxFit.cover,
                          ),
                        ),
                        Text(
                          'Empower &',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 4.2 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          'Connect',
                          style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 4.2 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular',
                          ),
                          textAlign: TextAlign.start,
                        ),

                        SizedBox(
                          height: 10.0,
                        ),
                        // Text(
                        //   'Complete Registration by paying a fee of Ksh. 500/=',
                        //   style: TextStyle(
                        //     color: Theme.of(context).accentColor,
                        //     fontSize: 2.28 * SizeConfig.textMultiplier,
                        //     fontFamily: 'PoppinsRegular',
                        //   ),
                        //   textAlign: TextAlign.start,
                        // ),
                        SizedBox(
                          height: 10.0,
                        ),
                        //Email/phone
                        // Padding(
                        //   padding: EdgeInsets.fromLTRB(20,10,20,10),
                        //   child: Container(
                        //     height: 7 * SizeConfig.heightSizeMultiplier,
                        //     width: MediaQuery.of(context).size.width- 10,
                        //     child: TextFormField(
                        //       controller: username,
                        //       textCapitalization: TextCapitalization.characters,
                        //       keyboardType: TextInputType.text,
                        //       validator: (value) => validatePhone(value),
                        //       style: TextStyle(
                        //         color: Theme.of(context).accentColor,
                        //         fontSize: 1.8 * SizeConfig.textMultiplier,
                        //         fontFamily: 'PoppinsRegular',
                        //       ),
                        //       decoration: InputDecoration(
                        //         border: OutlineInputBorder(
                        //             borderSide: BorderSide(
                        //                 color: Theme.of(context).accentColor)),
                        //         hintText: ' .... ',
                        //         labelText: 'MPESA CODE',
                        //
                        //         prefixIcon: Padding(
                        //           padding: EdgeInsets.only(top: 1), // add padding to adjust icon
                        //           child: Icon(Icons.call),
                        //         ),
                        //       ),
                        //
                        //     ),
                        //   ),
                        // ),
                        SizedBox(
                          height: 30.0,
                        ),
                        //NextButton
                        Container(
                          height: 6.1 * SizeConfig.heightSizeMultiplier,
                          child: loading ? spinKit() :RaisedButton(
                            onPressed: () {
                              setState(() {

                                loading = true;
                              });
                              regPayment('stk_push');


                              print('Next');
                            },
                            child: Text(
                              'CONFIRM PAYMENT ',
                              style: TextStyle(fontSize: 2.6 * SizeConfig.textMultiplier, fontWeight: FontWeight.bold, color: Colors.white),
                            ),
                            color: Theme.of(context).primaryColor,
                          ),


                        ),
                        SizedBox(
                          height: 20.0,
                        ),


                      ],
                    )
                )
              ]
          ),
        ),
      ),
      bottomNavigationBar: new Container(
        height: 50.0,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30,0,30,10),
              child: Text(
                'By signing up you indicate that you have read and agreed to the Terms of Service',
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 1.3 * SizeConfig.textMultiplier,
                  fontFamily: 'PoppinsRegular',
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ListItem {
  int value;
  String name;

  ListItem(this.value, this.name);
}
//Forgot Password Page 3

class ForgotPasswordNew extends StatefulWidget {
  @override
  _ForgotPasswordNewState createState() => _ForgotPasswordNewState();
}

class _ForgotPasswordNewState extends State<ForgotPasswordNew> {
  String newPassword;
  String confirmPassword;
  List<ListItem> _dropdownItems = [
    ListItem(1, " KAA  123T "),
    ListItem(2, " KBB  123B "),
    ListItem(3, " KAY  234U "),
    ListItem(4, " KCS  234S ")
  ];

  List<DropdownMenuItem<ListItem>> _dropdownMenuItems;
  ListItem _selectedItem;

  void initState() {
    super.initState();
    _dropdownMenuItems = buildDropDownMenuItems(_dropdownItems);
    _selectedItem = _dropdownMenuItems[0].value;

  }

  List<DropdownMenuItem<ListItem>> buildDropDownMenuItems(List listItems) {
    List<DropdownMenuItem<ListItem>> items = List();
    for (ListItem listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Text(listItem.name , style: TextStyle(
              fontSize: 25,
            fontFamily: 'PoppinsMedium',
            color: Color(0xFF5191FA),
          ),),
          value: listItem,
        ),
      );
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        // leading: IconButton(
        //   onPressed: () {
        //     Navigator.push(context,
        //         MaterialPageRoute(builder: (context) => ForgotPasswordOtp()));
        //   },
        //   icon: Icon(
        //     Icons.chevron_left,
        //     color: Theme.of(context).primaryColor,
        //   ),
        // ),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'Goodley Savings App',
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              //Logo
              SizedBox(
                height: 50.0,
              ),
              Column(
                children: [

                  Text(
                    'Please enter a details required',
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 2.8 * SizeConfig.textMultiplier,
                      fontFamily: 'PoppinsMedium',
                    ),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 70.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Select Plates: ',
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 2.8 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsMedium',
                          ),
                          textAlign: TextAlign.center,
                        ),
                    Container(
                      padding: EdgeInsets.all(20.0),
                      child: DropdownButton<ListItem>(
                          value: _selectedItem,
                          items: _dropdownMenuItems,
                          onChanged: (value) {
                            setState(() {
                              _selectedItem = value;
                            });
                          }),
                    ),

                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Account Balance: ',
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 2.8 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsMedium',
                          ),
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          '    Ksh. 4,300',
                          style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 2.8 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsMedium',
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 7.0,
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Container(
                      height: 7.8 * SizeConfig.heightSizeMultiplier,
                      width: 54.6 * SizeConfig.heightSizeMultiplier,
                      child: TextField(
                        keyboardType: TextInputType.number,
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontSize: 2.2 * SizeConfig.textMultiplier,
                          fontFamily: 'PoppinsRegular',
                        ),
                        decoration: InputDecoration(

                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context).accentColor)),
                          hintText: 'Enter amount to refill',
                          labelText: 'Enter amount to refill',
                        ),
                        onSubmitted: (text) {
                          confirmPassword = text;
                          print('phone : $confirmPassword');
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  //NextButton
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child:  Container(
                      height: 7.8 * SizeConfig.heightSizeMultiplier,
                      width: MediaQuery.of(context).size.width,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ForgotPasswordOtp2()));
                        },
                        child: Text(
                          'Submit',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontFamily: 'PoppinsMedium'),
                        ),
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

//Forgot Password Page 2

class ForgotPasswordOtp2 extends StatefulWidget {
  @override
  _ForgotPasswordOtpState2 createState() => _ForgotPasswordOtpState2();
}

class _ForgotPasswordOtpState2 extends State<ForgotPasswordOtp2> {
  String otp;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar:  AppBar(
        // leading: IconButton(
        //   onPressed: () {
        //     Navigator.push(context,
        //         MaterialPageRoute(builder: (context) => ForgotPasswordOtp()));
        //   },
        //   icon: Icon(
        //     Icons.chevron_left,
        //     color: Theme.of(context).primaryColor,
        //   ),
        // ),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'Verify',
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
        centerTitle: true,
      ),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              //Logo
              Column(
                children: [
                  Image(
                    image: AssetImage('assets/forgot2.png'),
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                    'Please Verify Your Transaction',
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 2.8 * SizeConfig.textMultiplier,
                      fontFamily: 'PoppinsMedium',
                    ),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Text(
                      'We will send a verification code to your registered  Phone',
                      style: TextStyle(
                        color: Theme.of(context).accentColor,
                        fontSize: 2.18 * SizeConfig.textMultiplier,
                        fontFamily: 'PoppinsRegular',
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  //Email/phone
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Container(
                          height: 7.8 * SizeConfig.heightSizeMultiplier,
                          width: 7.8 * SizeConfig.heightSizeMultiplier,
                          child: TextField(
                            maxLength: 1,
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 2.2 * SizeConfig.textMultiplier,
                              fontFamily: 'PoppinsRegular',
                            ),
                            decoration: InputDecoration(
                              hintText: '*',
                              counterText: "",
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Theme.of(context).accentColor)),
                            ),
                            onSubmitted: (text) {
                              otp = text;
                              print('otp $otp');
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Container(
                          height: 7.8 * SizeConfig.heightSizeMultiplier,
                          width: 7.8 * SizeConfig.heightSizeMultiplier,
                          child: TextField(
                            maxLength: 1,
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 2.2 * SizeConfig.textMultiplier,
                              fontFamily: 'PoppinsRegular',
                            ),
                            decoration: InputDecoration(
                              hintText: '*',
                              counterText: "",
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Theme.of(context).accentColor)),
                            ),
                            onSubmitted: (text) {
                              otp = text;
                              print('otp $otp');
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Container(
                          height: 7.8 * SizeConfig.heightSizeMultiplier,
                          width: 7.8 * SizeConfig.heightSizeMultiplier,
                          child: TextField(
                            maxLength: 1,
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 2.2 * SizeConfig.textMultiplier,
                              fontFamily: 'PoppinsRegular',
                            ),
                            decoration: InputDecoration(
                              hintText: '*',
                              counterText: "",
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Theme.of(context).accentColor)),
                            ),
                            onSubmitted: (text) {
                              otp = text;
                              print('otp $otp');
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Container(
                          height: 7.8 * SizeConfig.heightSizeMultiplier,
                          width: 7.8 * SizeConfig.heightSizeMultiplier,
                          child: TextField(
                            maxLength: 1,
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 2.2 * SizeConfig.textMultiplier,
                              fontFamily: 'PoppinsRegular',
                            ),
                            decoration: InputDecoration(
                              hintText: '*',
                              counterText: "",
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Theme.of(context).accentColor)),
                            ),
                            onSubmitted: (text) {
                              otp = text;
                              print('otp $otp');
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 7.0,
                  ),
                  //NextButton
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Container(
                        height: 7.8 * SizeConfig.heightSizeMultiplier,
                        width: 54.6 * SizeConfig.heightSizeMultiplier,
                        child: RaisedButton(
                          onPressed: () {
                            print('Done');
                          },
                          child: Text(
                            'Done',
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Theme.of(context).primaryColor,
                        )),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}