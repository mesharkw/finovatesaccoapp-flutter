import 'package:flutter/material.dart';
import 'package:travelx_v1/screens/login.dart';
import 'package:travelx_v1/screens/onboarding.dart';

import '../size_config.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {

  String userName;
  String email;
  String password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        left: false,
        right: false,
        bottom: false,
        child: SingleChildScrollView(
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: Container(
              height: MediaQuery.of(context).size.height / 1.05,
              width: MediaQuery.of(context).size.width,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //circle image,travelx,descover
                    Padding(
                      padding: EdgeInsets.only(
                          top: 1.2 * SizeConfig.heightSizeMultiplier),
                      child: Column(
                        children: [
                          ClipRRect(
                            child: CircleAvatar(
                              radius: 7.3 * SizeConfig.heightSizeMultiplier,
                              child: Image(
                                image: AssetImage('assets/Splash.png'),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          FittedBox(
                            child: Text(
                              'Gas Refil',
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 7.5 * SizeConfig.textMultiplier,
                                  fontFamily: 'PoetsenOne'),
                            ),
                          ),
                          FittedBox(
                            child: Text(
                              'Discover The World Together',
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 2.2 * SizeConfig.textMultiplier,
                                  fontFamily: 'PoppinsRegular'),
                            ),
                          ),
                        ],
                      ),
                    ),
                    //signin
                    Padding(
                      padding: EdgeInsets.only(
                          top: 1 * SizeConfig.heightSizeMultiplier,
                          bottom: 1 * SizeConfig.heightSizeMultiplier),
                      child: FittedBox(
                        child: Text(
                          'SIGN UP',
                          style: TextStyle(
                            color: Color(0xFF265E9E),
                            fontFamily: 'PoppinsSemiBold',
                            fontSize: 4.1 * SizeConfig.textMultiplier,
                          ),
                        ),
                      ),
                    ),
                    //username
                    Container(
                      constraints: BoxConstraints(
                        minHeight: 4 * SizeConfig.heightSizeMultiplier,
                        maxHeight: 6 * SizeConfig.heightSizeMultiplier,
                        minWidth: 40 * SizeConfig.heightSizeMultiplier,
                        maxWidth: 42 * SizeConfig.heightSizeMultiplier,
                      ),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(
                              4.6 * SizeConfig.heightSizeMultiplier)),
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xFFcadefd),
                              blurRadius: 5.0,
                              offset: Offset(0, 5),
                              spreadRadius: 0.1,
                            )
                          ]),
                      child: TextField(
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(
                              top: 0.8 * SizeConfig.heightSizeMultiplier),
                          border: InputBorder.none,
                          prefixIcon: Image(
                              image: AssetImage('assets/usernameicon.png')),
                          hintText: 'Patrick Oliver',
                        ),
                        style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 2.1 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular'),
                        onSubmitted: (text) {
                          userName = text;
                          print("username is $userName");
                        },
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    //Email
                    Container(
                      constraints: BoxConstraints(
                        minHeight: 4 * SizeConfig.heightSizeMultiplier,
                        maxHeight: 6 * SizeConfig.heightSizeMultiplier,
                        minWidth: 40 * SizeConfig.heightSizeMultiplier,
                        maxWidth: 42 * SizeConfig.heightSizeMultiplier,
                      ),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(
                              4.6 * SizeConfig.heightSizeMultiplier)),
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xFFcadefd),
                              blurRadius: 5.0,
                              offset: Offset(0, 5),
                              spreadRadius: 0.1,
                            )
                          ]),
                      child: TextField(
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(
                              top: 0.8 * SizeConfig.heightSizeMultiplier),
                          border: InputBorder.none,
                          prefixIcon:
                              Image(image: AssetImage('assets/gmail.png')),
                          hintText: 'ryancooper@gmail.com',
                        ),
                        style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 2.1 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular'),
                        onSubmitted: (text) {
                          email = text;
                          print("username is $email");
                        },
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    //password
                    Container(
                      constraints: BoxConstraints(
                        minHeight: 4 * SizeConfig.heightSizeMultiplier,
                        maxHeight: 6 * SizeConfig.heightSizeMultiplier,
                        minWidth: 40 * SizeConfig.heightSizeMultiplier,
                        maxWidth: 42 * SizeConfig.heightSizeMultiplier,
                      ),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(
                              4.6 * SizeConfig.heightSizeMultiplier)),
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xFFcadefd),
                                spreadRadius: 0.1,
                                offset: Offset(0, 5),
                                blurRadius: 5.0)
                          ]),
                      child: TextField(
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                                top: 1.3 * SizeConfig.heightSizeMultiplier),
                            border: InputBorder.none,
                            prefixIcon: Image(
                                image:
                                    AssetImage('assets/passwordpadlock.png')),
                            hintText: '* * * * * * * *',
                            suffixIcon: Icon(Icons.remove_red_eye)),
                        obscureText: true,
                        style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 2.1 * SizeConfig.textMultiplier,
                            fontFamily: 'PoppinsRegular'),
                        onSubmitted: (text) {
                          password = text;
                          print("password is $password");
                        },
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    //signin button
                    Container(
                        constraints: BoxConstraints(
                          minHeight: 6.2 * SizeConfig.heightSizeMultiplier,
                          maxHeight: 9.3 * SizeConfig.heightSizeMultiplier,
                          minWidth: 43.7 * SizeConfig.heightSizeMultiplier,
                          maxWidth: 46.8 * SizeConfig.heightSizeMultiplier,
                        ),
                        decoration: BoxDecoration(
//                      color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.all(Radius.circular(
                              6.2 * SizeConfig.heightSizeMultiplier)),
                        ),
                        child: RaisedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Login()));
                            print('SIGN UP');
                          },
                          elevation: 2.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(
                                  6.2 * SizeConfig.heightSizeMultiplier),
                            ),
                          ),
                          color: Theme.of(context).primaryColor,
                          child: Text(
                            'SIGN UP',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 2.5 * SizeConfig.textMultiplier,
                              fontFamily: 'PoppinsRegular',
                            ),
                          ),
                        )),
                    //sign in later
                    FlatButton(
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Sliders()));
                        print('signin later');
                      },
                      child: Text(
                        'sign in later',
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontFamily: 'ChangaMedium',
                            fontSize: 2.5 * SizeConfig.heightSizeMultiplier),
                      ),
                    ),
                    //or
                    Text(
                      'OR',
                      style: TextStyle(
                        color: Color(0xFF265E9E),
                        fontFamily: 'Roboto',
                        fontSize: 2.3 * SizeConfig.textMultiplier,
                      ),
                    ),
                    //signup
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FittedBox(
                          child: Text(
                            'Here for the first time?',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 2 * SizeConfig.textMultiplier,
                            ),
                          ),
                        ),
                        Container(
                          height: 5.3 * SizeConfig.heightSizeMultiplier,
                          width: 10 * SizeConfig.heightSizeMultiplier,
                          child: IconButton(
                            icon: FittedBox(
                              child: Text(
                                'Sign in',
                                style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 2 * SizeConfig.textMultiplier,
                                  fontFamily: 'PoppinsSemiBold',
                                ),
                              ),
                            ),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Login()));
                              print('Sign in');
                            },
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
