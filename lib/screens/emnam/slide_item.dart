import 'package:flutter/material.dart';

class SlideItem extends StatelessWidget {
  final int index;
  SlideItem(this.index);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment:   MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          height: 200.0,
          width: 298.0,
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            image: DecorationImage(
                image: AssetImage(silderList[index].imageUrl),
                fit: BoxFit.fill),
          ),
        ),
        SizedBox(height: 170,),
        Text(silderList[index].title,style: TextStyle(fontSize: 20,color: Theme.of(context).primaryColor),),
        SizedBox(height: 10,),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Text(silderList[index].description,textAlign: TextAlign.center,style: TextStyle(fontSize: 14,color: Theme.of(context).accentColor),),
        )
      ],
    );
  }
}



class Slider{
  final String imageUrl;
  final String title;
  final String description;

  Slider({this.imageUrl,this.title,this.description});
}

final List silderList = [
  Slider(
    imageUrl: 'assets/carousel1.png',
    title: 'Refill Gas',
    description:
    'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
  ),
  Slider(
    imageUrl: 'assets/carousel2.png',
    title: 'Enjoy Easy Access',
    description:
    'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
  ),
];
