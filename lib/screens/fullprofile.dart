import 'package:flutter/material.dart';

class FullProfile extends StatefulWidget {
  @override
  _FullProfileState createState() => _FullProfileState();
}

class _FullProfileState extends State<FullProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          SafeArea(
            child: Container(
              // color: Colors.red,
              height: MediaQuery.of(context).size.height / 3.5,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: [
                  Positioned(
                    top: 50.0,
                    left: 14,
                    child: Image.asset(
                      'assets/drawerimage.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned(
                    right: 10.0,
                    child: IconButton(
                      icon: Icon(
                        Icons.chevron_left,
                        color: Theme.of(context).primaryColor,
                        size: 20.0,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                        print('back');
                      },
                    ),
                  ),
                  Positioned(
                    top: 53.5,
                    left: 17.9,
                    child: Image.asset('assets/profilepic.png'),
                  ),
                  Positioned(
                    top: 60.0,
                    right: 90,
                    child: FlatButton(
                        child: Text(
                          'Patric Oliver',
                          style: TextStyle(
                              fontFamily: 'PoppinsMedium',
                              fontSize: 22,
                              color: Theme.of(context).primaryColor),
                        ),
                        onPressed: () {
                          print('Full profile');
                        }),
                  ),
                  Positioned(
                    top: 100.0,
                    right: 70.0,
                    child: Text(
                      'Patricoliver@gmail.com',
                      style: TextStyle(
                          fontFamily: 'PoppinsMedium',
                          fontSize: 14,
                          color: Theme.of(context).accentColor),
                    ),
                  ),
                ],
              ),
            ),
          ),
          //Edit Profile
          Container(
            // color: Colors.red,
            height: MediaQuery.of(context).size.height / 2.8,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              children: [
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height / 3.1,
                    width: MediaQuery.of(context).size.width / 1.05,
                    decoration: BoxDecoration(
                        // color: Colors.amber,
                        border: Border.all(
                          color: Theme.of(context).accentColor,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(1.0))),
                    child: Center(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(height: 5.0,),
                        Text(
                          'Name',
                          style: TextStyle(color: Color(0xFF5E6D77)),
                        ),
                        Text(
                          'Petrick Oliver',
                          style: TextStyle(color: Theme.of(context).accentColor),
                        ),
                        SizedBox(height: 5.0,),
                        Text(
                          'Email',
                          style: TextStyle(color: Color(0xFF5E6D77)),
                        ),
                        Text(
                          'Petrickoliver@gmail.com',
                          style: TextStyle(color: Theme.of(context).accentColor),
                        ),
                        SizedBox(height: 5.0,),
                        Text(
                          'Phone',
                          style: TextStyle(color: Color(0xFF5E6D77)),
                        ),
                        Text(
                          '+44 256 658 9854',
                          style: TextStyle(color: Theme.of(context).accentColor),
                        ),
                        SizedBox(height: 5.0,),
                        Text(
                          'Address',
                          style: TextStyle(color: Color(0xFF5E6D77)),
                        ),
                        Text(
                          '21 The Grove Manchester, London, UK',
                          style: TextStyle(color: Theme.of(context).accentColor),
                        ),
                        SizedBox(height: 5.0,),
                      ],
                    )),
                  ),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: Theme.of(context).accentColor,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(1.0))),
                    child: Padding(
                      padding: EdgeInsets.all(3.0),
                      child: Text(
                        'Edit Profile',
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          //Change Password
          Container(
            // color: Colors.red,
            height: MediaQuery.of(context).size.height / 3.5,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              children: [
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height / 4,
                    width: MediaQuery.of(context).size.width / 1.05,
                    decoration: BoxDecoration(
                      // color: Colors.amber,
                        border: Border.all(
                          color: Theme.of(context).accentColor,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(1.0))),
                    child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            SizedBox(height: 10.0,),
                            Text(
                              'Old Password',
                              style: TextStyle(color: Color(0xFF5E6D77)),
                            ),
                            Text(
                              '**********',
                              style: TextStyle(color: Theme.of(context).accentColor),
                            ),
                            SizedBox(height: 5.0,),
                            Text(
                              'New Password',
                              style: TextStyle(color: Color(0xFF5E6D77)),
                            ),
                            Text(
                              '**********',
                              style: TextStyle(color: Theme.of(context).accentColor),
                            ),
                            SizedBox(height: 5.0,),
                            Text(
                              'Confirm Password',
                              style: TextStyle(color: Color(0xFF5E6D77)),
                            ),
                            Text(
                              '**********',
                              style: TextStyle(color: Theme.of(context).accentColor),
                            ),
                            SizedBox(height: 5.0,),
                          ],
                        )),
                  ),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: Theme.of(context).accentColor,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(1.0))),
                    child: Padding(
                      padding: EdgeInsets.all(3.0),
                      child: Text(
                        'Change Password',
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
