import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:travelx_v1/screens/forgot_password.dart';
import 'package:travelx_v1/screens/onboarding.dart';
import 'package:travelx_v1/screens/sign_up.dart';
import '../size_config.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String userName;
  String password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        left: false,
        right: false,
        bottom: false,
        child: SingleChildScrollView(
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: Container(
              height: MediaQuery.of(context).size.height / 1.05,
              width: MediaQuery.of(context).size.width,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //circle image,travelx,descover
                    Padding(
                      padding: EdgeInsets.only(
                          top: 1.2 * SizeConfig.heightSizeMultiplier),
                      child: Column(
                        children: [
                          ClipRRect(
                            child: CircleAvatar(
                              radius: 7.3 * SizeConfig.heightSizeMultiplier,
                              child: Image(
                                image: AssetImage('assets/fsac.jpeg'),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          FittedBox(
                            child: Text(
                              'Gas Refill',
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 7.5 * SizeConfig.textMultiplier,
                                  fontFamily: 'PoetsenOne'),
                            ),
                          ),

                        ],
                      ),
                    ),
                    //signin
                    Padding(
                      padding: EdgeInsets.only(
                          top: 1 * SizeConfig.heightSizeMultiplier,
                          bottom: 1 * SizeConfig.heightSizeMultiplier),
                      child: FittedBox(
                        child: Text(
                          'SIGN IN',
                          style: TextStyle(
                            color: Color(0xFF265E9E),
                            fontFamily: 'PoppinsSemiBold',
                            fontSize: 4.1 * SizeConfig.textMultiplier,
                          ),
                        ),
                      ),
                    ),
                    //username
                    Container(
                      constraints: BoxConstraints(
                        minHeight: 4 * SizeConfig.heightSizeMultiplier,
                        maxHeight: 6 * SizeConfig.heightSizeMultiplier,
                        minWidth: 40 * SizeConfig.heightSizeMultiplier,
                        maxWidth: 42 * SizeConfig.heightSizeMultiplier,
                      ),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(
                              4.6 * SizeConfig.heightSizeMultiplier)),
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xFFcadefd),
                              blurRadius: 5.0,
                              offset: Offset(0, 5),
                              spreadRadius: 0.1,
                            )
                          ]),
                      child: TextField(
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(
                              top: 1.5 * SizeConfig.heightSizeMultiplier),
                          border: InputBorder.none,
                          prefixIcon: Image(
                              image: AssetImage('assets/fsac.jpeg')),
                          hintText: '254711223344',
                        ),
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontSize: 2.2 * SizeConfig.textMultiplier,
                          fontFamily: 'PoppinsRegular',
                        ),
                        onSubmitted: (text) {
                          userName = text;
                          print("username is $userName");
                        },
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    //password

                    //signin button
                    Container(
                        constraints: BoxConstraints(
                          minHeight: 6.2 * SizeConfig.heightSizeMultiplier,
                          maxHeight: 9.3 * SizeConfig.heightSizeMultiplier,
                          minWidth: 43.7 * SizeConfig.heightSizeMultiplier,
                          maxWidth: 46.8 * SizeConfig.heightSizeMultiplier,
                        ),
                        decoration: BoxDecoration(
//                      color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.all(Radius.circular(
                              6.2 * SizeConfig.heightSizeMultiplier)),
                        ),
                        child: RaisedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Sliders()));
                            print('SIGN IN');
                          },
                          elevation: 2.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(
                                  6.2 * SizeConfig.heightSizeMultiplier),
                            ),
                          ),
                          color: Theme.of(context).primaryColor,
                          child: Text(
                            'SIGN IN',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 2.5 * SizeConfig.textMultiplier,
                              fontFamily: 'PoppinsRegular',
                            ),
                          ),
                        )),
                    SizedBox(height: 200,)

                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
